<?php

session_start();
	if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && !isset($_SESSION['permiso_user']) && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
    exit;
        }
        elseif ($_SESSION['permiso_user'] == 2) {
          header("location: accesorestringido.php");
        }elseif($_SESSION['permiso_user'] == 1){
	
	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_facturas="";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";	
	$active_reportes="";
  $active_reportes_fecha="active";
	$title="Clientes | Control Total";
?>

<!DOCTYPE HTML>
<html> 
<head> 
	<?php include("head.php");?>
  <title>Desarrollo Hidrocálido</title> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="utf-8">
 <link rel="stylesheet" href="css/custom.css">
  <link href="bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        
 
        
        
</head> 
<body class="hold-transition skin-blue sidebar-mini"> 
<?php
	include("navbar2.php");
	?>

  <div class="content-wrapper">
    
     <!--INICIO -->
<div class="container">
<div class="row">
  <div class="col-xs-12">
   
    <div class="col-md-4 col-md-offset-3 div1">
        <div class="well well-sm">
          <form class="form-horizontal" action="" method="post">
          <fieldset>
            <legend class="text-center">Desde: </legend>
                  <div class='input-group date' id='divMiCalendario'> 
                      <input type='hidden' id="q" class="form-control" onkeyup='load(1);' />
                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                  
          </fieldset>
          </form>
        </div>
      </div>
 

      <div class="col-md-4 col-md-offset-3 div2">
        <div class="well well-sm">
          <form class="form-horizontal" action="" method="post">
          <fieldset>
            <legend class="text-center">Hasta: </legend>
                  <div class='input-group date' id='divMiCalendario2'>
                      <input type='text' id="q2" class="form-control" onkeyup='load(1);'  />
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                      </span>

                  </div>
          </fieldset>
          </form>
        </div>
      </div>

                   <input class="btn btn-primary" type="button" name="buscar" value="Buscar" onclick='load(1);'>
             
    </div>
  </div> 
</div>
      
  


  
  <div id="resultados"></div><!-- Carga los datos ajax -->
  <section class='outer_div content'>
    
  </section><!-- Carga los datos ajax -->


  </div>
 
  <!--FIN -->
 
   <script src="bootstrap/js/moment.min.js"></script>
   <script src="bootstrap/js/bootstrap-datetimepicker.min.js"></script>
   <script src="bootstrap/js/bootstrap-datetimepicker.es.js"></script>
   <script type="text/javascript">
     $('#divMiCalendario').datetimepicker({
          format: 'YYYY-MM-DD'      
      });
      

      $('#divMiCalendario2').datetimepicker({
          format: 'YYYY-MM-DD'       
      });
      
   </script>
    
   <script src="js/reporte_cuenta.js"></script>
   <script src="js/VentanaCentrada.js"></script>
   <?php
	include("footer.php");
	?>
</body>
</html>
<?php
        }
?>