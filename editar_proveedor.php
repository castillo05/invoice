<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: login.php");
		exit;
        }
        
	$active_facturas="active";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Editar Factura | Control Total";
	
	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	if (isset($_GET['cod_prov']))
	{
		$id=intval($_GET['cod_prov']);
		//$factura=intval($_GET['numero_factura']);
		$campos="*";
		$sql=mysqli_query($con,"select * from proveedores where cod_prov='".$id."'");
		$count=mysqli_num_rows($sql);
		if ($count==1)
		{
				$row=mysqli_fetch_array($sql);
						$cod_prov=$row['cod_prov'];
						$cedula=$row['cedula'];
						$nombre=$row['nombre'];
						$direccion=$row['direccion'];
						$telefono=$row['telefono'];
						$celular=$row['celular'];
						$email=$row['email'];
						$estado=$row['estado'];

		}	
		else
		{
			header("location: proveedores.php");
			exit;	
		}
	} 
	else 
	{
		header("location: proveedores.php");
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php");?>
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	?>  
	<div class="content-wrapper">
   
	<div class="panel panel-info">
		<div class="panel-heading">
			<h4><i class='glyphicon glyphicon-edit'></i> Editar Proveedores</h4>
		</div>
		<div class="panel-body">
		<?php 
			// include("modal/buscar_productos.php");
			// include("modal/registro_clientes.php");
			// include("modal/registro_productos.php");
		?>
			<form class="form-horizontal" role="form" id="datos_proveedor">
				<div class="form-group row">
				  <label for="nombre_cliente" class="col-md-2 control-label">Cedula o # Ruc</label>
				  <div class="col-md-2">
					  <input type="text" class="form-control input-sm" id="cedula" name="cedula" placeholder="" required value="<?php echo $cedula;?>">
					  <input id="cod_prov" name="cod_prov" type='hidden' value="<?php echo $cod_prov;?>">	
				  </div>
				  <label for="tel1" class="col-md-2 control-label">Nombre</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="nombre" name="nombre" placeholder="factura" value="<?php echo $nombre;?>" >
							</div>


							<label for="fecha_compra" class="col-md-2 control-label">Direccion</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="direccion" name="direccion" placeholder="Fecha de compra" value="<?php echo $direccion;?>">
							</div>

				</div>


				<div class="form-group row">		
							
							<label for="monto" class="col-md-2 control-label">Correo</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="correo" name="correo" placeholder="correo" readonly value="<?php echo $email;?>">
							</div>

							<label for="fecha_vencimiento" class="col-md-2 control-label">Celular</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="celular" name="celular" placeholder="Fecha de vencimiento" value="<?php echo $celular;?>">
							</div>
							<label for="estado_factura" class="col-md-2 control-label">Estado</label>
							<div class="col-md-2">
								<select class='form-control input-sm ' id="estado" name="estado">
									<option value="1" <?php if ($estado==1){echo "selected";}?>>Activo</option>
									<option value="2" <?php if ($estado==2){echo "selected";}?>>Inactivo</option>
									
								</select>
							</div>
				</div>
					

				 
						

							
				
				
				<div class="col-md-12">
					<div class="pull-right">
						<button type="submit" class="btn btn-default">
						  <span class="glyphicon glyphicon-refresh"></span> Actualizar datos
						</button>
						
						
						
						
					</div>	
				</div>
			</form>	
			<div class="clearfix"></div>
				<div class="editar_proveedor" class='col-md-12' style="margin-top:10px"></div><!-- Carga los datos ajax -->	
			
		<!--<div id="resultados" class='col-md-12' style="margin-top:10px"></div> Carga los datos ajax -->			
			
		</div>
	</div>		
		 
	
	</div>
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	<script type="text/javascript" src="js/proveedores.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	
  </body>
</html>