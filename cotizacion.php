<?php
ob_start();
?>
<?php
	 /* Connect To Database*/
  require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
  require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	@session_start();
	if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
		exit;
        }

	
	$active_facturas="active";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Cotizaciones | Control Total";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<?php include("head.php");?>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	 include("modal/buscar_productos.php");
	?>  
	<div class="content-wrapper">
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-shopping-cart"></i> </a></li>
				<li class="active">Cotizaciones</li>
			</ol>
			
		</div>
    <div class="row">
			
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4><i class="glyphicon glyphicon-edit"></i> Nueva Cotización</h4>
					</div>
					<div class="panel-body">
					
						<form class="form-horizontal" role="form" id="datos_cotizacion">
							<div class="form-group row">
							  <label for="nombre_cliente" class="col-md-2 control-label">Selecciona el cliente:</label>
							  <div class="col-md-3">
								  <input type="text" class="form-control input-sm ui-autocomplete-input" id="nombre_cliente" placeholder="Ingresa el nombre del cliente" required="" autocomplete="off">
								  <input id="id_cliente" type="hidden">	
							 </div>
							  
							  
							  <label for="atencion" class="col-md-1 control-label">Atención:</label>
								<div class="col-md-2">
									<select class="form-control input-sm" id="atencion" name="atencion" onchange="">
									<option value="">Atención</option>
									
									<?php
										$sql_vendedor=mysqli_query($con,"select * from users where user_id='".$_SESSION['user_id']."' order by lastname");
										while ($rw=mysqli_fetch_array($sql_vendedor)){
											$id_vendedor=$rw["user_id"];
											$nombre_vendedor=$rw["firstname"]." ".$rw["lastname"];
											if ($id_vendedor==$_SESSION['user_id']){
												$selected="";
											} else {
												$selected="";
											}
											?>
											<option value="<?php echo $id_vendedor?>" <?php echo $selected;?>><?php echo $nombre_vendedor?></option>
											<?php
										}
									?>
								</select>
								<input type="hidden" name="user_id" id="user_id" value="">
								</div>
								
								<div class="row">
								<div class="col-xs-2">
									<input type="text" class="form-control input-sm" id="tel1" placeholder="" value="Teléfono" readonly="">
								 </div>
								 <div class="col-xs-2">
									<input type="text" class="form-control input-sm" id="email_contact" placeholder="" value="Correo electrónico" readonly="">
								 </div>
								
								</div>
							</div>
							<div class="form-group row">
								<label for="empresa" class="col-md-2 control-label">Empresa:</label>
								<div class="col-md-3">
									<input type="text" class="form-control input-sm" id="empresa" placeholder="" readonly="">
								</div>
								<label for="tel2" class="col-md-1 control-label">Teléfono:</label>
								<div class="col-md-2">
									<input type="text" class="form-control input-sm" id="tel2" placeholder="" readonly="">
								</div>
								<label for="email" class="col-md-1 control-label">Email:</label>
								<div class="col-md-3">
									<input type="email" class="form-control input-sm" id="email" placeholder="" readonly="">
								</div>
							</div>
							<div class="form-group row">
								<label for="condiciones" class="col-md-2 control-label">Condiciones de pago:</label>
								<div class="col-md-3">
									<select class="form-control input-sm" id="condiciones" required="">
										<option value="">Selecciona condiciones de pago</option>
										<option value="1" selected="">Contado</option>
										<option value="2">Crédito 30 días</option>
										<option value="3">Crédito 45 días</option>
										<option value="4">Crédito 60 días</option>
										
									</select>
								</div>
								<label for="validez" class="col-md-1 control-label">Validez:</label>
								<div class="col-md-2">
									<select class="form-control input-sm" id="validez" required="">
										<option value="">Selecciona validez de oferta</option>
										<option value="1">5 días</option>
										<option value="2">10 días</option>
										<option value="3" selected="">15 días</option>
										<option value="4">30 días</option>
										
									</select>
								</div>
								<label for="entrega" class="col-md-1 control-label">Tiempo:</label>
								<div class="col-md-3">
									<input type="text" class="form-control input-sm" id="entrega" placeholder="Tiempo de entrega" value="Inmediato">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="condiciones" class="col-md-2 control-label">Nota:</label>
								<div class="col-md-6">
									<input type="text" class="form-control input-sm" id="notas" placeholder="Nota" maxlength="255">
								</div>
								
								<label for="moneda" class="col-md-1 control-label">Moneda:</label>
								<div class="col-md-3">
									<select name="moneda" id="moneda" class="form-control input-sm" onchange="update_cotizacion(7,this.value);">
																						<option value="1" selected="">USD Dollar</option>
																								<option value="3">Euro</option>
																								<option value="4">Pesos Chilenos</option>
																								<option value="5">Real</option>
																								<option value="8">Pesos</option>
																								<option value="10">Pesos Colombianos</option>
																								<option value="11">SOL</option>
																								<option value="13">pesos mexicanos</option>
																								<option value="14">Pesos Mexicanos</option>
																								<option value="17">Pesos Dominicanos</option>
																								<option value="18">Peso argentino</option>
																								<option value="19">BRL Real</option>
																								<option value="20">Bolivar Fuerte</option>
																								<option value="21">Kwanza</option>
																								<option value="22">Sol</option>
																								<option value="23">Colon</option>
																					</select>
								</div>
								
							</div>
							<hr>
							
							<div class="col-md-12">
								<div class="pull-right">
									<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
									 <span class="glyphicon glyphicon-plus"></span> Agregar productos
									</button>
									<button type="submit" class="btn btn-default">
									  <span class="glyphicon glyphicon-print"></span> Imprimir
									</button>
								</div>	
							</div>
						</form>	
						<div id="resultados" class="col-md-12" style="margin-top:10px"></div><!-- Carga los datos ajax -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	<script type="text/javascript" src="js/nueva_cotizacion.js"></script>
  	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="jquery-ui-themes-1.12.0/jquery-ui.css">
		<script>
		$(function() {
						$("#nombre_cliente").autocomplete({
							source: "./ajax/autocomplete/clientes.php",
							minLength: 2,
							select: function(event, ui) {
								event.preventDefault();
								$('#id_cliente').val(ui.item.id_cliente);
								$('#nombre_cliente').val(ui.item.nombre_cliente);
								$('#tel2').val(ui.item.telefono_cliente);
								$('#email').val(ui.item.email_cliente);
																
								
							 }
						});
						 
						
					});
					
	$("#nombre_cliente" ).on( "keydown", function( event ) {
						if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
						{
							$("#id_cliente" ).val("");
							$("#tel1" ).val("");
							$("#mail" ).val("");
											
						}
						if (event.keyCode==$.ui.keyCode.DELETE){
							$("#nombre_cliente" ).val("");
							$("#id_cliente" ).val("");
							$("#tel1" ).val("");
							$("#mail" ).val("");
						}
			});	
	</script>



  </body>
</html>
<?php
ob_end_flush();
?>