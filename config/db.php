<?php
/*Datos de conexion a la base de datos*/
define('DB_HOST', 'localhost');//DB_HOST:  generalmente suele ser "127.0.0.1"
define('DB_USER', 'root');//Usuario de tu base de datos
define('DB_PASS', '');//Contraseña del usuario de la base de datos
define('DB_NAME', 'invoice');//Nombre de la base de datos
 
/*Datos de la empresa*/
define('NOMBRE_EMPRESA', 'APPTECNO');
define('DIRECCION_EMPRESA', 'EL SAUCE, LEON, NICARAGUA');
define('TELEFONO_EMPRESA', '+(505) 8616-5414');
define('EMAIL_EMPRESA', 'jorgeantonio20142014@gmail.com');
define('TAX', '13');

/*-------------------------------------------------
Los datos por defecto del usuario administrador son:
#usuario: admin
#contraseña: admin
Los datos de inicio de sesion podran ser modificados 
desde el panel de control del sistema
---------------------------------------------------*/


?>