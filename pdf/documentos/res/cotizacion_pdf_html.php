
<style type="text/css">
<!--
table { vertical-align: top; }
tr    { vertical-align: top; }
td    { vertical-align: top; }
.midnight-blue{
	background:#2c3e50;
	padding: 4px 4px 4px;
	color:white;
	font-weight:bold;
	font-size:12px;
}
.silver{
	background:white;
	padding: 3px 4px 3px;
}
.clouds{
	background:#ecf0f1;
	padding: 3px 4px 3px;
}
.border-top{
	border-top: solid 1px #bdc3c7;
	
}
.border-left{
	border-left: solid 1px #bdc3c7;
}
.border-right{
	border-right: solid 1px #bdc3c7;
}
.border-bottom{
	border-bottom: solid 1px #bdc3c7;
}
table.page_footer {width: 80%; border: none; background-color: white; padding: 2mm;border-collapse:collapse; border: none;}
}
-->
</style>
<page backtop="15mm" backbottom="15mm" backleft="15mm" backright="15mm" style="font-size: 12pt; font-family: arial" >
        <page_footer>
        <table class="page_footer">
            <tr>

                <td style="width: 50%; text-align: left">
                    P&aacute;gina [[page_cu]]/[[page_nb]]
                </td>
                <td style="width: 50%; text-align: right">
                    &copy; <?php echo "obedalvarado.pw "; echo  $anio=date('Y'); ?>
                </td>
            </tr>
        </table>
    </page_footer>
    <table cellspacing="0" style="width: 100%;">
        <tr>

            <td style="width: 25%; color: #444444;">
                <img style="width: 100%;" src="../../img/logo.jpg" alt="Logo"><br>
                
            </td>
			<td style="width: 50%; color: #34495e;font-size:12px;text-align:center">
                <span style="color: #34495e;font-size:14px;font-weight:bold"><?php echo NOMBRE_EMPRESA;?></span>
				<br><?php echo DIRECCION_EMPRESA;?><br> 
				Teléfono: <?php echo TELEFONO_EMPRESA;?><br>
				Email: <?php echo EMAIL_EMPRESA;?>
                
            </td>
			<td style="width: 25%;text-align:right">
			COTIZACION Nº <?php echo $numero_cotizacion;?>
			</td>
			
        </tr>
    </table>
    <br>
    

	
    <table cellspacing="0" style="width: 80%; text-align: left; font-size: 11pt;">
        <tr>
           <td style="width:50%;" class='midnight-blue'>COTIZACION A</td>
        </tr>
		<tr>
           <td style="width:50%;" >
			<?php 
				$sql_cliente=mysqli_query($con,"select * from clientes where id_cliente='$id_cliente'");
				$rw_cliente=mysqli_fetch_array($sql_cliente);
				echo $rw_cliente['nombre_cliente'];
				echo "<br>";
				echo $rw_cliente['direccion_cliente'];
				echo "<br> Teléfono: ";
				echo $rw_cliente['telefono_cliente'];
				echo "<br> Email: ";
				echo $rw_cliente['email_cliente'];
			?>
			
		   </td>
        </tr>
        
   
    </table>
    
       <br>
		<table cellspacing="0" style="width: 80%; text-align: left; font-size: 11pt;">
        <tr>
           <td style="width:35%;" class='midnight-blue'>VENDEDOR</td>
		  <td style="width:25%;" class='midnight-blue'>FECHA</td>
		   <td style="width:40%;" class='midnight-blue'>FORMA DE PAGO</td>
		   <td style="width:40%;" class='midnight-blue'>VALIDEZ</td>
        </tr>
		<tr>
           <td style="width:35%;">
			<?php 
				$sql_user=mysqli_query($con,"select * from users where user_id='$id_contacto'");
				$rw_user=mysqli_fetch_array($sql_user);
				echo $rw_user['firstname']." ".$rw_user['lastname'];
			?>
		   </td>
		  <td style="width:25%;"><?php echo date("d/m/Y");?></td>
		   <td style="width:40%;" >
				<?php 
				if ($condiciones==1){echo "Contado";}
				elseif ($condiciones==2){echo "Credito de 30 dias";}
				elseif ($condiciones==3){echo "Credito de 45 dias";}
				elseif ($condiciones==4){echo "Credito de 60 dias";}
				?>
		   </td>
		   <td style="width:40%;" >
				<?php 
				if ($validez==1){echo "5 Dias";}
				elseif ($validez==2){echo "10 dias";}
				elseif ($validez==3){echo "15 dias";}
				elseif ($validez==4){echo "30 dias";}
				?>
		   </td>
        </tr>
		
        
   
    </table>
	<br>
  
    <table cellspacing="0" style="width: 80%; text-align: left; font-size: 10pt;">
        <tr>
            <th style="width: 10%;text-align:center" class='midnight-blue'>CANT.</th>
            <th style="width: 60%" class='midnight-blue'>DESCRIPCION</th>
            <th style="width: 15%;text-align: right" class='midnight-blue'>PRECIO UNIT.</th>
             <th style="width: 15%;text-align: right" class='midnight-blue'>DESCUENTO.</th>
             <th style="width: 15%;text-align: right" class='midnight-blue'>IVA.</th>
            <th style="width: 15%;text-align: right" class='midnight-blue'>PRECIO TOTAL</th>
            
        </tr>

<?php
$nums=1;
$sumador_total=0;
$iva_total=0;
$sql=mysqli_query($con, "select * from products, tmp_cotizacion where products.id_producto=tmp_cotizacion.id_producto and tmp_cotizacion.session_id='".$session_id."'");
while ($row=mysqli_fetch_array($sql))
	{
	$id_tmp=$row["id_tmp"];
	$id_producto=$row["id_producto"];
	$codigo_producto=$row['codigo_producto'];
	$cantidad=$row['cantidad_tmp'];
	$nombre_producto=$row['nombre_producto'];
	$des=$row['descuento_tmp'];
	$iva=$row['iva_tmp'];
	//$stock=$row['stock'];
	//$cantidad2=$row['cantidad'];
	$precio_venta=$row['precio_tmp'];
	$precio_venta_f=number_format($precio_venta,2);//Formateo variables
	$precio_venta_r=str_replace(",","",$precio_venta_f);//Reemplazo las comas
	$precio_total=$precio_venta_r*$cantidad;
	//$stock=$stock-$cantidad;

	$precio_total_f=number_format($precio_total,2);//Precio total formateado
	$precio_total_r=str_replace(",","",$precio_total_f);//Reemplazo las comas
	$sumador_total+=$precio_total_r;//Sumador
	$iva_total+=$iva;
	//Actualiza cada uno de los productos facturados (Actualiza el stock)
	//salidas de productos segun codigo
	// $salidas=$row['salida'];
	// $salida=$salidas+$cantidad;
	$date2=date("Y-m-d");
	//$update=mysqli_query($con,"UPDATE products set stock=stock-'".$cantidad."',salida='".$salida."' where codigo_producto='".$codigo_producto."'");

	if ($nums%2==0){
		$clase="clouds";
	} else {
		$clase="silver";
	}
	?>

        <tr>
            <td class='<?php echo $clase;?>' style="width: 10%; text-align: center"><?php echo $cantidad; ?></td>
            <td class='<?php echo $clase;?>' style="width: 60%; text-align: left"><?php echo $nombre_producto;?></td>
            <td class='<?php echo $clase;?>' style="width: 15%; text-align: right"><?php echo $precio_venta_f;?></td>
              <td class='<?php echo $clase;?>' style="width: 15%; text-align: right"><?php echo $des;?></td>
               <td class='<?php echo $clase;?>' style="width: 15%; text-align: right"><?php echo $iva;?></td>
            <td class='<?php echo $clase;?>' style="width: 15%; text-align: right"><?php echo $precio_total_f;?></td>
            
        </tr>

	<?php 
	//Insert en la tabla detalle_cotizacion
	$insert_detail=mysqli_query($con, "INSERT INTO detalle_cotizacion VALUES ('','$numero_cotizacion','$id_producto','$cantidad','$des','$iva','$precio_venta_r')");
	//$consultar_kardex=mysqli_query($con,"select * from kardex where cod_producto='".$codigo_producto."'");
	// $existencia=0;
	// $existencias=$existencia-$cantidad;
	 //$insert_kardex=mysqli_query($con,"INSERT into kardex values ('','$codigo_producto','$date2','1','$numero_factura','$nombre_producto','','$cantidad','$existencias')");
	$nums++;
	}
	$subtotal=number_format($sumador_total,2,'.','');
	$total_iva=$iva_total;
	$total_iva=number_format($total_iva,2,'.','');
	$total_factura=$subtotal+$total_iva;

	
	
	
	

?>
	  
        <tr>
        <td></td>
        <td></td>
            <td colspan="3" style="widtd: 85%; text-align: right;">SUBTOTAL &#36; </td>
            <td style="widtd: 15%; text-align: right;"> <?php echo number_format($subtotal,2);?></td>
        </tr>
		<tr>
		<td></td>
		<td></td>
            <td colspan="3" style="widtd: 85%; text-align: right;">IVA (<?php echo 15; ?>)% &#36; </td>
            <td style="widtd: 15%; text-align: right;"> <?php echo number_format($total_iva,2);?></td>
        </tr><tr>
        <td></td>
        <td></td>
            <td colspan="3" style="widtd: 85%; text-align: right;">TOTAL &#36; </td>
            <td style="widtd: 15%; text-align: right;"> <?php echo number_format($total_factura,2);?></td>
        </tr>
    </table>
	
	
	
	<br>
	<div style="font-size:11pt;text-align:center;font-weight:bold">Gracias por su compra!</div>
	
	
	  

</page>

<?php
$date=date("Y-m-d");

		// if ($vencido==1) {
			
		// 	$fecha_ven=$nuevafecha = strtotime ( '+15 day' , strtotime ( $date ) ) ;
		// 	$fecha_ven=date ( 'Y-m-d' , $fecha_ven );
		// }elseif($vencido==2){
		// 	$fecha_ven=$nuevafecha = strtotime ( '+30 day' , strtotime ( $date ) ) ;
		// 	$fecha_ven=date ( 'Y-m-d' , $fecha_ven );
		// }elseif($vencido==3){
		// 	$fecha_ven=$nuevafecha = strtotime ( '+45 day' , strtotime ( $date ) ) ;
		// 	$fecha_ven=date ( 'Y-m-d' , $fecha_ven );
		// }
// $update=mysqli_query($con,"UPDATE products set stock='".$stock."' WHERE id_producto='".$id_producto_detalle."'");

$insert=mysqli_query($con,"INSERT INTO cotizacion VALUES ('','$numero_cotizacion','$date','$id_cliente','$id_contacto','$condiciones','$validez','$entrega','$notas','$monedas','$subtotal','$des','$total_factura')");
//
// if ($condiciones==4) {
// 	$insertar=mysqli_query($con,"INSERT INTO cuentas_cobrar VALUES ('','$numero_factura','$date','$fecha_ven','$total_factura','$id_cliente','$total_factura','','$total_factura','','$mes','$anio')");
// }



 $delete=mysqli_query($con,"DELETE FROM tmp_cotizacion WHERE session_id='".$session_id."'");





//$select=mysqli_query($con,"select * from detalle_factura,products");
	
	// 	while ($row2=mysqli_fetch_array($select)) {
	// 	# code...
	// 	$id_producto_detalle=$row2['id_producto'];
	// 	$cantidad=$row2['cantidad'];
	// 	$stock=$row['stock'];
	// 	//$stock2=$stock-$cantidad;
	// 	$update=mysqli_query($con,"update products, detalle_factura set  products.stock=products.stock-detalle_factura.cantidad where products.id_producto='".$id_producto_detalle."'");
	// }
	?>