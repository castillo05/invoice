<?php
ob_start();
?>
<?php
	@session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: ../../login.php");
		exit;
    }
	/* Connect To Database*/
	include("../../config/db.php");
	include("../../config/conexion.php");
	$id= intval($_GET['id']);
	$sql_count=mysqli_query($con,"select * from cuentas_cobrar where id='".$id."'");
	$count=mysqli_num_rows($sql_count);
	if ($count==0)
	{
	// echo "<script>alert('Factura no encontrada')</script>";
	// echo "<script>window.close();</script>";
	exit;
	}
	$sql_cuenta=mysqli_query($con,"select * from cuentas_cobrar where id='".$id."'");
	$rw_cuenta=mysqli_fetch_array($sql_cuenta);
	
	// require_once(dirname(__FILE__).'/../html2pdf.class.php');
    require __DIR__.'/vendor/autoload.php';

	use Spipu\Html2Pdf\Html2Pdf;

	$html2pdf = new Html2Pdf();
    // get the HTML
     ob_start();
     include(dirname('__FILE__').'/res/ver_cuenta_html.php');
    $content = ob_get_clean();

    try
    {
        // init HTML2PDF
        $html2pdf = new HTML2PDF('P', 'A7', 'es', true, 'UTF-8', array(0, 0, 0, 0));
        // display the full page
        $html2pdf->pdf->SetDisplayMode('fullpage');
        // convert
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        // send the PDF
        $html2pdf->Output('Factura.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>
<?php
ob_end_flush();
?>