<?php
ob_start();
?>
<?php
	@session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: ../../login.php");
		exit;
    }
	/* Connect To Database*/
	include("../../config/db.php");
	include("../../config/conexion.php");
	$id_factura= $_GET['id_factura'];
	$id_factura2= $_GET['id_factura2'];
	$condiciones= $_GET['condiciones'];
	$estado_factura= $_GET['estado_factura'];
	$sql_count=mysqli_query($con,"SELECT * FROM facturas, clientes, users WHERE facturas.id_cliente=clientes.id_cliente and facturas.id_vendedor=users.user_id and facturas.estado_factura= '".$estado_factura."' and facturas.condiciones='".$condiciones."' and facturas.fecha_factura between '".$id_factura."' and '".$id_factura2."'");
	$count=mysqli_num_rows($sql_count);
	if ($count==0)
	{
	echo "<script>alert('Rango de fecha no validos')</script>";
	echo "<script>window.close();</script>";
	exit;
	}
	$sql_factura=mysqli_query($con,"SELECT * FROM facturas, clientes, users WHERE facturas.id_cliente=clientes.id_cliente and facturas.id_vendedor=users.user_id and facturas.estado_factura= '".$estado_factura."' and facturas.condiciones='".$condiciones."' and facturas.fecha_factura between '".$id_factura."' and '".$id_factura2."'");
	$rw_factura=mysqli_fetch_array($sql_factura);
	$numero_factura=$rw_factura['numero_factura'];
	$id_cliente=$rw_factura['id_cliente'];
	$id_vendedor=$rw_factura['id_vendedor'];
	$fecha_factura=$rw_factura['fecha_factura'];
	$condiciones=$rw_factura['condiciones'];
	// require_once(dirname(__FILE__).'/../html2pdf.class.php');
	require __DIR__.'/vendor/autoload.php';

	use Spipu\Html2Pdf\Html2Pdf;

	$html2pdf = new Html2Pdf();
    // get the HTML
     ob_start();
     include(dirname('__FILE__').'/res/ver_reporte_html.php');
    $content = ob_get_clean();

    try
    {
        // init HTML2PDF
        $html2pdf = new HTML2PDF('P', 'A4', 'es', true, 'UTF-8', array(0, 0, 0, 0));
        // display the full page
        $html2pdf->pdf->SetDisplayMode('fullpage');
        // convert
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        // send the PDF
        $html2pdf->Output('Factura.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>
<?php
ob_end_flush();
?>