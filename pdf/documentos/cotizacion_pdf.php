<?php
ob_start();
?>
<?php
	@session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: ../../login.php");
		exit;
    }
	
	
	/* Connect To Database*/
	include("../../config/db.php");
	include("../../config/conexion.php");
	$session_id= session_id();
	$sql_count=mysqli_query($con,"select * from tmp_cotizacion where session_id='".$session_id."'");
	$count=mysqli_num_rows($sql_count);
	if ($count==0)
	{
	echo "<script>alert('No hay productos agregados a la cotizacion')</script>";
	echo "<script>window.close();</script>";
	exit;
	}

	// require_once(dirname(__FILE__).'/../html2pdf.class.php');
	require __DIR__.'/vendor/autoload.php';

	use Spipu\Html2Pdf\Html2Pdf;

	$html2pdf = new Html2Pdf();
		
	//Variables por GET
	$id_cliente=intval($_GET['id_cliente']);
	$id_contacto=intval($_GET['id_contacto']);
	$condiciones=mysqli_real_escape_string($con,(strip_tags($_REQUEST['condiciones'], ENT_QUOTES)));
	$validez=mysqli_real_escape_string($con,(strip_tags($_REQUEST['validez'], ENT_QUOTES)));
	$entrega=mysqli_real_escape_string($con,(strip_tags($_REQUEST['entrega'], ENT_QUOTES)));
	$notas=mysqli_real_escape_string($con,(strip_tags($_REQUEST['notas'], ENT_QUOTES)));
	$monedas=mysqli_real_escape_string($con,(strip_tags($_REQUEST['moneda'], ENT_QUOTES)));
	//$anio=mysqli_real_escape_string($con,(strip_tags($_REQUEST['anio'], ENT_QUOTES)));
	//Fin de variables por GET
	$sql=mysqli_query($con, "select LAST_INSERT_ID(numero_cotizacion) as last from cotizacion order by id_cotizacion desc limit 0,1 ");
	$rw=mysqli_fetch_array($sql);
	$numero_cotizacion=$rw['last']+1;	
    // get the HTML
     ob_start();
     include(dirname('__FILE__').'/res/cotizacion_pdf_html.php');
    $content = ob_get_clean();

    try
    {
        // init HTML2PDF
        $html2pdf = new HTML2PDF('P', 'LETTER', 'es', true, 'UTF-8', array(0, 0, 0, 0));
        // display the full page
        $html2pdf->pdf->SetDisplayMode('fullpage');
        // convert
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        // send the PDF
        $html2pdf->Output('Factura.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>
<?php
ob_end_flush();
?>