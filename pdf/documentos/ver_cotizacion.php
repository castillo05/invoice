<?php
ob_start();
?>
<?php
	@session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: ../../login.php");
		exit;
    }
	/* Connect To Database*/
	include("../../config/db.php");
	include("../../config/conexion.php");
	$id_factura= intval($_GET['id_factura']);
	$sql_count=mysqli_query($con,"select * from cotizacion where id_cotizacion='".$id_factura."'");
	$count=mysqli_num_rows($sql_count);
	if ($count==0)
	{
	echo "<script>alert('Cotizacion no encontrada')</script>";
	echo "<script>window.close();</script>";
	exit;
	}
	$sql_cotizacion=mysqli_query($con,"select * from cotizacion where id_cotizacion='".$id_factura."'");
	$rw_cotizacion=mysqli_fetch_array($sql_cotizacion);
	$numero_cotizacion=$rw_cotizacion['numero_cotizacion'];
	$id_cliente=$rw_cotizacion['id_cliente'];
	$id_contacto=$rw_cotizacion['id_vendedor'];
	$fecha_cotizacion=$rw_cotizacion['fecha_cotizacion'];
	$condiciones=$rw_cotizacion['condiciones'];
	$validez=$rw_cotizacion['validez'];
	// require_once(dirname(__FILE__).'/../html2pdf.class.php');
	require __DIR__.'/vendor/autoload.php';

	use Spipu\Html2Pdf\Html2Pdf;

	$html2pdf = new Html2Pdf();
    // get the HTML
     ob_start();
     include(dirname('__FILE__').'/res/ver_cotizacion_html.php');
    $content = ob_get_clean();

    try
    {
        // init HTML2PDF
        $html2pdf = new HTML2PDF('P', 'LETTER', 'es', true, 'UTF-8', array(0, 0, 0, 0));
        // display the full page
        $html2pdf->pdf->SetDisplayMode('fullpage');
        // convert
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        // send the PDF
        $html2pdf->Output('Factura.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>
<?php
ob_end_flush();
?>