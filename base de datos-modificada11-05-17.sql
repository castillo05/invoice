-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-05-2017 a las 00:45:21
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `simple_invoice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(255) NOT NULL,
  `telefono_cliente` char(30) NOT NULL,
  `email_cliente` varchar(64) NOT NULL,
  `direccion_cliente` varchar(255) NOT NULL,
  `status_cliente` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `telefono_cliente`, `email_cliente`, `direccion_cliente`, `status_cliente`, `date_added`) VALUES
(11, 'Jorge Castillo Moreno', '86165414', 'jorgeantonio20142014@gmail.com', 'Del calvario 300 mts al este', 1, '2017-05-11 19:45:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `cod_compra` int(11) NOT NULL,
  `numero_factura` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `estado_compra` int(50) NOT NULL,
  `importe` float NOT NULL,
  `importe_pendiente` float NOT NULL,
  `vencimiento` date NOT NULL,
  `condiciones` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`cod_compra`, `numero_factura`, `fecha`, `proveedor`, `estado_compra`, `importe`, `importe_pendiente`, `vencimiento`, `condiciones`) VALUES
(1, '1', '2017-05-11', '2', 1, 1000, 1000, '2017-05-11', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id_cotizacion` int(11) NOT NULL,
  `numero_cotizacion` int(11) NOT NULL,
  `fecha_cotizacion` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` int(20) NOT NULL,
  `validez` int(20) NOT NULL,
  `entrega` varchar(50) NOT NULL,
  `nota` varchar(255) NOT NULL,
  `moneda` int(50) NOT NULL,
  `sub_total` float NOT NULL,
  `descuento` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_cobrar`
--

CREATE TABLE `cuentas_cobrar` (
  `id` int(20) NOT NULL,
  `num_fact` varchar(50) NOT NULL,
  `fecha_fact` varchar(20) NOT NULL,
  `vencimiento` varchar(20) NOT NULL,
  `monto` varchar(50) NOT NULL,
  `id_cliente` varchar(20) NOT NULL,
  `saldo_anterior` varchar(50) NOT NULL,
  `abono` varchar(50) NOT NULL,
  `saldo_actual` varchar(50) NOT NULL,
  `total_abono` varchar(50) NOT NULL,
  `mes` varchar(10) NOT NULL,
  `anio` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuentas_cobrar`
--

INSERT INTO `cuentas_cobrar` (`id`, `num_fact`, `fecha_fact`, `vencimiento`, `monto`, `id_cliente`, `saldo_anterior`, `abono`, `saldo_actual`, `total_abono`, `mes`, `anio`) VALUES
(1, '1', '2017-05-09', '2017-05-10', '1087.5', '11', '1312.5', '500', '162', '1600', '05/2017', '2017');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compra`
--

CREATE TABLE `detalle_compra` (
  `id_detalle` int(11) NOT NULL,
  `numero_factura` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  `cod_prov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_compra`
--

INSERT INTO `detalle_compra` (`id_detalle`, `numero_factura`, `id_producto`, `cantidad`, `precio_venta`, `cod_prov`) VALUES
(2, '1', 1489, 5, 200, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cotizacion`
--

CREATE TABLE `detalle_cotizacion` (
  `id_detalle` int(11) NOT NULL,
  `numero_cotizacion` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descuento_p` float NOT NULL,
  `iva_p` float NOT NULL,
  `precio_venta` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_factura`
--

CREATE TABLE `detalle_factura` (
  `id_detalle` int(11) NOT NULL,
  `numero_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descuento_p` float NOT NULL,
  `iva_p` float NOT NULL,
  `precio_venta` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_factura`
--

INSERT INTO `detalle_factura` (`id_detalle`, `numero_factura`, `id_producto`, `cantidad`, `descuento_p`, `iva_p`, `precio_venta`) VALUES
(268, 1, 1489, 3, 0, 112.5, 250);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id_factura` int(11) NOT NULL,
  `numero_factura` int(11) NOT NULL,
  `fecha_factura` date NOT NULL,
  `mes` varchar(20) NOT NULL,
  `anio` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` varchar(30) NOT NULL,
  `sub_total` varchar(20) NOT NULL,
  `iva` varchar(20) NOT NULL,
  `total_venta` varchar(20) NOT NULL,
  `estado_factura` tinyint(1) NOT NULL,
  `cheke` varchar(50) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `descuento` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id_factura`, `numero_factura`, `fecha_factura`, `mes`, `anio`, `id_cliente`, `id_vendedor`, `condiciones`, `sub_total`, `iva`, `total_venta`, `estado_factura`, `cheke`, `estado`, `descuento`) VALUES
(98, 1, '2017-05-11', '05/2017', 2017, 11, 4, '4', '750.00', '112.50', '862.5', 2, '', 'vigente', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex`
--

CREATE TABLE `kardex` (
  `cod_kardex` int(11) NOT NULL,
  `cod_producto` varchar(100) NOT NULL,
  `fecha_registro` date NOT NULL,
  `movimiento` int(11) NOT NULL,
  `num_documento` varchar(100) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `entradas` int(11) NOT NULL,
  `salidas` int(11) NOT NULL,
  `existencias` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `kardex`
--

INSERT INTO `kardex` (`cod_kardex`, `cod_producto`, `fecha_registro`, `movimiento`, `num_documento`, `descripcion`, `entradas`, `salidas`, `existencias`) VALUES
(61, '1489', '2017-05-11', 1, '1', 'Salidas', 0, 5, -5),
(62, '1489', '2017-05-11', 1, '1', 'Entradas', 5, 0, -5),
(63, '1489', '2017-05-11', 1, '1', 'salidas', 0, 3, -3),
(64, '1489', '2017-05-11', 1, '1', 'Entradas', 5, 0, -5),
(65, '1489', '2017-05-11', 1, '1', 'Salidas', 0, 5, -5),
(66, '1489', '2017-05-11', 1, '1', 'Entradas', 5, 0, -5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id_marca` int(11) NOT NULL,
  `nombre_marca` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id_marca`, `nombre_marca`) VALUES
(2, 'SHERWIN WILLIAMS'),
(3, 'PROTECTO'),
(4, 'TRUPER'),
(5, 'CORONA'),
(6, 'LANCO'),
(7, 'OUPONT'),
(8, 'BEST VALUE'),
(9, 'BOXER TOOLS'),
(10, 'PRETUL'),
(11, 'CONICA'),
(12, 'MEGAHOOK'),
(13, 'DIESEL TOOLS'),
(14, 'DOGLEAD'),
(15, 'HUNTER'),
(16, 'HERMEX '),
(17, 'STANLEY'),
(18, 'FIXWORX'),
(19, 'OUPONT'),
(20, 'AMERICAN-ELECTRIC'),
(21, 'EAGLE'),
(22, 'VOLTECH'),
(23, 'MA ELECTRIC'),
(24, 'BTICINO'),
(25, 'BRICKELL'),
(26, 'TRIPP-LITE'),
(27, 'TALTOOOS'),
(28, 'BRINK'),
(29, 'FIERRO'),
(30, 'DEVCON'),
(31, 'LOCTITE'),
(32, 'PHILIPS'),
(33, 'TRESB'),
(34, 'SKY LIGHT'),
(35, 'SUPER-FLEX'),
(36, 'RHODIUS'),
(37, 'UNIVERSAL CUT'),
(38, 'PREMIER'),
(39, 'MAKITA'),
(40, 'DEWALT'),
(41, 'CONINCA'),
(42, 'KORF & HONSBERG'),
(43, 'SYLVANIA'),
(44, 'TARTAN'),
(45, 'TOOLCRAFT'),
(46, 'NORTON'),
(47, 'YALE'),
(48, 'Nike');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_permisos` int(11) NOT NULL,
  `permisos` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permisos`, `permisos`) VALUES
(1, 'administrador'),
(2, 'vendedor');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `prod`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `prod` (
`id_producto` int(11)
,`codigo_producto` varchar(100)
,`nombre_producto` varchar(255)
,`unidad_med` int(50)
,`proveedor` int(100)
,`marca` int(255)
,`status_producto` tinyint(4)
,`date_added` datetime
,`precio_producto` double
,`precio_compra` int(11)
,`stock` int(11)
,`cantidad_total` int(11)
,`ex_min` int(11)
,`exento` int(11)
,`salida` int(11)
,`unidad_m` varchar(255)
,`marca_p` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id_producto` int(11) NOT NULL,
  `codigo_producto` varchar(100) NOT NULL,
  `nombre_producto` varchar(255) NOT NULL,
  `unidad_med` int(50) NOT NULL,
  `proveedor` int(100) NOT NULL,
  `marca` int(255) NOT NULL,
  `status_producto` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `precio_producto` double NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `cantidad_total` int(11) NOT NULL,
  `ex_min` int(11) NOT NULL,
  `exento` int(11) NOT NULL,
  `salida` int(11) NOT NULL,
  `unidad_m` varchar(255) NOT NULL,
  `marca_p` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id_producto`, `codigo_producto`, `nombre_producto`, `unidad_med`, `proveedor`, `marca`, `status_producto`, `date_added`, `precio_producto`, `precio_compra`, `stock`, `cantidad_total`, `ex_min`, `exento`, `salida`, `unidad_m`, `marca_p`) VALUES
(1489, '123456', 'ookoko', 7, 2, 2, 1, '2017-05-11 17:45:08', 250, 200, 14, 14, 1, 2, 0, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `cod_prov` int(11) NOT NULL,
  `cedula` varchar(25) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`cod_prov`, `cedula`, `nombre`, `direccion`, `telefono`, `celular`, `email`, `estado`) VALUES
(1, '2810302910010x', 'Proveedor General', 'El Sauce', '', '12345678', '', 1),
(2, 'gyybjnjknjniii99o', 'ferreteria jenny', 'Leon', '', '12345678', '', 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `prueba`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `prueba` (
`id_producto` int(11)
,`codigo_producto` varchar(100)
,`nombre_producto` varchar(255)
,`unidad_med` int(50)
,`proveedor` int(100)
,`marca` int(255)
,`status_producto` tinyint(4)
,`date_added` datetime
,`precio_producto` double
,`precio_compra` int(11)
,`stock` int(11)
,`cantidad_total` int(11)
,`ex_min` int(11)
,`exento` int(11)
,`salida` int(11)
,`unidad_m` varchar(255)
,`marca_p` varchar(255)
,`cod_prov` int(11)
,`cedula` varchar(25)
,`nombre` varchar(50)
,`direccion` varchar(100)
,`telefono` varchar(20)
,`celular` varchar(20)
,`email` varchar(100)
,`estado` int(11)
,`id_medida` int(11)
,`nombre_medida` varchar(255)
,`id_marca` int(11)
,`nombre_marca` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE `tmp` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8,2) DEFAULT NULL,
  `descuento_tmp` float NOT NULL,
  `iva_tmp` float NOT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_compra`
--

CREATE TABLE `tmp_compra` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double NOT NULL,
  `session_id` varchar(100) NOT NULL,
  `cod_prov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_cotizacion`
--

CREATE TABLE `tmp_cotizacion` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8,2) DEFAULT NULL,
  `descuento_tmp` float NOT NULL,
  `iva_tmp` float NOT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_medida`
--

CREATE TABLE `unidad_medida` (
  `id_medida` int(11) NOT NULL,
  `nombre_medida` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidad_medida`
--

INSERT INTO `unidad_medida` (`id_medida`, `nombre_medida`) VALUES
(2, 'Galon'),
(3, 'CUARTO'),
(4, '1/8'),
(5, '1/16'),
(6, 'UNIDAD'),
(7, 'CUBETA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL COMMENT 'auto incrementing user_id of each user, unique index',
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `date_added` datetime NOT NULL,
  `permiso_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `user_name`, `user_password_hash`, `user_email`, `date_added`, `permiso_user`) VALUES
(1, 'Jorge', 'Castillo', 'admin', '$2y$10$SS6MCqM7XMzya86JcXjKSOZaxYYGVJW6dYlaGka/xup2M8IRaIcUK', 'admin@admin.com', '2016-05-21 15:06:00', 1),
(2, 'Antonio', 'Moreno', 'Antonio', '$2y$10$uObwQIkIsD0jsI2VRyXYoOf8hp8RmE8uw5XQmQLpUDnEHMCL2dtn2', 'antonio@gmail.com', '2016-09-05 15:41:18', 2),
(3, 'FERRETERIA', 'EL ESFUERZO', 'VENDEDOR', '$2y$10$MeA091YuJ6Zs7wdBF/XrSOgvL2GV3HyAY3efLBSNxIsS5/TAq31WC', 'vendedor@controltotal.com', '2016-12-18 10:01:16', 2),
(4, 'Administrador', 'EL ESFUERZO', 'admon', '$2y$10$jqguraiY3PHmZ1rUx4u0n.QPRuFXRL62iL5j/aLLB2iSi/Cbkz/9m', 'ferreteriaelesfuerzo@outlook.es', '2016-12-18 11:35:52', 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `prod`
--
--DROP TABLE IF EXISTS `prod`;

--CREATE ALGORITHM=UNDEFINED DEFINER=`productos`@`%` SQL SECURITY DEFINER VIEW `prod`  AS  select `products`.`id_producto` AS `id_producto`,`products`.`codigo_producto` AS `codigo_producto`,`products`.`nombre_producto` AS `nombre_producto`,`products`.`unidad_med` AS `unidad_med`,`products`.`proveedor` AS `proveedor`,`products`.`marca` AS `marca`,`products`.`status_producto` AS `status_producto`,`products`.`date_added` AS `date_added`,`products`.`precio_producto` AS `precio_producto`,`products`.`precio_compra` AS `precio_compra`,`products`.`stock` AS `stock`,`products`.`cantidad_total` AS `cantidad_total`,`products`.`ex_min` AS `ex_min`,`products`.`exento` AS `exento`,`products`.`salida` AS `salida`,`products`.`unidad_m` AS `unidad_m`,`products`.`marca_p` AS `marca_p` from `products` WITH LOCAL CHECK OPTION ;

-- --------------------------------------------------------

--
-- Estructura para la vista `prueba`
--
--DROP TABLE IF EXISTS `prueba`;

--CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `prueba`  AS  select `products`.`id_producto` AS `id_producto`,`products`.`codigo_producto` AS `codigo_producto`,`products`.`nombre_producto` AS `nombre_producto`,`products`.`unidad_med` AS `unidad_med`,`products`.`proveedor` AS `proveedor`,`products`.`marca` AS `marca`,`products`.`status_producto` AS `status_producto`,`products`.`date_added` AS `date_added`,`products`.`precio_producto` AS `precio_producto`,`products`.`precio_compra` AS `precio_compra`,`products`.`stock` AS `stock`,`products`.`cantidad_total` AS `cantidad_total`,`products`.`ex_min` AS `ex_min`,`products`.`exento` AS `exento`,`products`.`salida` AS `salida`,`products`.`unidad_m` AS `unidad_m`,`products`.`marca_p` AS `marca_p`,`proveedores`.`cod_prov` AS `cod_prov`,`proveedores`.`cedula` AS `cedula`,`proveedores`.`nombre` AS `nombre`,`proveedores`.`direccion` AS `direccion`,`proveedores`.`telefono` AS `telefono`,`proveedores`.`celular` AS `celular`,`proveedores`.`email` AS `email`,`proveedores`.`estado` AS `estado`,`unidad_medida`.`id_medida` AS `id_medida`,`unidad_medida`.`nombre_medida` AS `nombre_medida`,`marcas`.`id_marca` AS `id_marca`,`marcas`.`nombre_marca` AS `nombre_marca` from (((`products` join `proveedores` on((`proveedores`.`cod_prov` = `products`.`proveedor`))) join `unidad_medida` on((`unidad_medida`.`id_medida` = `products`.`unidad_med`))) join `marcas` on((`marcas`.`id_marca` = `products`.`marca`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `codigo_producto` (`nombre_cliente`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`cod_compra`);

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- Indices de la tabla `cuentas_cobrar`
--
ALTER TABLE `cuentas_cobrar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indices de la tabla `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indices de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `numero_cotizacion` (`numero_factura`,`id_producto`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id_factura`),
  ADD UNIQUE KEY `numero_cotizacion` (`numero_factura`);

--
-- Indices de la tabla `kardex`
--
ALTER TABLE `kardex`
  ADD PRIMARY KEY (`cod_kardex`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permisos`),
  ADD KEY `permisos` (`permisos`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_producto`),
  ADD UNIQUE KEY `codigo_producto` (`codigo_producto`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`cod_prov`);

--
-- Indices de la tabla `tmp`
--
ALTER TABLE `tmp`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `tmp_compra`
--
ALTER TABLE `tmp_compra`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `tmp_cotizacion`
--
ALTER TABLE `tmp_cotizacion`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  ADD PRIMARY KEY (`id_medida`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD KEY `permiso_user` (`permiso_user`),
  ADD KEY `permiso_user_2` (`permiso_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `cod_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cuentas_cobrar`
--
ALTER TABLE `cuentas_cobrar`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;
--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT de la tabla `kardex`
--
ALTER TABLE `kardex`
  MODIFY `cod_kardex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1490;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `cod_prov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tmp`
--
ALTER TABLE `tmp`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1050;
--
-- AUTO_INCREMENT de la tabla `tmp_compra`
--
ALTER TABLE `tmp_compra`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tmp_cotizacion`
--
ALTER TABLE `tmp_cotizacion`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=858;
--
-- AUTO_INCREMENT de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  MODIFY `id_medida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index', AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
