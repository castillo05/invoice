-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-12-2016 a las 22:00:47
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;
/*!40101 SET NAMES utf8 */
;
--
-- Base de datos: `simple_invoice`
--

-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(255) NOT NULL,
  `telefono_cliente` char(30) NOT NULL,
  `email_cliente` varchar(64) NOT NULL,
  `direccion_cliente` varchar(255) NOT NULL,
  `status_cliente` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id_cliente`),
  UNIQUE KEY `codigo_producto` (`nombre_cliente`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 AUTO_INCREMENT = 10;
--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (
    `id_cliente`,
    `nombre_cliente`,
    `telefono_cliente`,
    `email_cliente`,
    `direccion_cliente`,
    `status_cliente`,
    `date_added`
  )
VALUES (
    1,
    'jorge castillo',
    '86165414',
    'ciberclub.club@gmail.com',
    '',
    1,
    '2016-08-25 23:24:04'
  ),
  (
    4,
    'jose',
    '123456',
    'jose@gmail.com',
    'el sauce',
    1,
    '2016-09-02 20:51:50'
  ),
  (
    3,
    'Pedro Pastora',
    '12345678',
    '',
    '',
    1,
    '2016-08-29 22:01:16'
  ),
  (
    5,
    'Emilio',
    '12345678',
    'emilio@gmail.com',
    'edfdfdf',
    0,
    '2016-09-09 15:58:28'
  ),
  (
    6,
    'marcos castillo',
    '',
    '',
    '',
    1,
    '2016-09-14 19:32:01'
  ),
  (
    7,
    'jorbely castillo',
    '86165414',
    '',
    '',
    1,
    '2016-09-17 04:46:00'
  ),
  (
    8,
    'MARIA ELENA MORENO CHAVARRIA',
    '89315230',
    '',
    'DEL CALVARIO 3C AL ESTE',
    1,
    '2016-09-22 20:44:02'
  ),
  (
    9,
    'Cliente General',
    '',
    '',
    '',
    1,
    '2016-10-03 15:05:33'
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE IF NOT EXISTS `compras` (
  `cod_compra` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `estado_compra` int(50) NOT NULL,
  `importe` float NOT NULL,
  `importe_pendiente` float NOT NULL,
  `vencimiento` date NOT NULL,
  `condiciones` int(11) NOT NULL,
  PRIMARY KEY (`cod_compra`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 8;
--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (
    `cod_compra`,
    `numero_factura`,
    `fecha`,
    `proveedor`,
    `estado_compra`,
    `importe`,
    `importe_pendiente`,
    `vencimiento`,
    `condiciones`
  )
VALUES (
    7,
    '1',
    '2016-12-02',
    '5',
    1,
    36000,
    36000,
    '2016-12-02',
    4
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE IF NOT EXISTS `cotizacion` (
  `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT,
  `numero_cotizacion` int(11) NOT NULL,
  `fecha_cotizacion` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` int(20) NOT NULL,
  `validez` int(20) NOT NULL,
  `entrega` varchar(50) NOT NULL,
  `nota` varchar(255) NOT NULL,
  `moneda` int(50) NOT NULL,
  `sub_total` float NOT NULL,
  `descuento` float NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_cotizacion`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 4;
--
-- Volcado de datos para la tabla `cotizacion`
--

INSERT INTO `cotizacion` (
    `id_cotizacion`,
    `numero_cotizacion`,
    `fecha_cotizacion`,
    `id_cliente`,
    `id_vendedor`,
    `condiciones`,
    `validez`,
    `entrega`,
    `nota`,
    `moneda`,
    `sub_total`,
    `descuento`,
    `total`
  )
VALUES (
    3,
    1,
    '2016-12-02',
    8,
    1,
    2,
    3,
    'Inmediato',
    '',
    1,
    13365,
    135,
    15369.8
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `cuentas_cobrar`
--

CREATE TABLE IF NOT EXISTS `cuentas_cobrar` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `num_fact` varchar(50) NOT NULL,
  `fecha_fact` varchar(20) NOT NULL,
  `vencimiento` varchar(20) NOT NULL,
  `monto` varchar(50) NOT NULL,
  `id_cliente` varchar(20) NOT NULL,
  `saldo_anterior` varchar(50) NOT NULL,
  `abono` varchar(50) NOT NULL,
  `saldo_actual` varchar(50) NOT NULL,
  `total_abono` varchar(50) NOT NULL,
  `mes` varchar(10) NOT NULL,
  `anio` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 10;
--
-- Volcado de datos para la tabla `cuentas_cobrar`
--

INSERT INTO `cuentas_cobrar` (
    `id`,
    `num_fact`,
    `fecha_fact`,
    `vencimiento`,
    `monto`,
    `id_cliente`,
    `saldo_anterior`,
    `abono`,
    `saldo_actual`,
    `total_abono`,
    `mes`,
    `anio`
  )
VALUES (
    9,
    '1',
    '2016-12-02',
    '2016-12-17',
    '13095',
    '9',
    '13095',
    '',
    '13095',
    '',
    '12/2016',
    '2016'
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `detalle_compra`
--

CREATE TABLE IF NOT EXISTS `detalle_compra` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  `cod_prov` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 10;
--
-- Volcado de datos para la tabla `detalle_compra`
--

INSERT INTO `detalle_compra` (
    `id_detalle`,
    `numero_factura`,
    `id_producto`,
    `cantidad`,
    `precio_venta`,
    `cod_prov`
  )
VALUES (9, '1', 606, 3, 12000, 5);
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `detalle_cotizacion`
--

CREATE TABLE IF NOT EXISTS `detalle_cotizacion` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `numero_cotizacion` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 4;
--
-- Volcado de datos para la tabla `detalle_cotizacion`
--

INSERT INTO `detalle_cotizacion` (
    `id_detalle`,
    `numero_cotizacion`,
    `id_producto`,
    `cantidad`,
    `precio_venta`
  )
VALUES (3, '1', 606, 1, 13365);
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `detalle_factura`
--

CREATE TABLE IF NOT EXISTS `detalle_factura` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `numero_cotizacion` (`numero_factura`, `id_producto`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 AUTO_INCREMENT = 86;
--
-- Volcado de datos para la tabla `detalle_factura`
--

INSERT INTO `detalle_factura` (
    `id_detalle`,
    `numero_factura`,
    `id_producto`,
    `cantidad`,
    `precio_venta`
  )
VALUES (85, 1, 606, 1, 13095);
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE IF NOT EXISTS `facturas` (
  `id_factura` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `fecha_factura` date NOT NULL,
  `mes` varchar(20) NOT NULL,
  `anio` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` varchar(30) NOT NULL,
  `sub_total` varchar(20) NOT NULL,
  `iva` varchar(20) NOT NULL,
  `total_venta` varchar(20) NOT NULL,
  `estado_factura` tinyint(1) NOT NULL,
  `cheke` varchar(50) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `descuento` float NOT NULL,
  PRIMARY KEY (`id_factura`),
  UNIQUE KEY `numero_cotizacion` (`numero_factura`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 AUTO_INCREMENT = 48;
--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (
    `id_factura`,
    `numero_factura`,
    `fecha_factura`,
    `mes`,
    `anio`,
    `id_cliente`,
    `id_vendedor`,
    `condiciones`,
    `sub_total`,
    `iva`,
    `total_venta`,
    `estado_factura`,
    `cheke`,
    `estado`,
    `descuento`
  )
VALUES (
    47,
    1,
    '2016-12-02',
    '12/2016',
    2016,
    9,
    1,
    '4',
    '13095.00',
    '0.00',
    '13095',
    2,
    '',
    'vigente',
    405
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `kardex`
--

CREATE TABLE IF NOT EXISTS `kardex` (
  `cod_kardex` int(11) NOT NULL AUTO_INCREMENT,
  `cod_producto` varchar(100) NOT NULL,
  `fecha_registro` date NOT NULL,
  `movimiento` int(11) NOT NULL,
  `num_documento` varchar(100) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `entradas` int(11) NOT NULL,
  `salidas` int(11) NOT NULL,
  `existencias` int(11) NOT NULL,
  PRIMARY KEY (`cod_kardex`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 17;
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE IF NOT EXISTS `marcas` (
  `id_marca` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_marca` varchar(255) NOT NULL,
  `id_prov` int(11) NOT NULL,
  PRIMARY KEY (`id_marca`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 1;
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `id_permisos` int(11) NOT NULL,
  `permisos` varchar(25) NOT NULL,
  PRIMARY KEY (`id_permisos`),
  KEY `permisos` (`permisos`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;
--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permisos`, `permisos`)
VALUES (1, 'administrador'),
  (2, 'vendedor');
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_producto` varchar(100) NOT NULL,
  `nombre_producto` varchar(255) NOT NULL,
  `unidad_med` varchar(50) NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `status_producto` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `precio_producto` double NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `cantidad_total` int(11) NOT NULL,
  `ex_min` int(11) NOT NULL,
  `exento` int(11) NOT NULL,
  `salida` int(11) NOT NULL,
  `unidad_m` varchar(255) NOT NULL,
  `marca_p` varchar(255) NOT NULL,
  PRIMARY KEY (`id_producto`),
  UNIQUE KEY `codigo_producto` (`codigo_producto`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 AUTO_INCREMENT = 608;
--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (
    `id_producto`,
    `codigo_producto`,
    `nombre_producto`,
    `unidad_med`,
    `proveedor`,
    `status_producto`,
    `date_added`,
    `precio_producto`,
    `precio_compra`,
    `stock`,
    `cantidad_total`,
    `ex_min`,
    `exento`,
    `salida`,
    `unidad_m`,
    `marca_p`
  )
VALUES (
    607,
    'A12',
    'Telefono blu',
    '',
    'FETESA',
    1,
    '2016-12-02 17:08:35',
    3200,
    2000,
    0,
    0,
    2,
    1,
    0,
    '',
    ''
  ),
  (
    606,
    '7129213123',
    'IMPRESORA SAMSUNG 2585F',
    '',
    'FERRETERIA JENNY',
    1,
    '2016-12-02 16:06:23',
    13500,
    12000,
    2,
    3,
    2,
    1,
    0,
    '',
    ''
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `cod_prov` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(25) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`cod_prov`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 6;
--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (
    `cod_prov`,
    `cedula`,
    `nombre`,
    `direccion`,
    `telefono`,
    `celular`,
    `email`,
    `estado`
  )
VALUES (
    3,
    'J2389977489',
    'MASESA S.A',
    '',
    '8973-9098',
    '',
    'jorbelia18062013@outlook.com',
    1
  ),
  (
    4,
    'J7878756gh',
    'FETESA',
    'LEON',
    '1234-5678',
    '1234-5678',
    'tecnicouriel@gmail.com',
    1
  ),
  (
    5,
    'J6767454566',
    'FERRETERIA JENNY',
    'LEON',
    '1234-5678-',
    '1234-5678',
    'tecnicouriel@gmail.com',
    1
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE IF NOT EXISTS `tmp` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8, 2) DEFAULT NULL,
  `descuento_tmp` float NOT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tmp`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci AUTO_INCREMENT = 879;
--
-- Volcado de datos para la tabla `tmp`
--

INSERT INTO `tmp` (
    `id_tmp`,
    `id_producto`,
    `cantidad_tmp`,
    `precio_tmp`,
    `descuento_tmp`,
    `session_id`
  )
VALUES (
    832,
    27,
    1,
    20.00,
    0,
    'h3m0kh0s3fkp9eh3nt10c30bc7'
  ),
  (
    834,
    26,
    1,
    350.00,
    0,
    'h3m0kh0s3fkp9eh3nt10c30bc7'
  ),
  (
    835,
    26,
    1,
    350.00,
    0,
    'h3m0kh0s3fkp9eh3nt10c30bc7'
  ),
  (
    836,
    27,
    1,
    20.00,
    0,
    'h3m0kh0s3fkp9eh3nt10c30bc7'
  ),
  (
    849,
    25,
    1,
    8075.00,
    425,
    'h731o7lk0te62c7hbscihjo246'
  ),
  (
    851,
    27,
    1,
    19.00,
    1,
    'h731o7lk0te62c7hbscihjo246'
  ),
  (
    852,
    25,
    1,
    8075.00,
    425,
    'h731o7lk0te62c7hbscihjo246'
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `tmp_compra`
--

CREATE TABLE IF NOT EXISTS `tmp_compra` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double NOT NULL,
  `session_id` varchar(100) NOT NULL,
  `cod_prov` int(11) NOT NULL,
  PRIMARY KEY (`id_tmp`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 14;
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `tmp_cotizacion`
--

CREATE TABLE IF NOT EXISTS `tmp_cotizacion` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8, 2) DEFAULT NULL,
  `descuento_tmp` float NOT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tmp`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci AUTO_INCREMENT = 818;
--
-- Volcado de datos para la tabla `tmp_cotizacion`
--

INSERT INTO `tmp_cotizacion` (
    `id_tmp`,
    `id_producto`,
    `cantidad_tmp`,
    `precio_tmp`,
    `descuento_tmp`,
    `session_id`
  )
VALUES (
    810,
    25,
    1,
    8075.00,
    425,
    'h731o7lk0te62c7hbscihjo246'
  ),
  (
    809,
    25,
    1,
    8075.00,
    425,
    'h731o7lk0te62c7hbscihjo246'
  );
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `unidad_medida`
--

CREATE TABLE IF NOT EXISTS `unidad_medida` (
  `id_medida` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_medida` varchar(255) NOT NULL,
  PRIMARY KEY (`id_medida`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT = 3;
--
-- Volcado de datos para la tabla `unidad_medida`
--

INSERT INTO `unidad_medida` (`id_medida`, `nombre_medida`)
VALUES (2, 'Galon');
-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index',
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `date_added` datetime NOT NULL,
  `permiso_user` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`),
  KEY `permiso_user` (`permiso_user`),
  KEY `permiso_user_2` (`permiso_user`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci COMMENT = 'user data' AUTO_INCREMENT = 3;
--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (
    `user_id`,
    `firstname`,
    `lastname`,
    `user_name`,
    `user_password_hash`,
    `user_email`,
    `date_added`,
    `permiso_user`
  )
VALUES (
    1,
    'Jorge',
    'Castillo',
    'admin',
    '$2y$10$MPVHzZ2ZPOWmtUUGCq3RXu31OTB.jo7M9LZ7PmPQYmgETSNn19ejO',
    'admin@admin.com',
    '2016-05-21 15:06:00',
    1
  ),
  (
    2,
    'Antonio',
    'Moreno',
    'Antonio',
    '$2y$10$uObwQIkIsD0jsI2VRyXYoOf8hp8RmE8uw5XQmQLpUDnEHMCL2dtn2',
    'antonio@gmail.com',
    '2016-09-05 15:41:18',
    2
  );
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;