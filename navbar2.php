<?php 
//include('config/db.php');
//include('config/conexion.php');
// $contar=mysqli_query($con,"select count(stock) from products where stock=0");
// $row=mysqli_fetch_array($contar,MYSQLI_ASSOC);
// $cuenta=$row['count(stock)'];

 ?>
<header class="main-header">

    <!-- Logo -->
    <a href="panel_de_control.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CN</b>T</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Control</b>Total</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>


      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
             
                <span class="label label-success"></span>
            </a>
             
              
            
            <ul class="dropdown-menu">
              <li class="header"></li>
              <li>
                <!-- inner menu: contains the messages -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <!-- User Image -->
                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <!-- Message title and timestamp -->
                      <h4>
                        
                        <small><i class="fa fa-clock-o"></i> </small>
                      </h4>
                      <!-- The message -->
                      <p></p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
                <!-- /.menu -->
              </li>
              <!-- <li class="footer"><a href="#">See All Messages</a></li> -->
            </ul>

          

             

              <span class="label label-success"></span>
            </a>
             
              
            
            <ul class="dropdown-menu">
              <li class="header">Usted tiene  productos sin existencias</li>
              <li>
                <!-- inner menu: contains the messages -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="productos.php">
                      <div class="pull-left">
                        <!-- User Image -->
                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <!-- Message title and timestamp -->
                      <h4>
                        Por Favor
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <!-- The message -->
                      <p>Actualice estos productos </p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
                <!-- /.menu -->
              </li>
              <!-- <li class="footer"><a href="#">See All Messages</a></li> -->
            </ul>
            




          </li>
          <!-- /.messages-menu -->
            
          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <li class="dropdown tasks-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- Inner menu: contains the tasks -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <!-- Task title and progress text -->
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <!-- The progress bar -->
                      <div class="progress xs">
                        <!-- Change the css width attribute to simulate progress -->
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo $_SESSION['user_name']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['user_name']; ?>
                  <small><?php echo date('d/m/y'); ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="login.php?logout" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['user_name']; ?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">Menu</li>
        <?php

        if ($_SESSION['permiso_user']== 1) {?>
        <!-- Optionally, you can add icons to the links -->
       

        <li class="treeview">
          <a href="#"><i class="fa fa-barcode"></i> <span>Facturas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="facturas.php"><i class="glyphicon glyphicon-th-list"></i>Factura Contado</a></li>
            <li><a href="facturas_credito.php"><i class="glyphicon glyphicon-plus"></i>Facturas Credito</a></li>
          </ul>
        </li>

       <li class="treeview">
          <a href="#"><i class="fa fa-barcode"></i> <span>Inventario</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="productos.php"><i class="glyphicon glyphicon-th-list"></i>Inventario</a></li>
            <li><a href="nuevo_producto.php"><i class="glyphicon glyphicon-plus"></i>Nuevo Producto</a></li>
            <li><a href="nueva_marca.php"><i class="glyphicon glyphicon-plus"></i>Nueva Marca</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-phone-alt"></i> <span>Contactos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="clientes.php"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
            <li><a href="proveedores.php"><i class="fa fa-users"></i> <span>Proveedores</span></a></li>

          </ul>
        </li>
        
        <li class="treeview">
          <a href="#"><i class="fa fa-area-chart"></i> <span>Reportes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="reporte_ventas.php"><i class="fa fa-line-chart"></i>Reporte Detallado</a></li>
            <li><a href="reporte_ventas_fecha.php"><i class="fa fa-pie-chart"></i>Reporte por Fechas</a></li>
            <li><a href="reporte_cobro.php"><i class="fa fa-pie-chart"></i>Reporte de Cobro</a></li>
            <li><a href="reporte_cuenta.php"><i class="fa fa-pie-chart"></i>Reporte de Cuentas Vencidas</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon glyphicon-book"></i> <span>Tramites</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="cuentas.php"><i class="fa fa-cc-visa"></i>Cuentas</a></li>
            <li><a href="#"></a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-usd"></i> <span>Transacciones</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a  href="nueva_factura.php"><i class="fa fa-truck"></i>Ventas</a></li>
            <li><a href="compras.php"><i class="fa fa-cart-plus"></i>Compras</a></li>
             <li><a href="cotizaciones.php"><i class="fa fa-cart-plus"></i>Cotizaciones</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-cog"></i> <span>Configuracion</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="usuarios.php"><i class="fa fa-user-plus"></i>Usuarios</a></li>
            <li><a href="#"></a></li>
          </ul>
        </li>

        <?php
        }else{
          ?>

               <li class="treeview">
          <a href="#"><i class="fa fa-barcode"></i> <span>Facturas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="facturas.php"><i class="glyphicon glyphicon-th-list"></i>Factura Contado</a></li>
            <li><a href="facturas_credito.php"><i class="glyphicon glyphicon-plus"></i>Facturas Credito</a></li>
          </ul>
        </li>


         <li class="treeview">
          <a href="#"><i class="fa fa-barcode"></i> <span>Inventario</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="productos.php"><i class="glyphicon glyphicon-th-list"></i>Inventario</a></li>
            <li><a href="nuevo_producto.php"><i class="glyphicon glyphicon-plus"></i>Nuevo Producto</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-usd"></i> <span>Transacciones</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a  href="nueva_factura.php"><i class="fa fa-truck"></i>Ventas</a></li>
           
             <li><a href="cotizaciones.php"><i class="fa fa-cart-plus"></i>Cotizaciones</a></li>
          </ul>
        </li>

          <?php
        }

       ?>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>