<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: login.php");
		exit;
        }
        
	$active_facturas="active";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Editar Factura | Control Total";
	
	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	if (isset($_GET['id']))
	{
		$id=intval($_GET['id']);
		$factura=intval($_GET['numero_factura']);
		$campos="clientes.id_cliente, clientes.nombre_cliente, clientes.telefono_cliente, clientes.email_cliente, facturas.id_vendedor, facturas.fecha_factura, facturas.condiciones, facturas.estado_factura, facturas.numero_factura,facturas.cheke";
		$sql_factura=mysqli_query($con,"select * from facturas,clientes,cuentas_cobrar where facturas.id_cliente=cuentas_cobrar.id_cliente and  cuentas_cobrar.id_cliente=clientes.id_cliente and id='".$id."' and numero_factura='".$factura."'");
		$count=mysqli_num_rows($sql_factura);
		if ($count==1)
		{
				$row=mysqli_fetch_array($sql_factura);
						$id=$row['id'];
						$numero_factura=$row['num_fact'];
						$fecha=date("d/m/Y", strtotime($row['fecha_fact']));
						$vencimiento=$row['vencimiento'];
						$nombre_cliente=$row['nombre_cliente'];
						$email_cliente=$row['email_cliente'];
						$telefono_cliente=$row['telefono_cliente'];
						$monto=$row['monto'];
						
						$estado_factura=$row['estado_factura'];
						$saldo_anterior=$row['saldo_anterior'];
						$abono=$row['abono'];
						$saldo_actual=$row['saldo_actual'];
						$total_abonado=$row['total_abono'];

		}	
		else
		{
			header("");
			exit;	
		}
	} 
	else 
	{
		header("location: cuentas.php");
		exit;
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php");?>
    <style type="text/css">
    	
    	#update{
    		visibility: hidden;
    	}
    </style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	?>  
	<div class="content-wrapper">
   
	<div class="panel panel-info">
		<div class="panel-heading">
			<h4><i class='glyphicon glyphicon-edit'></i> Editar Cuentas</h4>
		</div>
		<div class="panel-body">
		<?php 
			// include("modal/buscar_productos.php");
			// include("modal/registro_clientes.php");
			// include("modal/registro_productos.php");
		?>
			<form class="form-horizontal" role="form" id="datos_cuenta">
				<div class="form-group row">
				  <label for="nombre_cliente" class="col-md-2 control-label">Cliente</label>
				  <div class="col-md-2">
					  <input type="text" class="form-control input-sm" id="nombre_cliente" placeholder="Selecciona un cliente" required value="<?php echo $nombre_cliente;?>">
					  <input id="id" name="id" type='hidden' value="<?php echo $id;?>">	
				  </div>
				  <label for="tel1" class="col-md-2 control-label">Nº Factura</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="factura" name="factura" placeholder="factura" value="<?php echo $numero_factura;?>" readonly>
							</div>


							<label for="fecha_compra" class="col-md-2 control-label">Fecha de Compra</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="fecha_compra" placeholder="Fecha de compra" readonly value="<?php echo $fecha;?>">
							</div>

				</div>


				<div class="form-group row">		
							<label for="estado_factura" class="col-md-2 control-label">Estado Factura</label>
							<div class="col-md-2">
								<select class='form-control input-sm ' id="estado_factura" name="estado_factura">
									<option value="1" <?php if ($estado_factura==1){echo "selected";}?>>Pagado</option>
									<option value="2" <?php if ($estado_factura==2){echo "selected";}?>>Pendiente</option>
									<option value="3" <?php if ($estado_factura==3){echo "selected";}?>>Vencida</option>
										<option value="4" <?php if ($estado_factura==4){echo "selected";}?>>Anulada</option>
								</select>
							</div>
							<label for="monto" class="col-md-2 control-label">Capital</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="monto" name="monto" placeholder="monto" readonly value="<?php echo $monto;?>">
							</div>

							<label for="fecha_vencimiento" class="col-md-2 control-label">Fecha de Vencimiento</label>
							<div class="col-md-2">
								<input type="text" class="form-control input-sm" id="fecha_vencimiento" placeholder="Fecha de vencimiento" readonly value="<?php echo $vencimiento;?>">
							</div>
				</div>
					

				 
						<div class="form-group row">

						
							

							<label for="abono" class="col-md-2 control-label">Abono</label>
							
									<div class="col-md-2">
										<input type="text" class="form-control input-sm" name="abono"  id="abono" placeholder="Abono" value="">
									</div>	

							<label for="total_abonado" class="col-md-2 control-label"> Total Abonado</label>
							
									<div class="col-md-2">
										<input type="text" class="form-control input-sm" name="total_abonado"  id="total_abonado" readonly placeholder="total_abonado" value="<?php echo $total_abonado;?>">
									</div>

							<label for="saldo_actual" class="col-md-2 control-label">Saldo Actual</label>
							
									<div class="col-md-2">
										<input type="text" class="form-control input-sm" name="saldo_actual" id="saldo_actual" placeholder="saldo_actual" value="<?php echo $saldo_actual;?>">
									</div>	
							

						</div>

							
				
				
				<div class="col-md-12">
					<div class="pull-right">
						<button type="submit" class="btn btn-default" id="update">
						  <span class="glyphicon glyphicon-refresh"></span> Actualizar datos
						</button>
						<button type="button" class="btn btn-default" onclick="operaciones('restar'); return false;">
						 <span class="glyphicon glyphicon-plus"></span> Calcular Saldo
						</button>
						
						
						<button type="button" class="btn btn-default" onclick="imprimir_cuenta('<?php echo $id;?>')">
						  <span class="glyphicon glyphicon-print"></span> Imprimir
						</button>
					</div>	
				</div>
			</form>	
			<div class="clearfix"></div>
				<div class="editar_cuenta" class='col-md-12' style="margin-top:10px"></div><!-- Carga los datos ajax -->	
			
		<!--<div id="resultados" class='col-md-12' style="margin-top:10px"></div> Carga los datos ajax -->			
			
		</div>
	</div>		
		 
	
	</div>
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	<script type="text/javascript" src="js/editar_cuenta.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	

  </body>
</html>