<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
		exit;
        }

	
	$active_facturas="active";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Facturas | Control Total";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<?php include("head.php");?>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	?>  
<div class="content-wrapper">
    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Compras</h3>
              <div class="btn-group pull-right">
        <a href="agregar_compra.php"><span class="glyphicon glyphicon-plus" ></span> Agregar Compra</a>
      </div>
            </div>

          
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group col-md-3">
                  <label for="proveedor"># Factura</label>
                  <input type="text" class="form-control" id="q" placeholder="# Factura" onkeyup='load(1);'>
                </div>
                <div class="form-group col-md-3">
                  <label for="proveedor">Proveedor</label>
                  <input type="text" class="form-control" id="q2" placeholder="Proveedor" onkeyup='load(1);'>
                </div>
                <div class="form-group col-md-3">
                  <label for="proveedor">Estado de Compra</label>
                  <select name="estado" id="q3" class="form-control" onchange='load(1);'>
                    <option value="">Eliga una opcion</option>
                    <option value="4">Cancelada</option>
                    <option value="5">Pendiente</option>
                    <option value="6">Vencida</option>
                    
                    
                  </select>
                </div>
                
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
               <div class="col-md-3">
                <button type="button" class="btn btn-default" onclick='load(1);'>
                  <span class="glyphicon glyphicon-search" ></span> Buscar</button>
                <span id="loader"></span>
              </div>
              </div>
            </form>

            <div id="resultados"></div><!-- Carga los datos ajax -->
				    <div class='outer_div'></div><!-- Carga los datos ajax -->
          </div>
    		</div>
    	</div>
    </section>
	</div>

	
    
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	<script type="text/javascript" src="js/compras.js"></script>
  </body>
</html>

