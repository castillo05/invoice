<?php
 session_start();
    if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
        exit;
        }
				require_once("config/db.php");
				require_once("config/conexion.php");

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Control Total</title>

		<script type="text/javascript" src="js/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		
			
			<?php

            $anio=date('Y');

            $suma=mysqli_query($con,"select sum(total_venta) as total from facturas where mes='01/2016' and estado='vigente'");
            $row=mysqli_fetch_array($suma,MYSQLI_ASSOC);
            $total=$row['total'];
            if ($total==null) {
                # code...
                $total=0;
            }

            $suma2=mysqli_query($con,"select sum(total_venta) as total2 from facturas where mes='02/2016' and estado='vigente'");
            $row2=mysqli_fetch_array($suma2,MYSQLI_ASSOC);
            $total2=$row2['total2'];
            if ($total2==null) {
                # code...
                $total2=0;
            }

            $suma3=mysqli_query($con,"select sum(total_venta) as total3 from facturas where mes='03/2016' and estado='vigente'");
            $row3=mysqli_fetch_array($suma3,MYSQLI_ASSOC);
             $total3=$row3['total3'];
            if ($total3==null) {
                # code...
                $total3=0;
            }

            $suma4=mysqli_query($con,"select sum(total_venta) as total4 from facturas where mes='04/2016' and estado='vigente'");
            $row4=mysqli_fetch_array($suma4,MYSQLI_ASSOC);
            $total4=$row4['total4'];
            if ($total4==null) {
                # code...
                $total4=0;
            }

            $suma5=mysqli_query($con,"select sum(total_venta) as total5 from facturas where mes='05/2016' and estado='vigente'");
            $row5=mysqli_fetch_array($suma,MYSQLI_ASSOC);
            $total5=$row5['total5'];
            if ($total5==null) {
                # code...
                $total5=0;
            }

            $suma6=mysqli_query($con,"select sum(total_venta) as total6 from facturas where mes='06/2016' and estado='vigente'");
            $row6=mysqli_fetch_array($suma6,MYSQLI_ASSOC);
            $total6=$row6['total6'];
            if ($total6==null) {
                # code...
                $total6=0;
            }

            $suma7=mysqli_query($con,"select sum(total_venta) as total7 from facturas where mes='07/2016' and estado='vigente'");
            $row7=mysqli_fetch_array($suma7,MYSQLI_ASSOC);
            $total7=$row7['total7'];
            if ($total7==null) {
                # code...
                $total7=0;
            }

            $suma8=mysqli_query($con,"select sum(total_venta) as total8 from facturas where mes='08/2016' and estado='vigente'");
            $row8=mysqli_fetch_array($suma8,MYSQLI_ASSOC);
            $total8=$row8['total8'];
            if ($total8==null) {
                # code...
                $total8=0;
            }

            $suma9=mysqli_query($con,"select sum(total_venta) as total9 from facturas where mes='09/2016' and estado='vigente'");
            $row9=mysqli_fetch_array($suma9,MYSQLI_ASSOC);
            $total9=$row9['total9'];
            if ($total9==null) {
                # code...
                $total9=0;
            }

            $suma10=mysqli_query($con,"select sum(total_venta) as total10 from facturas where mes='10/2016' and estado='vigente'");
            $row10=mysqli_fetch_array($suma10,MYSQLI_ASSOC);
            $total10=$row10['total10'];
            if ($total10==null) {
                # code...
                $total10=0;
            }

            $suma11=mysqli_query($con,"select sum(total_venta) as total11 from facturas where mes='11/2016' and estado='vigente'");
            $row11=mysqli_fetch_array($suma11,MYSQLI_ASSOC);
            $total11=$row11['total11'];
            if ($total11==null) {
                # code...
                $total11=0;
            }

            $suma12=mysqli_query($con,"select sum(total_venta) as total12 from facturas where mes='12/2016' and estado='vigente'");
            $row12=mysqli_fetch_array($suma12,MYSQLI_ASSOC);
            $total12=$row12['total12'];
            if ($total12==null) {
                # code...
                $total12=0;
            }
            
            
            
		
			?>	

          

<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        title: {
            text: 'Ventas Mensuales Año Actual',
            x: 0 //center
        },
        subtitle: {
            text: 'www.desarrollotuapp.com',
            x: 0
        },
        xAxis: {
            categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
                'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
        },
        yAxis: {
            title: {
                text: 'VENTAS MENSUALES'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'C$'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Ventas Por Mes',
            data: [<?php echo $total;?>, <?php echo $total2;?>,<?php echo $total3;?>,<?php echo $total4;?>,<?php echo $total5;?>,<?php echo $total6;?>,<?php echo $total7;?>,<?php echo $total8;?>,<?php echo $total9;?>,<?php echo $total10;?>,<?php echo $total11;?>,<?php echo $total12;?>]
        }]
    });
});
        </script>
   
  



<?php include("head.php");?>
	</head>
	<body>
    <?php
    include("navbar.php");
    ?> 


<div id="container" style="width: 60%; height: auto;"></div>





<?php
    include("footer.php");
    ?>
<script src="Highcharts-4.1.5/js/highcharts.js"></script>
<script src="Highcharts-4.1.5/js/modules/exporting.js"></script>
	</body>
</html>
