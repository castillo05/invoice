<?php
//error_reporting(0);
	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	
				if (isset($_GET['id']) and $_SESSION['permiso_user'] == 1){
					$cod_prov=intval($_GET['id']);

					/*$consulta=mysqli_query($con,"select * from cuentas_cobrar where num_fact='".$_GET['id']."'");
					$contar=mysqli_num_rows($consulta);
						if ($contar<1) {*/


					$del1="delete from proveedores where cod_prov='".$cod_prov."'";
					//$del2="delete from detalle_factura where numero_factura='".$numero_factura."'";
					if ($delete1=mysqli_query($con,$del1)){
						?>
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Aviso!</strong> Datos eliminados exitosamente
						</div>
						<?php 
					}else {
						?>
						<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Error!</strong> No se puedo eliminar los datos
						</div>
						<?php
						
					}

					//}
				}
			
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
		  $sTable = "proveedores";
		 $sWhere = "";
		 $sWhere.="where cod_prov>'0'";
		if ( $_GET['q'] != "" )
		{
		$sWhere.= "and (nombre like '%$q%' or cedula like '%$q%')";
			
		}
		
		$sWhere.="";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './proveedores.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			echo mysqli_error($con);
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="info">
					<th># Cedula</th>
					<th>Nombre</th>
					<th>Direccion</th>
					<th>Telefono</th>
					<th>Celular</th>
					<th class='text-right'>Estado</th>
					<th class='text-right'>Acciones</th>
					
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						//$id_factura=$row['id_factura'];
						$cod_prov=$row['cod_prov'];
						$cedula=$row['cedula'];
						//$fecha=date("d/m/Y", strtotime($row['fecha_factura']));
						$nombre=$row['nombre'];
						$direccion=$row['direccion'];
						$telefono=$row['telefono'];
						//$email_cliente=$row['email_cliente'];
						//$nombre_vendedor=$row['firstname']." ".$row['lastname'];
						$celular=$row['celular'];
						$estado=$row['estado'];
						if ($estado==1){
							$text_estado="Activo";$label_class='label-info';
						}elseif ($estado==2) {
							$text_estado="Inactivoe";$label_class='label-warning';
						}


						
					?>
					<tr>
						<td><?php echo $cedula; ?></td>
						<td><?php echo $nombre; ?></td>
						<td><?php echo $direccion; ?></td>
						<td><?php echo $telefono; ?></td>
						<td><?php echo $celular; ?></td>
						<td><span class="label <?php echo $label_class;?>"><?php echo $text_estado; ?></span></td>
									
					<td class="text-right">
					<?php
						if ($_SESSION['permiso_user']==1) {?>
							<a href="editar_proveedor.php?cod_prov=<?php echo $cod_prov; ?>" class='btn btn-default' title='Editar Proveedor' ><i class="glyphicon glyphicon-edit"></i></a> 
						
							<a href="#" class='btn btn-default' title='Borrar Proveedor' onclick="eliminar('<?php echo $cod_prov; ?>')"><i class="glyphicon glyphicon-trash"></i> </a>

					<?php
						}elseif($_SESSION['permiso_user']==2){?>
							
							
					<?php

						}

					?>
						
					</td>
						
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=7><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>