<?php

	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	 $action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';

			?>
			
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title></title>
				<link rel="stylesheet" href="">
				
			</head>
			<body>
				
<?php
			
	
	
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
         $q2 = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q2'], ENT_QUOTES)));
         // $condiciones=mysqli_real_escape_string($con,(strip_tags($_REQUEST['condiciones'], ENT_QUOTES)));
         // $estado_factura=mysqli_real_escape_string($con,(strip_tags($_REQUEST['estado_factura'], ENT_QUOTES)));
		  $sTable = "facturas, clientes, cuentas_cobrar";
		 $sWhere = "";
		 $sWhere.=" WHERE cuentas_cobrar.id_cliente=clientes.id_cliente and facturas.id_cliente=cuentas_cobrar.id_cliente and facturas.numero_factura=cuentas_cobrar.num_fact and cuentas_cobrar.saldo_actual>0";
		if ( $_GET['q'] != "" and  $_GET['q2'] != "")
		{
		$sWhere.= " and cuentas_cobrar.vencimiento BETWEEN '".$q."' and '".$q2."'";
			
		}
		
		$sWhere.=" order by cuentas_cobrar.num_fact desc";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$suma=mysqli_query($con,"SELECT sum(total_venta) AS total FROM $sTable  $sWhere");
		$row2=mysqli_fetch_array($suma,MYSQLI_ASSOC);
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './facturas.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			echo mysqli_error($con);
			?>

			<div class="box-header with-border">
		<h3 class="box-title">Listado de Cobro</h3>
	</div>

	<div id="reporte" class="table-responsive">
<a href="#" class='btn btn-default' title='Descargar Reporte' onclick="imprimir_reporte_fecha('<?php echo $q; ?>');"><i class="glyphicon glyphicon-download"></i></a>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="info">
					<th>Cuenta Nº</th>
					<th># Factura</th>
					<th>Fecha de Compra</th>
					<th>Vencimiento</th>
					<th>Cliente</th>
					
					<th class='text-right'>Monto</th>
					<th class='text-right'>Saldo Anterior</th>
					<th class='text-right'>Abono</th>
					<th class='text-right'>Saldo Actual</th>
		
					<th class='text-right'>Acciones</th>
					
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						$id=$row['id'];
						$numero_factura=$row['num_fact'];
						$fecha=date("d/m/Y", strtotime($row['fecha_fact']));
						$vencimiento=$row['vencimiento'];
						$nombre_cliente=$row['nombre_cliente'];
						$email_cliente=$row['email_cliente'];
						$telefono_cliente=$row['telefono_cliente'];
						$monto=$row['monto'];
						$status=$row['estado_factura'];
						$saldo_anterior=$row['saldo_anterior'];
						$abono=$row['abono'];
						$saldo_actual=$row['saldo_actual'];
					?>
					<tr>
						<td><?php echo $id; ?></td>
						<td><?php echo $numero_factura; ?></td>
						<td><?php echo $fecha; ?></td>
						<td><?php echo $vencimiento; ?></td>
						<td style="width: 6%"><a href="#" data-toggle="tooltip" data-placement="top" title="<i class='glyphicon glyphicon-phone'></i> <?php echo $telefono_cliente;?><br><i class='glyphicon glyphicon-envelope'></i>  <?php echo $email_cliente;?>" ><?php echo $nombre_cliente;?></a></td>
						
						<td class='text-right'><?php echo $monto; ?></td>	
						<td class='text-right'><?php echo $saldo_anterior; ?></td>	
						<td class='text-right'><?php echo $abono; ?></td>
						<td class='text-right'><?php echo $saldo_actual; ?></td>



					<td class="text-right">
					<?php
						if ($_SESSION['permiso_user']==1) {?>
							
							<a href="editar_cuenta.php?id=<?php echo $id; ?>&numero_factura=<?php echo $numero_factura; ?>" class='btn btn-default' title='Editar Cuenta' ><i class="glyphicon glyphicon-edit"></i></a> 
							<a href="#" class='btn btn-default' title='Descargar Estado de Cuenta' onclick="imprimir_cuenta('<?php echo $id; ?>');"><i class="glyphicon glyphicon-download"></i></a> 
							    <!-- <a href="#" class='btn btn-default' title='Borrar Cuenta' onclick="eliminar('<?php //echo $id; ?>')"><i class="glyphicon glyphicon-trash"></i> </a> -->

					<?php
						}else{?>
							<a href="editar_factura.php?id_factura=<?php ?>" class='btn btn-default' title='Editar factura' ><i class="glyphicon glyphicon-edit"></i></a> 
							<a href="#" class='btn btn-default' title='Descargar factura' onclick="imprimir_factura('<?php ?>');"><i class="glyphicon glyphicon-download"></i></a> 
							
					<?php

						}

					?>
						
					</td>
						
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=7><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>
</div>


			</body>
			</html>
			
