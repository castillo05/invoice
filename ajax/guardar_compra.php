<?php
include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
$session_id=session_id();
	/*Inicia validacion del lado del servidor*/
	if (empty($_POST['cod_prov'])) {
           $errors[] = "Código vacío";
        } else if (empty($_POST['nombre_proveedor'])){
			$errors[] = "Nombre del proveedor vacío";
		} else if ($_POST['estado']==""){
			$errors[] = "Selecciona el estado del producto";
		} else if (empty($_POST['fecha'])){
			$errors[] = "Fecha de compra vacío";

		}else if (empty($_POST['num_factura'])) {
			$error[]="numero factura vacio";
		}else if (
			!empty($_POST['cod_prov']) &&
			!empty($_POST['nombre_proveedor']) &&
			$_POST['estado']!="" &&
			!empty($_POST['num_factura'])
		){

		 
		/* Connect To Database*/
		require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
		require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
		// escaping, additionally removing everything that could be (html/javascript-) code
		
		$cod_prov=mysqli_real_escape_string($con,(strip_tags($_POST["cod_prov"],ENT_QUOTES)));
		$nombre_proveedor=mysqli_real_escape_string($con,(strip_tags($_POST["nombre_proveedor"],ENT_QUOTES)));
		//$prov=mysqli_real_escape_string($con,(strip_tags($_POST["prov"],ENT_QUOTES)));
		$estado=intval($_POST['estado']);
		$condiciones=intval($_POST['condiciones']);
		$num_factura=$_POST['num_factura'];
		$fecha=$_POST['fecha'];

		if ($estado==1) {
			
			$fecha_ven=$nuevafecha = strtotime ( '+0 day' , strtotime ( $fecha ) ) ;
			$fecha_ven=date ( 'Y-m-d' , $fecha_ven );
		}elseif($estado==2){
			$fecha_ven=$nuevafecha = strtotime ( '+15 day' , strtotime ( $fecha ) ) ;
			$fecha_ven=date ( 'Y-m-d' , $fecha_ven );
		}elseif($estado==3){
			$fecha_ven=$nuevafecha = strtotime ( '+30 day' , strtotime ( $fecha ) ) ;
			$fecha_ven=date ( 'Y-m-d' , $fecha_ven );
		}
		
		$condiciones=$_POST['condiciones'];



	$comprobar=mysqli_query($con,"select * from compras,detalle_compra where compras.numero_factura='".$num_factura."' and detalle_compra.numero_factura='".$num_factura."' and detalle_compra.cod_prov='".$cod_prov."' and compras.proveedor='".$cod_prov."'");
$contar=mysqli_num_rows($comprobar);

if ($contar==0) {
	# code...

	$sumador_total=0;
	$sql=mysqli_query($con, "select * from products, tmp_compra where products.id_producto=tmp_compra.id_producto and tmp_compra.session_id='".$session_id."'");


	while ($row=mysqli_fetch_array($sql))
	{
	$id_tmp=$row["id_tmp"];
	$id_tmp2=$row["id_tmp"];
	$id_producto=$row["id_producto"];
	$codigo_producto=$row['codigo_producto'];
	$cantidad=$row['cantidad_tmp'];
	$nombre_producto=$row['nombre_producto'];
	$exento=$row['exento'];
	$cod_proveedor=$row['cod_prov'];
	$precio_venta=$row['precio_tmp'];
	$precio_venta_f=number_format($precio_venta,2);//Formateo variables
	$precio_venta_r=str_replace(",","",$precio_venta_f);//Reemplazo las comas
	$precio_total=$precio_venta_r*$cantidad;
	$precio_total_f=number_format($precio_total,2);//Precio total formateado
	$precio_total_r=str_replace(",","",$precio_total_f);//Reemplazo las comas
	$sumador_total+=$precio_total_r;//Sumador
		

	

		
		$insert_detail=mysqli_query($con, "INSERT INTO detalle_compra VALUES ('','$num_factura','$id_producto','$cantidad','$precio_venta_r','$cod_prov')");


		$date2=date("Y-m-d");

		$consultar_kardex=mysqli_query($con,"select * from kardex where cod_producto='".$id_producto."'");

	$existencia=0;
	$existencias=$existencia-$cantidad;
	$insert_kardex=mysqli_query($con,"INSERT into kardex values ('','$id_producto','$date2','1','$num_factura','Entradas','$cantidad','','$existencias')");


	}

	
	$subtotal=number_format($sumador_total,2,'.','');
	//$total_iva==0;
	//$total_iva=number_format($total_iva,2,'.','');
	$total_factura=$subtotal;



		//vERIFICAR SI EXISTE EL CODIGO DEL PRODUCTO MYSQLI
		

		$sql="INSERT INTO compras (numero_factura,fecha,proveedor,estado_compra,importe,importe_pendiente,vencimiento,condiciones) VALUES ('$num_factura','$fecha','$cod_prov','$estado','$total_factura','$total_factura','$fecha_ven','$condiciones')";

		$delete="DELETE FROM tmp_compra WHERE session_id='".$session_id."'";
		$query_new_insert = mysqli_query($con,$sql);
		$query_delete=mysqli_query($con,$delete);



			if ($query_new_insert and $query_delete){
				$messages[] = "Compra ha sido ingresada satisfactoriamente.";
				
				
			} else{
				$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
			}

			
			
		} 

//Aqui termina el if que comprueba si existe o no una factura igual con ese proveedor

		elseif ($comprobar) {
			$errors []= "Lo siento este proveedor ya tiene una factura con este Numero**Eliga otro proveedor u otro # de Factura**.".mysqli_error($con);
		
}else
			$errors []= "Error desconocido.";
		}
		
		if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
									
								}
							?>
				</div>
				<?php
			}


?>