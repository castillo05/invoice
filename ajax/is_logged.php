<?php	
	@session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: ../login.php");
		exit;
    }