<?php

	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';


	if (isset($_GET['id']) and $_SESSION['permiso_user'] == 1){
		$id=intval($_GET['id']);
		$del1="delete from cuentas_cobrar where id='".$id."'";
		//$del2="delete from detalle_factura where numero_factura='".$numero_factura."'";
		if ($delete1=mysqli_query($con,$del1)){
			?>
			<div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Aviso!</strong> Datos eliminados exitosamente
			</div>
			<?php 
		}else {
			?>
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Error!</strong> No se puedo eliminar los datos
			</div>
			<?php
			
		}
	}
	
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
          $q2 = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q2'], ENT_QUOTES)));
		  $sTable = "clientes,cuentas_cobrar,facturas";
		 $sWhere = "";
		 $sWhere.=" WHERE cuentas_cobrar.id_cliente=clientes.id_cliente and facturas.id_cliente=cuentas_cobrar.id_cliente and facturas.numero_factura=cuentas_cobrar.num_fact";
		if ( $_GET['q'] != "" )
		{
		$sWhere.= " and (facturas.numero_factura like '%$q%' or clientes.nombre_cliente like '%$q2%')";
			
		}
		
		$sWhere.=" order by cuentas_cobrar.num_fact desc";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './facturas.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			echo mysqli_error($con);
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="">
					<th>Cuenta Nº</th>
					<th># Factura</th>
					<th>Fecha de Compra</th>
					<th>Vencimiento</th>
					<th>Cliente</th>
					
					<th class='text-right'>Monto</th>
					<th class='text-right'>Saldo Anterior</th>
					<th class='text-right'>Abono</th>
					<th class='text-right'>Saldo Actual</th>
		
					<th class='text-right'>Acciones</th>
					
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						$id=$row['id'];
						$numero_factura=$row['num_fact'];
						$fecha=date("d/m/Y", strtotime($row['fecha_fact']));
						$vencimiento=$row['vencimiento'];
						$nombre_cliente=$row['nombre_cliente'];
						$email_cliente=$row['email_cliente'];
						$telefono_cliente=$row['telefono_cliente'];
						$monto=$row['monto'];
						$status=$row['estado_factura'];
						$saldo_anterior=$row['saldo_anterior'];
						$abono=$row['abono'];
						$saldo_actual=$row['saldo_actual'];
					?>
					<tr>
						<td><?php echo $id; ?></td>
						<td><?php echo $numero_factura; ?></td>
						<td><?php echo $fecha; ?></td>
						<td><?php echo $vencimiento; ?></td>
						<td style="width: 6%"><a href="#" data-toggle="tooltip" data-placement="top" title="<i class='glyphicon glyphicon-phone'></i> <?php echo $telefono_cliente;?><br><i class='glyphicon glyphicon-envelope'></i>  <?php echo $email_cliente;?>" ><?php echo $nombre_cliente;?></a></td>
						
						<td class='text-right'><?php echo $monto; ?></td>	
						<td class='text-right'><?php echo $saldo_anterior; ?></td>	
						<td class='text-right'><?php echo $abono; ?></td>
						<td class='text-right'><?php echo $saldo_actual; ?></td>



					<td class="text-right">
					<?php
						if ($_SESSION['permiso_user']==1) {?>
							
							<a href="editar_cuenta.php?id=<?php echo $id; ?>&numero_factura=<?php echo $numero_factura; ?>" class='btn btn-default' title='Editar Cuenta' ><i class="glyphicon glyphicon-edit"></i></a> 
							<a href="#" class='btn btn-default' title='Descargar Estado de Cuenta' onclick="imprimir_cuenta('<?php echo $id; ?>');"><i class="glyphicon glyphicon-download"></i></a> 
							    <!-- <a href="#" class='btn btn-default' title='Borrar Cuenta' onclick="eliminar('<?php //echo $id; ?>')"><i class="glyphicon glyphicon-trash"></i> </a> -->

					<?php
						}else{?>
							<a href="editar_factura.php?id_factura=<?php ?>" class='btn btn-default' title='Editar factura' ><i class="glyphicon glyphicon-edit"></i></a> 
							<a href="#" class='btn btn-default' title='Descargar factura' onclick="imprimir_factura('<?php ?>');"><i class="glyphicon glyphicon-download"></i></a> 
							
					<?php

						}

					?>
						
					</td>
						
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=7><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>