	<?php

	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	if (isset($_GET['id'])){
		$id_producto=intval($_GET['id']);
		$query=mysqli_query($con, "select * from detalle_factura where id_producto='".$id_producto."'");
		$count=mysqli_num_rows($query);
		if ($count==0){
			if ($delete1=mysqli_query($con,"DELETE FROM products WHERE id_producto='".$id_producto."'")){
			?>
			<div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Aviso!</strong> Datos eliminados exitosamente.
			</div>
			<?php 
		}else {
			?>
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Error!</strong> Lo siento algo ha salido mal intenta nuevamente.
			</div>
			<?php
			
		}
			
		} else {
			?>
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Error!</strong> No se pudo eliminar éste  producto. Existen cotizaciones vinculadas a éste producto. 
			</div>
			<?php
		}
		
		
		
	}
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
		 $aColumns = array('codigo_producto', 'nombre_producto');//Columnas de busqueda
		 $sTable = "products,proveedores,marcas,unidad_medida";
		 $sWhere = "";
		/*if ( $_GET['q'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".$q."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		$sWhere.=" order by id_producto desc";*/
		$sWhere="where products.proveedor=proveedores.cod_prov and products.unidad_med=unidad_medida.id_medida and products.marca=marcas.id_marca";
		if ( $_GET['q'] != "" )
		{
			// $sWhere = "WHERE (";
			// for ( $i=0 ; $i<count($aColumns) ; $i++ )
			// {
			// 	$sWhere .= $aColumns[$i]." LIKE '%".$q."%' OR ";
			// }
			// $sWhere = substr_replace( $sWhere, "", -3 );
			// $sWhere .= ')';
			$sWhere="where products.proveedor=proveedores.cod_prov and products.unidad_med=unidad_medida.id_medida and products.marca=marcas.id_marca and(products.codigo_producto like '%$q%' or products.nombre_producto like '%$q%')";
		}
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './productos.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="">
					<th>Código</th>
					<th>Producto</th>
					<th>Proveedor</th>
					<th>Marca</th>
					<th>Unidad M</th>
					<th>Agregado</th>
					<th>Stock</th>
					<th class='text-right'>Precio Compra</th>
					<th class='text-right'>Precio Venta</th>
					<th class='text-right'>Acciones</th>
					
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						$id_producto=$row['id_producto'];
						$codigo_producto=$row['codigo_producto'];
						$nombre_producto=$row['nombre_producto'];
						$prov_prod=$row['nombre'];
						$status_producto=$row['status_producto'];
						if ($status_producto==1){$estado="Activo";}
						else {$estado="Inactivo";}
						$date_added= date('d/m/Y', strtotime($row['date_added']));
						$stock=$row['stock'];
						$marca=$row['nombre_marca'];
						$id_marca=$row['id_marca'];
						$unidad_m=$row['nombre_medida'];
						$id_medida=$row['id_medida'];
						$cod_prov=$row['cod_prov'];
						$precio_producto=$row['precio_producto'];
						$precio_compra=$row['precio_compra'];
						$cantidad=$row['cantidad_total'];
						$ex_min=$row['ex_min'];
						//$ex_max=$row['ex_max'];
						$exentos=$row['exento'];
						if ($exentos==1){$exento="Si";}
						else {$exento="No";}
					?>
					
					<input type="hidden" value="<?php echo $codigo_producto;?>" id="codigo_producto<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $nombre_producto;?>" id="nombre_producto<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $cod_prov;?>" id="cod_prov<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $prov_prod;?>" id="prov_prod<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $estado;?>" id="estado<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo number_format($precio_producto,2,'.','');?>" id="precio_producto<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo number_format($precio_compra,2,'.','');?>" id="precio_compra<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $cantidad;?>" id="cantidad_total<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $stock;?>" id="stock<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $ex_min;?>" id="ex_min<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $marca;?>" id="marca<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $id_marca;?>" id="id_marca<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $unidad_m;?>" id="unidad_m<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $id_medida;?>" id="id_medida<?php echo $id_producto;?>">
					<input type="hidden" value="<?php echo $exento;?>" id="exento<?php echo $id_producto;?>">
					<tr>
						
						<td><?php echo $codigo_producto; ?></td>
						<td ><?php echo $nombre_producto; ?></td>
						<td ><?php echo $prov_prod; ?></td>
						<td><?php echo $marca; ?></td>
						<td><?php echo $unidad_m;?></td>
						<td><?php echo $date_added;?></td>
						<td><?php echo $stock; ?></td>
						<td><span class='pull-right'>C$<?php echo number_format($precio_compra,2);?></span></td>
						<td><span class='pull-right'>C$<?php echo number_format($precio_producto,2);?></span></td>
					<td ><span class="pull-right">
					<?php
						if ($_SESSION['permiso_user']==1) {?>
							<a href="#" class='btn btn-default' title='Editar producto' onclick="obtener_datos('<?php echo $id_producto;?>');" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-edit"></i></a> 
					<a href="#" class='btn btn-default' title='Borrar producto' onclick="eliminar('<?php echo $id_producto; ?>')"><i class="glyphicon glyphicon-trash"></i> </a></span></td>

					<?php
						}elseif($_SESSION['permiso_user']==2){?>
							<a href="#" class='btn btn-default' title='Editar producto' onclick="obtener_datos('<?php echo $id_producto;?>');" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-edit"></i></a> 
					
							
					<?php

						}

					?>
					
						
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=6><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>