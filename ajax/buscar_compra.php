<?php
//error_reporting(0);
	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	
				if (isset($_GET['id']) and isset($_GET['codigo']) and isset($_GET['id_compra']) and $_SESSION['permiso_user'] == 1){
					$id_compras=intval($_GET['id']);//Recibe el codigo de compra
					$codigo=intval($_GET['codigo']);//recibe el codigo del proveedor
					$numero_factura=intval($_GET['id_compra']);//recibe el numero de factura
					//$consulta=mysqli_query($con,"select * from compras where numero_factura_fact='".$_GET['id']."' and proveedor='".$codigo."'");
					
					
								$del1="delete from compras where numero_factura='".$numero_factura."' and proveedor='".$codigo."' and cod_compra='".$id_compras."'";
								$del2="delete from detalle_compra where numero_factura='".$numero_factura."' and cod_prov='".$codigo."'";
								if ($delete1=mysqli_query($con,$del1) and $delete2=mysqli_query($con,$del2)){
									?>
									<div class="alert alert-success alert-dismissible" role="alert">
									  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									  <strong>Aviso!</strong> Datos eliminados exitosamente
									</div>	
									<?php 
								}else {
									?>
									<div class="alert alert-danger alert-dismissible" role="alert">
									  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									  <strong>Error!</strong> No se puedo eliminar los datos
									</div>
									<?php
						
					}
					
				}
			
			
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
         $q2 = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q2'], ENT_QUOTES)));
         $q3 = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q3'], ENT_QUOTES)));
		  $sTable = "compras,proveedores";
		 $sWhere = "";
		 $sWhere.=" WHERE compras.proveedor=proveedores.cod_prov";
		if ( $_GET['q'] != "" or  $_GET['q2'] != "" or  $_GET['q3'] != "")
		{
		$sWhere.= " and compras.numero_factura like '%$q%' and proveedores.nombre like '%$q2%' and compras.condiciones like '%$q3%'";
			
		}
		
		$sWhere.="";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './facturas.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			echo mysqli_error($con);
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="info">
					<th># Factura</th>
					<th>Fecha</th>
					<th>Proveedor</th>
					<th>Estado</th>
					<th>Condicion</th>
					<th>Importe</th>
					<th class='text-right'>Importe Pendiente</th>
					<th class='text-right'>Vence</th>
					<th class='text-right'>Acciones</th>
					
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						
						$id_compra=$row['cod_compra'];
						$numero_factura=$row['numero_factura'];
						$fecha=date("d/m/Y", strtotime($row['fecha']));
						$proveedor=$row['nombre'];
						$codigo_proveedor=$row['proveedor'];
						// $telefono_cliente=$row['telefono_cliente'];
						// $email_cliente=$row['email_cliente'];
						$importe=$row['importe'];
						$importe_pendiente=$row['importe_pendiente'];
						$estado_factura=$row['condiciones'];
						if ($estado_factura==4){
							$text_estado="Pagada";$label_class='label-info';
						}elseif ($estado_factura==5) {
							$text_estado="Pendiente";$label_class='label-success';
						}elseif ($estado_factura==6) {
							$text_estado="Vencida";$label_class='label-warning';
						}

						$condiciones=$row['estado_compra'];
						if ($condiciones==1){
							$text_condiciones="Pagada";$label_class1='label-info';
						}elseif ($condiciones==2) {
							$text_condiciones="Credito de 15 Dias";$label_class1='label-warning';
						}elseif ($condiciones==3) {
							$text_condiciones="Credito de 30 Dias";$label_class1='label-danger';
						}
						$vence=$row['vencimiento'];
						$nombre_proveedor=$row['nombre'];
						$telefono_proveedor=$row['telefono'];
						$celular_proveedor=$row['celular'];
						$email_proveedor=$row['email'];


					
					?>
					<tr>
						<input type="hidden" name="" id="id_compra_<?php echo $id_compra; ?>" value="<?php echo $numero_factura; ?>">
						<input type="hidden" name="" id="codigo_<?php echo $id_compra; ?>" value="<?php echo $codigo_proveedor; ?>">
						<td><?php echo $numero_factura; ?></td>
						<td><?php echo $fecha; ?></td>
						<td><a href="#" data-toggle="tooltip" data-placement="top" title="<i class='glyphicon glyphicon-phone'></i> <?php echo $celular_proveedor?><br><i class='glyphicon glyphicon-phone'></i> <?php echo $telefono_proveedor?><br><i class='glyphicon glyphicon-user'></i><?php echo $nombre_proveedor;?>" ><?php echo $proveedor;?></a></td>
						<td><span class="label <?php echo $label_class;?>"><?php echo $text_estado; ?></span></td>
						<td><span class="label <?php echo $label_class1;?>"><?php echo $text_condiciones; ?></span></td>
						<td class='text-left'><?php echo number_format ($importe,2); ?></td>
						<td class='text-right'><?php echo number_format ($importe_pendiente,2); ?></td>
						<td class='text-right'><?php echo $vence; ?></td>

					<td class="text-right">
					<?php
						if ($_SESSION['permiso_user']==1) {?>
							<a href="editar_compra.php?cod_compra=<?php echo $id_compra;?>&numero_factura=<?php echo $numero_factura; ?>" class='btn btn-default' title='Editar compra' ><i class="glyphicon glyphicon-edit"></i></a> 
							<a href="#" class='btn btn-default' title='Descargar factura' onclick=""><i class="glyphicon glyphicon-download"></i></a> 
							<a href="#" class='btn btn-default' title='Borrar compra' onclick="eliminar('<?php echo $id_compra; ?>')"><i class="glyphicon glyphicon-trash"></i> </a>

					<?php
						}elseif($_SESSION['permiso_user']==2){?>
							<a href="editar_factura.php?id_factura=<?php echo $id_factura;?>&numero_factura=<?php echo $numero_factura; ?>" class='btn btn-default' title='Editar factura' ><i class="glyphicon glyphicon-edit"></i></a> 
							<a href="#" class='btn btn-default' title='Descargar factura' onclick="imprimir_factura('<?php echo $id_factura;?>');"><i class="glyphicon glyphicon-download"></i></a> 
							
					<?php

						}

					?>
						
					</td>
						
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=7><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>