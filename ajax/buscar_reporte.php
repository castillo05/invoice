<?php

	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	 $action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';

			?>
			
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title></title>
				<link rel="stylesheet" href="">
				
			</head>
			<body>
				
<?php
			
	
	
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
         $q2 = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q2'], ENT_QUOTES)));
         $condiciones=mysqli_real_escape_string($con,(strip_tags($_REQUEST['condiciones'], ENT_QUOTES)));
         $estado_factura=mysqli_real_escape_string($con,(strip_tags($_REQUEST['estado_factura'], ENT_QUOTES)));
		  $sTable = "facturas, clientes, users";
		 $sWhere = "";
		 $sWhere.=" WHERE facturas.id_cliente=clientes.id_cliente and facturas.id_vendedor=users.user_id";
		if ( $_GET['q'] != "" and  $_GET['q2'] != "")
		{
		$sWhere.= " and facturas.condiciones= '".$condiciones."' and facturas.estado_factura='".$estado_factura."' and facturas.fecha_factura BETWEEN '".$q."' and '".$q2."'";
			
		}
		
		$sWhere.=" order by facturas.id_factura desc";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$suma=mysqli_query($con,"SELECT sum(total_venta) AS total FROM $sTable  $sWhere");
		$row2=mysqli_fetch_array($suma,MYSQLI_ASSOC);
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './facturas.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			echo mysqli_error($con);
			?>




    <div class="box-header with-border">
		<h3 class="box-title">Listado de Ventas</h3>
	</div>


<div id="reporte" class="table-responsive">
<a href="#" class='btn btn-default' title='Descargar Reporte' onclick="imprimir_reporte('<?php echo $q; ?>');"><i class="glyphicon glyphicon-download"></i></a>
<table id="" name="reporte" class="table table-condensed table-hover table-striped ">


<tr>
<td class="text-center">Factura Nº</td>
<td>Cliente</td>
<td>Vendedor </td>
<td class="text-center">Fecha</td>
<!-- <th class="text-right">Neto </th>
<th class="text-right">IVA</th> -->
<td class="text-right">Sub-Total</td>
<td class="text-right">IVA</td>
<td class="text-right">Total</td>
</tr>



			
				<?php
				while ($row=mysqli_fetch_array($query)){
						//$id_factura=$row['id_factura'];
						$id_factura=$row['id_factura'];
						$numero_factura=$row['numero_factura'];
						$fecha=date("d/m/Y", strtotime($row['fecha_factura']));
						$nombre_cliente=$row['nombre_cliente'];
						//$telefono_cliente=$row['telefono_cliente'];
						//$email_cliente=$row['email_cliente'];
						$nombre_vendedor=$row['user_name'];
						//$estado_factura=$row['estado_factura'];
						// if ($estado_factura==1){$text_estado="Pagada";$label_class='label-success';}
						// else{$text_estado="Pendiente";$label_class='label-warning';}
						$subtotal=$row['sub_total'];
						$iva=$row['iva'];
						$total_venta=$row['total_venta'];
						$total=$row2['total'];
					?>
					<tr>
					<td class="text-center"><?php echo $numero_factura; ?></td>
					
					<td><?php echo $nombre_cliente; ?></td>
					<td><?php echo $nombre_vendedor; ?></td>
					<td class="text-center"><?php echo $fecha; ?></td>
					<td class="text-right"><?php echo $subtotal; ?></td>
					<td class="text-right"><?php echo $iva; ?></td>
					<td class="text-right"><?php echo $total_venta; ?></td>
					
					</tr>
					
					<?php
				}

				
				?>

				
				<tr>
				<td></td>
				<td></td>
				<td></td>
				<td class="text-center">TOTAL:</td>
					<td class="text-right">C$ <?php echo number_format($total,2); ?></td>
				</tr>
				
				
					<td colspan=7><span class="pull-right"><?
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				
				
			 
	</table>

	<?php
		}
	}
?>
</div>


			</body>
			</html>
			
