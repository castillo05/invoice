<?php
    require_once '../config/db.php';
    require_once '../config/conexion.php';
    require_once 'vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use PhpOffice\PhpSpreadsheet\Style\Alignment;
    use PhpOffice\PhpSpreadsheet\Style\Border;
    use PhpOffice\PhpSpreadsheet\Style\Fill;

    $consulta = "SELECT * FROM products";
    $resultado = $con->query($consulta);
    if ($resultado->num_rows > 0) {

        date_default_timezone_set('America/Mexico_City');

        if (PHP_SAPI == 'cli')
            die('Este archivo solo se puede ver desde un navegador web');

        // Crear un nuevo objeto Spreadsheet
        $spreadsheet = new Spreadsheet();

        // Asignar propiedades del libro
        $spreadsheet->getProperties()->setCreator("Codedrinks")
            ->setLastModifiedBy("Codedrinks")
            ->setTitle("Reporte Excel con PHP y MySQL")
            ->setSubject("Reporte Excel con PHP y MySQL")
            ->setDescription("INVENTARIO DE PRODUCTOS")
            ->setKeywords("")
            ->setCategory("Reporte excel");

        $tituloReporte = "INVENTARIO DE PRODUCTOS";
        $titulosColumnas = array('CODIGO PRODUCTOS', 'NOMBRE PRODUCTOS', 'PRECIO DE COMPRA', 'PRECIO DE VENTA', 'CANTIDAD');

        $spreadsheet->getActiveSheet()
            ->mergeCells('A1:E1');

        // Agregar títulos del reporte
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', $tituloReporte)
            ->setCellValue('A3', $titulosColumnas[0])
            ->setCellValue('B3', $titulosColumnas[1])
            ->setCellValue('C3', $titulosColumnas[2])
            ->setCellValue('D3', $titulosColumnas[3])
            ->setCellValue('E3', $titulosColumnas[4]);

        // Agregar datos de los productos
        $i = 4;
        while ($fila = $resultado->fetch_array()) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $i, $fila['codigo_producto'])
                ->setCellValue('B' . $i, $fila['nombre_producto'])
                ->setCellValue('C' . $i, $fila['precio_compra'])
                ->setCellValue('D' . $i, utf8_encode($fila['precio_producto']))
                ->setCellValue('E' . $i, $fila['stock']);
            $i++;
        }

        // Estilos
        $estiloTituloReporte = [
            'font' => [
                'name' => 'Verdana',
                'bold' => true,
                'italic' => false,
                'strike' => false,
                'size' => 16,
                'color' => [
                    'rgb' => 'FFFFFF'
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FF220835'
                ]
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_NONE
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];

        $estiloTituloColumnas = [
            'font' => [
                'name' => 'Arial',
                'bold' => true,
                'color' => [
                    'rgb' => 'FFFFFF'
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'rgb' => 'c47cf2'
                ],
                'endColor' => [
                    'argb' => 'FF431a5d'
                ]
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '143860'
                    ]
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '143860'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];

        $estiloInformacion = [
            'font' => [
                'name' => 'Arial',
                'color' => [
                    'rgb' => '000000'
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FFd9b7f4'
                ]
            ],
            'borders' => [
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [
                        'rgb' => '3a2a47'
                    ]
                ]
            ]
        ];

        $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($estiloTituloReporte);
        $spreadsheet->getActiveSheet()->getStyle('A3:E3')->applyFromArray($estiloTituloColumnas);
        $spreadsheet->getActiveSheet()->getStyle('A4:E' . ($i - 1))->applyFromArray($estiloInformacion);

        // Ajustar tamaño de columnas automáticamente
        foreach (range('A', 'E') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Asignar nombre a la hoja
        $spreadsheet->getActiveSheet()->setTitle('INVENTARIO');

        // Establecer la hoja activa al índice 0
        $spreadsheet->setActiveSheetIndex(0);
        // Inmovilizar paneles
        $spreadsheet->getActiveSheet()->freezePaneByColumnAndRow(0, 4);

        // Descargar archivo de Excel
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Disposition: attachment;filename="INVENTARIO DE PRODUCTOS.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    } else {
        print_r('No hay resultados para mostrar');
    }
?>
