<?php
error_reporting(0);
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/*Inicia validacion del lado del servidor*/
	if (empty($_POST['mod_id'])) {
           $errors[] = "ID vacío";
        }else if (empty($_POST['mod_codigo'])) {
           $errors[] = "Código vacío";
        } else if (empty($_POST['mod_nombre'])){
			$errors[] = "Nombre del producto vacío";
		} else if ($_POST['mod_estado']==""){
			$errors[] = "Selecciona el estado del producto";
		}else if (empty($_POST['id_prov'])){
			$errors[] = "Selecione el proveedor";
		} else if (empty($_POST['mod_precio'])){
			$errors[] = "Precio de venta vacío";
		} else if (empty($_POST['mod_precio_compra'])){
			$errors[] = "Precio de compra vacío";
		}
		else if (empty($_POST['mod_marca'])){
			$errors[] = "marca vacío";
		}/*else if (empty($_POST['mod_stock'])){
			$errors[] = "Ingrese la cantidad del stock";
		}else if (empty($_POST['mod_cantidad'])){
			$errors[] = "Ingrese la cantidad del producto";
		}*/else if (empty($_POST['mod_ex_min'])){
			$errors[] = "Ingrese la existencia minima";
		}else if ($_POST['mod_exento']==""){
			$errors[] = "Selecciona si es exento o no...";
		
		} else if (
			!empty($_POST['mod_id']) &&
			!empty($_POST['mod_codigo']) &&
			!empty($_POST['mod_nombre']) &&
			$_POST['mod_estado']!="" &&
			!empty($_POST['mod_precio']) &&
			!empty($_POST['mod_precio_compra']) &&
			// !empty($_POST['mod_cantidad']) &&
			// !empty($_POST['mod_stock']) &&
			!empty($_POST['mod_ex_min']) &&
			!empty($_POST['mod_prov']) &&
			$_POST['mod_exento']!=""

		){
		/* Connect To Database*/
		require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
		require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
		// escaping, additionally removing everything that could be (html/javascript-) code
		$codigo=mysqli_real_escape_string($con,(strip_tags($_POST["mod_codigo"],ENT_QUOTES)));
		$nombre=mysqli_real_escape_string($con,(strip_tags($_POST["mod_nombre"],ENT_QUOTES)));
		$prov=mysqli_real_escape_string($con,(strip_tags($_POST["id_prov"],ENT_QUOTES)));
		$marca=mysqli_real_escape_string($con,(strip_tags($_POST["id_marca"],ENT_QUOTES)));
		$unidad_m=mysqli_real_escape_string($con,(strip_tags($_POST["id_medida"],ENT_QUOTES)));
		$estado=intval($_POST['mod_estado']);
		$precio_compra=floatval($_POST['mod_precio_compra']);
		$precio_venta=floatval($_POST['mod_precio']);
		$id_producto=$_POST['mod_id'];
		$stock=$_POST['mod_stock'];
		$cant_total=intval($_POST['mod_cantidad']);
		$ex_min=intval($_POST['mod_ex_min']);
		//$ex_max=intval($_POST['mod_ex_max']);
		//Revisar porque no actualiza exento.
		$exentos=$_POST['mod_exento'];
		

		//fin//
		
		$sql="UPDATE products SET codigo_producto='".$codigo."', nombre_producto='".$nombre."',proveedor='".$prov."',unidad_med='".$unidad_m."',marca='".$marca."', status_producto='".$estado."',precio_compra='".$precio_compra."', precio_producto='".$precio_venta."',stock='".$stock."',cantidad_total='".$cant_total."',ex_min='".$ex_min."',exento='".$exentos."' WHERE id_producto='".$id_producto."'";
		$query_update = mysqli_query($con,$sql);
			if ($query_update){
				$messages[] = "Producto ha sido actualizado satisfactoriamente.";
			} else{
				$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
			}
		} else {
			$errors []= "Error desconocido.";
		}
		
		if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}

?>