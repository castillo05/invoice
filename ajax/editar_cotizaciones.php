<?php
	
include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
$id_factura= $_SESSION['id_cotizacion'];
$numero_factura= $_SESSION['numero_cotizacion'];
if (isset($_POST['id'])){$id=intval($_POST['id']);}
if (isset($_POST['cantidad'])){$cantidad=intval($_POST['cantidad']);}
if (isset($_POST['precio_venta'])){$precio_venta=floatval($_POST['precio_venta']);}
if (isset($_POST['descuento'])){$descuento=$_POST['descuento'];}
if (isset($_POST['moneda'])){$moneda=$_POST['moneda'];}
if (isset($_POST['exento'])){$exento=$_POST['exento'];}
if (isset($_POST['iva'])) {$iva=$_POST['iva'];}
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
if (!empty($id) and !empty($cantidad) and !empty($precio_venta))
{
$precio=$precio_venta-$descuento;
$insert_tmp=mysqli_query($con, "INSERT INTO detalle_cotizacion (numero_cotizacion, id_producto,cantidad,descuento_p,iva_p,precio_venta) VALUES ('$numero_factura','$id','$cantidad','$descuento','$iva','$precio')");
//$update_prod=mysqli_query($con,"UPDATE products set stock=stock-'".$cantidad."' where id_producto='".$id."'");

if ($insert_tmp) {
	# code...
	$messages[] = "Producto Agregado satisfactoriamente.";
	
}else{
					$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
			}
		 
		
		if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
}








if (isset($_GET['id']))//codigo elimina un elemento del array
{
$id_detalle=intval($_GET['id']);	
$delete=mysqli_query($con, "DELETE FROM detalle_cotizacion WHERE id_detalle='".$id_detalle."'");

}




?>
<div class="table-responsive">
	

<table class="table">
<tr>

	<th class='text-center'>CODIGO</th>
	<th class='text-center'>CANT.</th>
	<th>DESCRIPCION</th>
	<th class='text-right'>PRECIO UNIT.</th>
	<th class='text-right'>DESC.</th>
	<th class='text-right'>IVA.</th>
	<th class='text-right'>PRECIO TOTAL</th>
	<th></th>
</tr>
<?php
	$sumador_total=0;
	$iva_total=0;
	$sql=mysqli_query($con, "select * from products, cotizacion, detalle_cotizacion where cotizacion.numero_cotizacion=detalle_cotizacion.numero_cotizacion and  cotizacion.id_cotizacion='$id_factura' and products.id_producto=detalle_cotizacion.id_producto");
	while ($row=mysqli_fetch_array($sql))
	{
	$id_producto=$row['id_producto'];
	$id_detalle=$row["id_detalle"];
	$codigo_producto=$row['codigo_producto'];
	$cantidad=$row['cantidad'];
	$nombre_producto=$row['nombre_producto'];
	$iva=$row['iva_p'];
	$desc=$row['descuento_p'];
	
	$precio_venta=$row['precio_venta'];
	$precio_venta_f=number_format($precio_venta,2);//Formateo variables
	$precio_venta_r=str_replace(",","",$precio_venta_f);//Reemplazo las comas
	$precio_total=$precio_venta_r*$cantidad;
	$precio_total_f=number_format($precio_total,2);//Precio total formateado
	$precio_total_r=str_replace(",","",$precio_total_f);//Reemplazo las comas
	$sumador_total+=$precio_total_r;//Sumador
	$iva_total+=$iva;
		?>
		<tr>
			<input type="hidden" name="" id="id_detale_<?php echo $id_detalle; ?>" value="<?php echo $id_producto; ?>" placeholder="">
			<td class='text-center'><?php echo $codigo_producto;?></td>
			<td class='text-center'><?php echo $cantidad;?></td>
			<td><?php echo $nombre_producto;?></td>
			<td class='text-right'><?php echo $precio_venta_f;?></td>
			<td class='text-right'><?php echo $desc; ?></td>
			<td class='text-right'><?php echo $iva; ?></td>
			<td class='text-right'><?php echo $precio_total_f;?></td>
			<td class='text-center'><a href="#" onclick="eliminar('<?php echo $id_detalle ?>')"><i class="glyphicon glyphicon-trash"></i></a></td>
			
		</tr>		
		<?php
	}
	$subtotal=number_format($sumador_total,2,'.','');
	$total_iva=$iva_total;
	$total_iva=number_format($total_iva,2,'.','');
	$total_factura=$subtotal+$total_iva;

	$update=mysqli_query($con,"update cotizacion set total='$total_factura',sub_total='$subtotal' where id_cotizacion='$id_factura'");
?>
<tr>
<td></td>
<td></td>
	<td class='text-right' colspan=4>SUBTOTAL $</td>
	<td class='text-right'><?php echo number_format($subtotal,2);?></td>
	<td></td>
</tr>
<tr>
<td></td>
<td></td>
	<td class='text-right' colspan=4>IVA (<?php echo 15?>)% $</td>
	<td class='text-right'><?php echo number_format($total_iva,2);?></td>
	<td></td>
</tr>
<tr>
<td></td>
<td></td>
	<td class='text-right' colspan=4>TOTAL $</td>
	<td class='text-right'><?php echo number_format($total_factura,2);?></td>
	<td></td>
</tr>

</table>
</div>