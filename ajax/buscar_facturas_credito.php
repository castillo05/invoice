<?php
error_reporting(0);
	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	
				if (isset($_GET['id']) and $_SESSION['permiso_user'] == 1){
					$numero_factura=intval($_GET['id']);

					$consulta=mysqli_query($con,"select * from cuentas_cobrar where num_fact='".$_GET['id']."'");
					$contar=mysqli_num_rows($consulta);
						if ($contar<1) {


					$del1="delete from facturas where numero_factura='".$numero_factura."'";
					$del2="delete from detalle_factura where numero_factura='".$numero_factura."'";
					if ($delete1=mysqli_query($con,$del1) and $delete2=mysqli_query($con,$del2)){
						?>
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Aviso!</strong> Datos eliminados exitosamente
						</div>
						<?php 
					}else {
						?>
						<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Error!</strong> No se puedo eliminar los datos
						</div>
						<?php
						
					}

					}else{

						?>
						<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Error!</strong> Esta factura esta relacionada a una cuenta por cobrar
						</div>
						<?php

					}
				}
			
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
		  $sTable = "facturas, clientes, users,cuentas_cobrar";
		 $sWhere = "";
		 $sWhere.=" WHERE facturas.id_cliente=clientes.id_cliente and facturas.id_vendedor=users.user_id and cuentas_cobrar.id_cliente=facturas.id_cliente and facturas.numero_factura=cuentas_cobrar.num_fact";
		if ( $_GET['q'] != "" )
		{
		$sWhere.= " and  (clientes.nombre_cliente like '%$q%' or facturas.numero_factura like '%$q%')";
			
		}
		
		$sWhere.=" order by facturas.id_factura desc";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 10; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './facturas.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			echo mysqli_error($con);
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="info">
					<th>#</th>
					<th>Fecha</th>
					<th>Cliente</th>
					<th>Vendedor</th>
					<th>Estado</th>
					<th class='text-right'>Total</th>
					<th class='text-right'>Acciones</th>
					
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						$id_factura=$row['id_factura'];
						$numero_factura=$row['numero_factura'];
						$fecha=date("d/m/Y", strtotime($row['fecha_factura']));
						$nombre_cliente=$row['nombre_cliente'];
						$telefono_cliente=$row['telefono_cliente'];
						$email_cliente=$row['email_cliente'];
						$nombre_vendedor=$row['firstname']." ".$row['lastname'];
						$estado_factura=$row['estado_factura'];
						if ($estado_factura==1){
							$text_estado="Pagada";$label_class='label-info';
						}elseif ($estado_factura==2) {
							$text_estado="Pendiente";$label_class='label-warning';
						}elseif ($estado_factura==3) {
							$text_estado="Vencida";$label_class='label-danger';
						}else{$text_estado="Anulada";$label_class='label-danger';}
						$condiciones=$row['condiciones'];
						$total_venta=$row['total_venta'];
						$fecha_actual=date('Y-m-d');
						$vencimiento=$row['vencimiento'];
						if ($vencimiento<$fecha_actual) {
							$text_estado="Vencida";$label_class='label-danger';
						}

						
						/*$fecha1=new DateTime($fecha_actual);
						$fecha2=new DateTime($vencimiento);
						$diferencia=$fecha1->diff($fecha2);

						echo $diferencia->days;
						echo "<br>";
						$cantidad=1000;
						$mora_diaria=0.1666666666667;
						$mora=($diferencia->days*$mora_diaria);
						$monto_mora=($cantidad*$mora)/100;
						$total_cobro=$monto_mora+$cantidad;
						echo $monto_mora;
						echo "<br>";
						echo $total_cobro;*/

						// $f=date('Y-06-15');
						// 	//echo $f;
						// $p=date('2017-07-15');
						// echo "<br>";
						// if ($vencimiento<$fecha_actual) {
						// 	$fecha1=new DateTime($vencimiento);
						// 	$fecha2=new DateTime($fecha_actual);
						// 	$diferencia=$fecha1->diff($fecha2);
						// 	//$cantidad=1000;
						// 	$mora_diaria=0.1666666666667;
						// 	$mora=($diferencia->days*$mora_diaria);
						// 	$monto_mora=($total_venta*$mora)/100;
						// 	$total_cobro=$monto_mora+$total_venta;
						// 	echo $mora;
						// 	echo "<br>";
						// 	echo $monto_mora;
						// 	echo "<br>";
						// 	echo $total_venta;	

						// 	echo "<br>";
						// 	echo $total_cobro;
						// 	echo "<br>";
						// } else {
						// 	$fecha1=new DateTime($vencimiento);
						// 	$fecha2=new DateTime($fecha_actual);
						// 	$diferencia=$fecha1->diff($fecha2);
						// 	//$cantidad=1000;
						// 	$mora_diaria=0;
						// 	$mora=($diferencia->days*$mora_diaria);
						// 	$monto_mora=($total_venta*$mora)/100;
						// 	$total_cobro=$monto_mora+$total_venta;
						// 	/*echo $mora;
						// 	echo "<br>";
						// 	echo $monto_mora;
						// 	echo "<br>";
						// 	echo $total_venta;	

						// 	echo "<br>";
						// 	echo $total_cobro;
						// 	echo "<br>";*/
						// }
						

					?>
					<tr>
						<td><?php echo $numero_factura; ?></td>
						<td><?php echo $fecha; ?></td>
						<td><a href="#" data-toggle="tooltip" data-placement="top" title="<i class='glyphicon glyphicon-phone'></i> <?php echo $telefono_cliente;?><br><i class='glyphicon glyphicon-envelope'></i>  <?php echo $email_cliente;?>" ><?php echo $nombre_cliente;?></a></td>
						<td><?php echo $nombre_vendedor; ?></td>
						<td><span class="label <?php echo $label_class;?>"><?php echo $text_estado; ?></span></td>
						<td class='text-right'><?php echo number_format ($total_venta,2); ?></td>					
					<td class="text-right">
					<?php
						if ($_SESSION['permiso_user']==1) {?>
							<a href="editar_factura.php?id_factura=<?php echo $id_factura;?>&numero_factura=<?php echo $numero_factura; ?>&estado_factura=<?php echo $estado_factura; ?>&condiciones=<?php echo $condiciones; ?>" class='btn btn-default' title='Editar factura' ><i class="glyphicon glyphicon-edit"></i></a> 
							<a href="#" class='btn btn-default' title='Descargar factura' onclick="imprimir_factura('<?php echo $id_factura;?>');"><i class="glyphicon glyphicon-download"></i></a> 
							<!-- <a href="#" class='btn btn-default' title='Borrar factura' onclick="eliminar('<?php //echo $numero_factura; ?>')"><i class="glyphicon glyphicon-trash"></i> </a> -->

					<?php
						}elseif($_SESSION['permiso_user']==2){?>
							<a href="editar_factura.php?id_factura=<?php echo $id_factura;?>&numero_factura=<?php echo $numero_factura; ?>&estado_factura=<?php echo $estado_factura; ?>&condiciones=<?php echo $condiciones; ?>" class='btn btn-default' title='Editar factura' ><i class="glyphicon glyphicon-edit"></i></a>
							<a href="#" class='btn btn-default' title='Descargar factura' onclick="imprimir_factura('<?php echo $id_factura;?>');"><i class="glyphicon glyphicon-download"></i></a> 
							
					<?php

						}

					?>
						
					</td>
						
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=7><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>