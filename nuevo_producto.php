<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
		exit;
        }

	
	$active_facturas="active";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Facturas | Control Total";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<?php include("head.php");?>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	?>  
<div class="content-wrapper">
    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Agregar Productos</h3>
              <div class="btn-group pull-right">
               <a href="nueva_marca.php"><span class="glyphicon glyphicon-plus" ></span> Nueva Marca</a>
       <!--  <button type='button' class="btn btn-info" data-toggle="modal" data-target="#nuevoProveedor"><span class="glyphicon glyphicon-plus" ></span> Nuevo Proveedor</button>
      </div> -->
            </div>
<form class="form-horizontal" method="post" id="guardar_producto" name="guardar_producto">
      <div id="resultados_ajax_productos"></div>
        <div class="form-group">
        <label for="codigo" class="col-sm-3 control-label">Código</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Código del producto">
        </div>
        </div>
        
        <div class="form-group">
        <label for="nombre" class="col-sm-3 control-label">Nombre</label>
        <div class="col-sm-8">
          <textarea class="form-control" id="nombre" name="nombre" placeholder="Nombre del producto" required maxlength="255" ></textarea>
          
        </div>
        </div>

         <div class="form-group">
        <label for="unidad_m" class="col-sm-3 control-label">Unidad de Medida</label>
        <div class="col-sm-8">
         <input type="text" class="form-control input-sm" id="unidad_m" name="unidad_m" placeholder="Selecciona una unidad de medida" required>
        <input id="id_medida" name="id_medida" type='hidden'> 
          
        </div>
        </div>

        <div class="form-group">
        <label for="unidad_m" class="col-sm-3 control-label">Proveedor</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="prov" value="" id="prov" placeholder="Ingrese el Proveedor" >
          <input id="cod_prov" name="cod_prov" type='hidden'> 
        </div>
        </div>

         <div class="form-group">
        <label for="unidad_m" class="col-sm-3 control-label">Marca</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="marca" value="" id="marca" placeholder="Ingrese la marca" >
          <input id="id_marca" name="id_marca" type='hidden'> 
        </div>
        </div>
        
        <div class="form-group">
        <label for="estado" class="col-sm-3 control-label">Estado</label>
        <div class="col-sm-8">
         <select class="form-control" id="estado" name="estado" required>
          <option value="">-- Selecciona estado --</option>
          <option value="1" selected>Activo</option>
          <option value="0">Inactivo</option>
          </select>
        </div>
        </div>

        <div class="form-group">
        <label for="precio" class="col-sm-3 control-label">Precio Compra</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="precio_compra" name="precio_compra" placeholder="Precio de compra del producto" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
        </div>
        </div>

        <div class="form-group">
        <label for="precio" class="col-sm-3 control-label">Precio Venta</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="precio" name="precio" placeholder="Precio de venta del producto" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
        </div>
        </div>

        

       <div class="form-group">
        <label for="stock" class="col-sm-3 control-label">Stock</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="stock" name="stock" placeholder="Cantidad de producto"  pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
        </div>
        </div>

         <div class="form-group">
        <label for="stock" class="col-sm-3 control-label">Cantidad Total</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="cant_total" name="cant_total" placeholder="Cantidad de producto" pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
        </div>
        </div>
       
        <div class="form-group">
        <label for="ex_min" class="col-sm-3 control-label">Existecia Minima</label>
        <div class="col-sm-8">
          <input value="1" type="text" class="form-control" id="ex_min" name="ex_min" placeholder="Existencia Minima" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
        </div>
        </div>

         

         <div class="form-group">
        <label for="exento" class="col-sm-3 control-label">Exento</label>
        <div class="col-sm-8">
          <select class="form-control" name="exento" id="exento">
            <option value="1">Si</option>
            <option value="2">No</option>
            option
          </select>
        </div>
        </div>
      
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      <button type="submit" class="btn btn-primary" id="guardar_datos">Guardar datos</button>
      </div>
      </form>

           
          </div>
    		</div>
    	</div>
      </div>
    </section>
	</div>

	
    
	<?php
	include("footer.php");
	?>
  <link rel="stylesheet" href="jquery-ui-themes-1.12.0/jquery-ui.css">
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	
  <script>
$( "#guardar_producto" ).submit(function( event ) {
  $('#guardar_datos').attr("disabled", true);
  
 var parametros = $(this).serialize();
   $.ajax({
      type: "POST",
      url: "ajax/nuevo_producto.php",
      data: parametros,
       beforeSend: function(objeto){
        $("#resultados_ajax_productos").html("Mensaje: Cargando...");
        },
      success: function(datos){
      $("#resultados_ajax_productos").html(datos);
      $('#guardar_datos').attr("disabled", false);
      load(1);
      }
  });
  event.preventDefault();

 var nombre= document.getElementById('nombre');
 var unidad_m=document.getElementById('id_medida');
 var prov=document.getElementById('cod_prov');
 var marca=document.getElementById('id_marca');
 var estado=document.getElementById('estado');
 var precio_compra=document.getElementById('precio');
 var precio=document.getElementById('precio');
 var stock=document.getElementById('stock');
 var cant_total=document.getElementById('cant_total');
 var ex_min=document.getElementById('ex_min');
 unidad_m.value="";
 prov.value="";
 marca.value="";
 estado.value="";
 precio_compra.value="";
 precio.value="";
 stock.value="";
 cant_total.value="";
 ex_min.value="";
 precio.value="";
 nombre.value="";
})

/*$( "#editar_producto" ).submit(function( event ) {
  $('#actualizar_datos').attr("disabled", true);
  
 var parametros = $(this).serialize();
   $.ajax({
      type: "POST",
      url: "ajax/editar_producto.php",
      data: parametros,
       beforeSend: function(objeto){
        $("#resultados_ajax2").html("Mensaje: Cargando...");
        },
      success: function(datos){
      $("#resultados_ajax2").html(datos);
      $('#actualizar_datos').attr("disabled", false);
      load(1);
      }
  });
  event.preventDefault();
})

  function obtener_datos(id){
      var codigo_producto = $("#codigo_producto"+id).val();
      var nombre_producto = $("#nombre_producto"+id).val();
      var estado = $("#estado"+id).val();
      var precio_producto = $("#precio_producto"+id).val();
      var cant_total= $("#cantidad_total"+id).val();
      var ex_min = $("#ex_min"+id).val();
      var stock = $("#stock"+id).val();
      var exento = $("#exento"+id).val();
      $("#mod_id").val(id);
      $("#mod_codigo").val(codigo_producto);
      $("#mod_nombre").val(nombre_producto);
      $("#mod_precio").val(precio_producto);
      $("#mod_cantidad").val(cant_total);
      $("#mod_ex_min").val(ex_min);
      $("#mod_stock").val(stock);
      $("#mod_exento").val(exento);
    }*/
</script>

<script>
  $("form").keypress(function(e) {
        if (e.which == 13) {
          var parametros = $(this).serialize();
   $.ajax({
      type: "POST",
      url: "ajax/nuevo_producto.php",
      data: parametros,
       beforeSend: function(objeto){
        $("#resultados_ajax_productos").html("Mensaje: Cargando...");
        },
      success: function(datos){
      $("#resultados_ajax_productos").html(datos);
      $('#guardar_datos').attr("disabled", false);
      load(1);
      }
  });  
        }
    });

  // body...
//   $( "#codigo" ).dblclick(function( event ) {
//   $('#guardar_datos').attr("disabled", true);
  
 
//   event.preventDefault();
// })



</script>
<script>
  $(document).ready(function() {
   document.getElementById('codigo').focus();
});
</script>
<script>
    $(function() {
            $("#prov").autocomplete({
              source: "./ajax/autocomplete/proveedor.php",
              minLength: 2,
              select: function(event, ui) {
                event.preventDefault();
                $('#cod_prov').val(ui.item.cod_prov);
                $('#prov').val(ui.item.nombre);
                // $('#tel1').val(ui.item.telefono_cliente);
                // $('#mail').val(ui.item.email_cliente);
                                
                
               }
            });
             
            
          });
          
  $("#prov" ).on( "keydown", function( event ) {
            if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
            {
              $("#id_cliente" ).val("");
              $("#tel1" ).val("");
              $("#mail" ).val("");
                      
            }
            if (event.keyCode==$.ui.keyCode.DELETE){
              $("#nombre_cliente" ).val("");
              $("#id_cliente" ).val("");
              $("#tel1" ).val("");
              $("#mail" ).val("");
            }
      }); 
  </script>



  <script>
    $(function() {
            $("#unidad_m").autocomplete({
              source: "./ajax/autocomplete/uniadad.php",
              minLength: 2,
              select: function(event, ui) {
                event.preventDefault();
                $('#id_medida').val(ui.item.id_medida);
                $('#unidad_m').val(ui.item.nombre_medida);
                // $('#tel1').val(ui.item.telefono_cliente);
                // $('#mail').val(ui.item.email_cliente);
                                
                
               }
            });
             
            
          });
          
  $("#unidad_m" ).on( "keydown", function( event ) {
            if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
            {
              $("#id_medida" ).val("");
             
                      
            }
            if (event.keyCode==$.ui.keyCode.DELETE){
              $("#nombre_medida" ).val("");
              $("#id_medida" ).val("");
             
            }
      }); 
  </script>




  <script>
    $(function() {
            $("#marca").autocomplete({
              source: "./ajax/autocomplete/marca.php",
              minLength: 2,
              select: function(event, ui) {
                event.preventDefault();
                $('#id_marca').val(ui.item.id_marca);
                $('#marca').val(ui.item.nombre_marca);
                // $('#tel1').val(ui.item.telefono_cliente);
                // $('#mail').val(ui.item.email_cliente);
                                
                
               }
            });
             
            
          });
          
  $("#marca" ).on( "keydown", function( event ) {
            if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
            {
              $("#id_marca" ).val("");
             
                      
            }
            if (event.keyCode==$.ui.keyCode.DELETE){
              $("#marca" ).val("");
              $("#id_marca" ).val("");
             
            }
      }); 
  </script>

  </body>
</html>

