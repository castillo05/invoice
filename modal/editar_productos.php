	<?php
		if (isset($con))
		{
	?>
	<!-- Modal -->
	<div class="modal fade modal-warning" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Editar producto</h4>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal" method="post" id="editar_producto" name="editar_producto">
			<div id="resultados_ajax2"></div>
			  <div class="form-group">
				<label for="mod_codigo" class="col-sm-3 control-label">Código</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="mod_codigo" name="mod_codigo" placeholder="Código del producto" required>
					<input type="hidden" name="mod_id" id="mod_id">
				</div>
			  </div>
			   <div class="form-group">
				<label for="mod_nombre" class="col-sm-3 control-label">Nombre</label>
				<div class="col-sm-8">
				  <textarea class="form-control" id="mod_nombre" name="mod_nombre" placeholder="Nombre del producto" required></textarea>
				</div>
			  </div>

			  <div class="form-group">
				
				
					
				  <input  class="form-control" type="hidden" name="mod_unidad_m" value="" id="mod_unidad_m" placeholder="">
				  <!--<input type="hidden" style="color:black;" name="id_medida" id="id_medida">-->
				

				<label for="empresa" class="col-md-3 control-label">Unidad de Medida</label>
							<div class="col-md-8">
								<select class="form-control" id="id_medida" name="id_medida">
									<?php
										$sql_vendedor=mysqli_query($con,"select * from unidad_medida");
										while ($rw=mysqli_fetch_array($sql_vendedor)){
											$id_medida=$rw["id_medida"];
											$nombre_medida=$rw["nombre_medida"];
											$selected="";
											?>
											<option name="id_medida" id="id_medida" value="<?php echo $id_medida?>" <?php echo $selected;?>><?php echo $nombre_medida?></option>
											<?php
										}
									?>
								</select>
							</div>



			  </div>
			   <div class="form-group">
				
				
					<input class="form-control" type="hidden" name="mod_prov" value="" id="mod_prov" placeholder="Ingrese el Proveedor">
				  	
					
					<label for="empresa" class="col-md-3 control-label">Proveedor</label>
							<div class="col-md-8">
								<select class="form-control" name="id_prov" id="id_prov">
									<?php
										$sql_vendedor=mysqli_query($con,"select * from proveedores");
										while ($rw=mysqli_fetch_array($sql_vendedor)){
											$cod_prov=$rw["cod_prov"];
											$nombre=$rw["nombre"];
											$selected="";
											?>
											<option name="id_prov" id="id_prov" value="<?php echo $cod_prov?>" <?php echo $selected;?>><?php echo $nombre?></option>
											<?php
										}
									?>
								</select>
							</div>
					

				</div>
			  
				 <div class="form-group">
			  
				
					<input class="form-control" type="hidden" name="mod_marca" value="" id="mod_marca" placeholder="Ingrese la marca">
				  	 
			


				<label for="empresa" class="col-md-3 control-label">Marca</label>
							<div class="col-md-8">
								<select class="form-control" id="id_marca" name="id_marca">
									<?php
										$sql_vendedor=mysqli_query($con,"select * from marcas order by nombre_marca asc");
										while ($rw=mysqli_fetch_array($sql_vendedor)){
											$id_marca=$rw["id_marca"];
											$nombre_marca=$rw["nombre_marca"];
											$selected="";
											?>
											<option id="id_marca" name="id_marca" value="<?php echo $id_marca?>" <?php echo $selected;?>><?php echo $nombre_marca?></option>
											<?php
										}
									?>
								</select>
							</div>
					

			  </div>
			  <div class="form-group">
				<label for="mod_estado" class="col-sm-3 control-label">Estado</label>
				<div class="col-sm-8">
				 <select class="form-control" id="mod_estado" name="mod_estado" required>
					<option value="">-- Selecciona estado --</option>
					<option value="1" selected>Activo</option>
					<option value="0">Inactivo</option>
				  </select>
				</div>
			  </div>
				
				<div class="form-group">
				<label for="mod_precio" class="col-sm-3 control-label">Precio Compra</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="mod_precio_compra" name="mod_precio_compra" placeholder="Precio de compra del producto" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
				</div>
			  </div>

			  <div class="form-group">
				<label for="mod_precio" class="col-sm-3 control-label">Precio Venta</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="mod_precio" name="mod_precio" placeholder="Precio de venta del producto" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
				</div>
			  </div>


			   <div class="form-group">
				<label for="stock" class="col-sm-3 control-label">Stock</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="mod_stock" name="mod_stock" placeholder="Cantidad de producto" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
				</div>
			  </div>


			   <div class="form-group">
				<label for="stock" class="col-sm-3 control-label">Cantidad</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="mod_cantidad" name="mod_cantidad" placeholder="Cantidad de producto" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
				</div>
			  </div>
			 

			  <div class="form-group">
				<label for="ex_min" class="col-sm-3 control-label">Existecia Minima</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="mod_ex_min" name="mod_ex_min" placeholder="Existencia Minima" required pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" title="Ingresa sólo números con 0 ó 2 decimales" maxlength="8">
				</div>
			  </div>

			  

			  <div class="form-group">
				<label for="exento" class="col-sm-3 control-label">Exento</label>
				<div class="col-sm-8">
				  <select class="form-control" name="mod_exento" id="mod_exento">
				  	<option value="">Seleccion</option>
				  	<option value="1" selected>Si</option>
				  	<option value="2">No</option>
				  	
				  </select>
				</div>
			  </div>
			 
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="actualizar_datos">Actualizar datos</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>


	<?php
		}
	?>
	