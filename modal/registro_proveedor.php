	<?php
		if (isset($con))
		{
	?>
	<!-- Modal -->
	<div class="modal fade modal-primary" id="nuevoProveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Agregar nuevo cliente</h4>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal" method="post" id="guardar_proveedor" name="guardar_proveedor">
			<div id="resultados_ajax"></div>
			 <div class="form-group">
				<label for="nombre" class="col-sm-3 control-label">Cedula</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cedula o # Ruc" required>
				</div>
			</div>
			  <div class="form-group">
				<label for="nombre" class="col-sm-3 control-label">Nombre</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del Proveedor" required>
				</div>
			  </div>
			   <div class="form-group">
				<label for="nombre" class="col-sm-3 control-label">Direccion</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direccion del Proveedor" required>
				</div>
			</div>
			  <div class="form-group">
				<label for="telefono" class="col-sm-3 control-label">Teléfono</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ej. 1234-5678">
				</div>
			  </div>
			   <div class="form-group">
				<label for="nombre" class="col-sm-3 control-label">Celular</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="celular" name="celular" placeholder="Ej. 1234-5678" required>
				</div>
			  </div>
			  <div class="form-group">
				<label for="email" class="col-sm-3 control-label">Email</label>
				<div class="col-sm-8">
					<input type="email" class="form-control" id="email" name="email" placeholder="Ej. jorgeantonio20142014@gmail.com">
				  
				</div>
			  </div>
			  
			 
			  
			  <div class="form-group">
				<label for="estado" class="col-sm-3 control-label">Estado</label>
				<div class="col-sm-8">
				 <select class="form-control" id="estado" name="estado" required>
					<option value="">-- Selecciona estado --</option>
					<option value="1" selected>Activo</option>
					<option value="2">Inactivo</option>
				  </select>
				</div>
			  </div>
			 
			 
			 
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn-primary" id="guardar_datos">Guardar datos</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	<?php
		}
	?>