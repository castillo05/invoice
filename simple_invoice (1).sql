-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-09-2016 a las 15:26:49
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `simple_invoice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(255) NOT NULL,
  `telefono_cliente` char(30) NOT NULL,
  `email_cliente` varchar(64) NOT NULL,
  `direccion_cliente` varchar(255) NOT NULL,
  `status_cliente` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id_cliente`),
  UNIQUE KEY `codigo_producto` (`nombre_cliente`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `telefono_cliente`, `email_cliente`, `direccion_cliente`, `status_cliente`, `date_added`) VALUES
(1, 'jorge castillo', '86165414', 'ciberclub.club@gmail.com', '', 1, '2016-08-25 23:24:04'),
(4, 'jose', '123456', 'jose@gmail.com', 'el sauce', 1, '2016-09-02 20:51:50'),
(3, 'Pedro Pastora', '12345678', '', '', 1, '2016-08-29 22:01:16'),
(5, 'Emilio', '12345678', 'emilio@gmail.com', 'edfdfdf', 0, '2016-09-09 15:58:28'),
(6, 'marcos castillo', '', '', '', 1, '2016-09-14 19:32:01'),
(7, 'jorbely castillo', '86165414', '', '', 1, '2016-09-17 04:46:00'),
(8, 'MARIA ELENA MORENO CHAVARRIA', '89315230', '', 'DEL CALVARIO 3C AL ESTE', 1, '2016-09-22 20:44:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_cobrar`
--

CREATE TABLE IF NOT EXISTS `cuentas_cobrar` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `num_fact` varchar(50) NOT NULL,
  `fecha_fact` varchar(20) NOT NULL,
  `vencimiento` varchar(20) NOT NULL,
  `monto` varchar(50) NOT NULL,
  `status` varchar(15) NOT NULL,
  `id_cliente` varchar(20) NOT NULL,
  `saldo_anterior` varchar(50) NOT NULL,
  `abono` varchar(50) NOT NULL,
  `saldo_actual` varchar(50) NOT NULL,
  `total_abono` varchar(50) NOT NULL,
  `mes` varchar(10) NOT NULL,
  `anio` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `cuentas_cobrar`
--

INSERT INTO `cuentas_cobrar` (`id`, `num_fact`, `fecha_fact`, `vencimiento`, `monto`, `status`, `id_cliente`, `saldo_anterior`, `abono`, `saldo_actual`, `total_abono`, `mes`, `anio`) VALUES
(13, '1', '2016-09-25 22:14:11', '2016-09-30', '28.75', 'Vigente', '7', '28.75', '', '28.75', '', '09/2016', '2016'),
(14, '2', '2016-09-25 22:39:08', '2016-09-28', '28.75', 'Vigente', '7', '28.75', '', '28.75', '', '09/2016', '2016');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_factura`
--

CREATE TABLE IF NOT EXISTS `detalle_factura` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `numero_cotizacion` (`numero_factura`,`id_producto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=306 ;

--
-- Volcado de datos para la tabla `detalle_factura`
--

INSERT INTO `detalle_factura` (`id_detalle`, `numero_factura`, `id_producto`, `cantidad`, `precio_venta`) VALUES
(302, 1, 12, 1, 25),
(305, 2, 12, 1, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE IF NOT EXISTS `facturas` (
  `id_factura` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `fecha_factura` date NOT NULL,
  `mes` varchar(20) NOT NULL,
  `anio` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` varchar(30) NOT NULL,
  `sub_total` varchar(20) NOT NULL,
  `iva` varchar(20) NOT NULL,
  `total_venta` varchar(20) NOT NULL,
  `estado_factura` tinyint(1) NOT NULL,
  `cheke` varchar(50) NOT NULL,
  `estado` varchar(20) NOT NULL,
  PRIMARY KEY (`id_factura`),
  UNIQUE KEY `numero_cotizacion` (`numero_factura`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=156 ;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id_factura`, `numero_factura`, `fecha_factura`, `mes`, `anio`, `id_cliente`, `id_vendedor`, `condiciones`, `sub_total`, `iva`, `total_venta`, `estado_factura`, `cheke`, `estado`) VALUES
(152, 1, '2016-09-25', '09/2016', 2016, 7, 1, '4', '25.00', '3.75', '28.75', 1, '', 'vigente'),
(155, 2, '2016-09-25', '09/2016', 2016, 7, 1, '4', '25.00', '3.75', '28.75', 2, '', 'vigente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `id_permisos` int(11) NOT NULL,
  `permisos` varchar(25) NOT NULL,
  PRIMARY KEY (`id_permisos`),
  KEY `permisos` (`permisos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permisos`, `permisos`) VALUES
(1, 'administrador'),
(2, 'vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_producto` char(20) NOT NULL,
  `nombre_producto` char(255) NOT NULL,
  `status_producto` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `precio_producto` double NOT NULL,
  `stock` int(11) NOT NULL,
  `cantidad_total` int(11) NOT NULL,
  PRIMARY KEY (`id_producto`),
  UNIQUE KEY `codigo_producto` (`codigo_producto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id_producto`, `codigo_producto`, `nombre_producto`, `status_producto`, `date_added`, `precio_producto`, `stock`, `cantidad_total`) VALUES
(11, '1', 'frijol', 1, '2016-09-01 20:39:02', 10, 4, 10),
(10, '2', 'ARROZ', 1, '2016-09-01 20:09:58', 10, 10, 10),
(12, '22', 'cafe', 1, '2016-09-02 17:52:20', 25, 16, 0),
(13, '3', 'azucar', 1, '2016-09-06 15:58:25', 12, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE IF NOT EXISTS `tmp` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8,2) DEFAULT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tmp`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=542 ;

--
-- Volcado de datos para la tabla `tmp`
--

INSERT INTO `tmp` (`id_tmp`, `id_producto`, `cantidad_tmp`, `precio_tmp`, `session_id`) VALUES
(498, 11, 1, 10.00, 'v43b16mubv6auvrv90bsl0mk31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index',
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `date_added` datetime NOT NULL,
  `permiso_user` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`),
  KEY `permiso_user` (`permiso_user`),
  KEY `permiso_user_2` (`permiso_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `user_name`, `user_password_hash`, `user_email`, `date_added`, `permiso_user`) VALUES
(1, 'Jorge', 'Castillo', 'admin', '$2y$10$MPVHzZ2ZPOWmtUUGCq3RXu31OTB.jo7M9LZ7PmPQYmgETSNn19ejO', 'admin@admin.com', '2016-05-21 15:06:00', 1),
(2, 'Antonio', 'Moreno', 'Antonio', '$2y$10$EaFBf0SVwIDUFW7s/56DbOf.k6hgivIYkQRVWN40dNo14vxxLOaue', 'antonio@gmail.com', '2016-09-05 15:41:18', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
