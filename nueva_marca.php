<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
		exit;
        }

	
	$active_facturas="active";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Facturas | Control Total";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<?php include("head.php");?>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	?>  
<div class="content-wrapper">
    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Agregar Marca</h3>
              <div class="btn-group pull-right">
       <!--  <button type='button' class="btn btn-info" data-toggle="modal" data-target="#nuevoProveedor"><span class="glyphicon glyphicon-plus" ></span> Nuevo Proveedor</button>
      </div> -->
            </div>
<form class="form-horizontal" method="post" id="guardar_producto" name="guardar_producto">
      <div id="resultados_ajax_productos"></div>
        <!-- <div class="form-group">
        <label for="codigo" class="col-sm-3 control-label">Código</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Código" required>
        </div>
        </div> -->
        
        <div class="form-group">
        <label for="nombre" class="col-sm-3 control-label">Nombre</label>
        <div class="col-sm-8">
          <textarea class="form-control" id="nombre" name="nombre" placeholder="Nombre de la marca" required maxlength="255" ></textarea>
          
        </div>
        
      
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      <button type="submit" class="btn btn-primary" id="guardar_datos">Guardar datos</button>
      </div>
      </form>

           
          </div>
    		</div>
    	</div>
      </div>
    </section>
	</div>

	
    
	<?php
	include("footer.php");
	?>
  <link rel="stylesheet" href="jquery-ui-themes-1.12.0/jquery-ui.css">
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	
  <script>
$( "#guardar_producto" ).submit(function( event ) {
  $('#guardar_datos').attr("disabled", true);
  
 var parametros = $(this).serialize();
   $.ajax({
      type: "POST",
      url: "ajax/nueva_marca.php",
      data: parametros,
       beforeSend: function(objeto){
        $("#resultados_ajax_productos").html("Mensaje: Cargando...");
        },
      success: function(datos){
      $("#resultados_ajax_productos").html(datos);
      $('#guardar_datos').attr("disabled", false);
      load(1);
      }
  });
  event.preventDefault();
})

/*$( "#editar_producto" ).submit(function( event ) {
  $('#actualizar_datos').attr("disabled", true);
  
 var parametros = $(this).serialize();
   $.ajax({
      type: "POST",
      url: "ajax/editar_producto.php",
      data: parametros,
       beforeSend: function(objeto){
        $("#resultados_ajax2").html("Mensaje: Cargando...");
        },
      success: function(datos){
      $("#resultados_ajax2").html(datos);
      $('#actualizar_datos').attr("disabled", false);
      load(1);
      }
  });
  event.preventDefault();
})

  function obtener_datos(id){
      var codigo_producto = $("#codigo_producto"+id).val();
      var nombre_producto = $("#nombre_producto"+id).val();
      var estado = $("#estado"+id).val();
      var precio_producto = $("#precio_producto"+id).val();
      var cant_total= $("#cantidad_total"+id).val();
      var ex_min = $("#ex_min"+id).val();
      var stock = $("#stock"+id).val();
      var exento = $("#exento"+id).val();
      $("#mod_id").val(id);
      $("#mod_codigo").val(codigo_producto);
      $("#mod_nombre").val(nombre_producto);
      $("#mod_precio").val(precio_producto);
      $("#mod_cantidad").val(cant_total);
      $("#mod_ex_min").val(ex_min);
      $("#mod_stock").val(stock);
      $("#mod_exento").val(exento);
    }*/
</script>

<script>
    $(function() {
            $("#prov").autocomplete({
              source: "./ajax/autocomplete/proveedor.php",
              minLength: 2,
              select: function(event, ui) {
                event.preventDefault();
                $('#cod_prov').val(ui.item.cod_prov);
                $('#prov').val(ui.item.nombre);
                // $('#tel1').val(ui.item.telefono_cliente);
                // $('#mail').val(ui.item.email_cliente);
                                
                
               }
            });
             
            
          });
          
  $("#prov" ).on( "keydown", function( event ) {
            if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
            {
              $("#id_cliente" ).val("");
              $("#tel1" ).val("");
              $("#mail" ).val("");
                      
            }
            if (event.keyCode==$.ui.keyCode.DELETE){
              $("#nombre_cliente" ).val("");
              $("#id_cliente" ).val("");
              $("#tel1" ).val("");
              $("#mail" ).val("");
            }
      }); 
  </script>



  <script>
    $(function() {
            $("#unidad_m").autocomplete({
              source: "./ajax/autocomplete/uniadad.php",
              minLength: 2,
              select: function(event, ui) {
                event.preventDefault();
                $('#id_medida').val(ui.item.id_medida);
                $('#unidad_m').val(ui.item.nombre_medida);
                // $('#tel1').val(ui.item.telefono_cliente);
                // $('#mail').val(ui.item.email_cliente);
                                
                
               }
            });
             
            
          });
          
  $("#unidad_m" ).on( "keydown", function( event ) {
            if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
            {
              $("#id_medida" ).val("");
             
                      
            }
            if (event.keyCode==$.ui.keyCode.DELETE){
              $("#nombre_medida" ).val("");
              $("#id_medida" ).val("");
             
            }
      }); 
  </script>

  </body>
</html>

