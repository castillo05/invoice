-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-09-2016 a las 04:52:52
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `simple_invoice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(255) NOT NULL,
  `telefono_cliente` char(30) NOT NULL,
  `email_cliente` varchar(64) NOT NULL,
  `direccion_cliente` varchar(255) NOT NULL,
  `status_cliente` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id_cliente`),
  UNIQUE KEY `codigo_producto` (`nombre_cliente`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `telefono_cliente`, `email_cliente`, `direccion_cliente`, `status_cliente`, `date_added`) VALUES
(1, 'jorge castillo', '86165414', 'ciberclub.club@gmail.com', '', 1, '2016-08-25 23:24:04'),
(4, 'jose', '123456', 'jose@gmail.com', 'el sauce', 1, '2016-09-02 20:51:50'),
(3, 'Pedro Pastora', '12345678', '', '', 1, '2016-08-29 22:01:16'),
(5, 'Emilio', '12345678', 'emilio@gmail.com', 'edfdfdf', 1, '2016-09-09 15:58:28'),
(6, 'marcos castillo', '', '', '', 1, '2016-09-14 19:32:01'),
(7, 'jorbely castillo', '86165414', '', '', 1, '2016-09-17 04:46:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_factura`
--

CREATE TABLE IF NOT EXISTS `detalle_factura` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `numero_cotizacion` (`numero_factura`,`id_producto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=279 ;

--
-- Volcado de datos para la tabla `detalle_factura`
--

INSERT INTO `detalle_factura` (`id_detalle`, `numero_factura`, `id_producto`, `cantidad`, `precio_venta`) VALUES
(262, 11, 10, 1, 10),
(258, 10, 10, 5, 10),
(257, 9, 11, 5, 10),
(255, 8, 11, 8, 10),
(254, 8, 10, 8, 10),
(256, 9, 10, 5, 10),
(252, 7, 13, 1, 12),
(221, 5, 11, 3, 10),
(224, 6, 10, 1, 10),
(217, 3, 10, 5, 10),
(223, 6, 10, 1, 10),
(222, 5, 10, 2, 10),
(219, 4, 10, 1, 10),
(218, 3, 11, 5, 10),
(220, 4, 11, 1, 10),
(216, 2, 11, 5, 10),
(215, 2, 10, 5, 10),
(214, 1, 10, 8, 15),
(263, 12, 11, 1, 10),
(264, 12, 10, 3, 10),
(265, 13, 11, 2, 10),
(266, 14, 11, 3, 10),
(267, 14, 11, 2, 10),
(268, 15, 11, 3, 10),
(269, 16, 11, 1, 10),
(270, 17, 11, 1, 10),
(271, 18, 10, 1, 10),
(272, 19, 11, 1, 10),
(273, 19, 10, 1, 10),
(274, 19, 12, 2, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE IF NOT EXISTS `facturas` (
  `id_factura` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `fecha_factura` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` varchar(30) NOT NULL,
  `sub_total` varchar(20) NOT NULL,
  `iva` varchar(20) NOT NULL,
  `total_venta` varchar(20) NOT NULL,
  `estado_factura` tinyint(1) NOT NULL,
  `cheke` varchar(50) NOT NULL,
  PRIMARY KEY (`id_factura`),
  UNIQUE KEY `numero_cotizacion` (`numero_factura`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=130 ;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id_factura`, `numero_factura`, `fecha_factura`, `id_cliente`, `id_vendedor`, `condiciones`, `sub_total`, `iva`, `total_venta`, `estado_factura`, `cheke`) VALUES
(100, 1, '2016-09-01', 1, 1, '1', '', '', '135.6', 1, ''),
(101, 2, '2016-09-01', 3, 1, '1', '', '', '113', 1, ''),
(102, 3, '2016-09-01', 1, 1, '1', '', '', '113', 1, ''),
(103, 4, '2016-09-01', 1, 1, '1', '', '', '22.6', 1, ''),
(104, 5, '2016-09-01', 3, 1, '1', '', '', '56.5', 1, ''),
(105, 6, '2016-09-01', 1, 1, '1', '', '', '22.6', 1, ''),
(115, 7, '2016-09-06', 1, 1, '1', '', '', '13.56', 1, ''),
(116, 8, '2016-09-06', 3, 2, '4', '', '', '180.8', 1, ''),
(117, 9, '2016-09-06', 3, 1, '1', '', '', '113', 1, ''),
(118, 10, '2016-09-06', 1, 2, '1', '', '', '56.5', 1, ''),
(120, 11, '2016-09-13', 3, 1, '4', '10.00', '1.30', '11.3', 1, ''),
(121, 12, '2016-09-14', 6, 1, '3', '40.00', '5.20', '45.2', 1, ''),
(122, 13, '2016-09-16', 1, 1, '2', '20.00', '2.60', '22.6', 1, ''),
(123, 14, '2016-09-17', 4, 1, '2', '50.00', '6.50', '56.5', 1, ''),
(124, 15, '2016-09-17', 1, 1, '2', '30.00', '3.90', '33.9', 1, ''),
(125, 16, '2016-09-17', 1, 1, '2', '10.00', '1.30', '11.3', 1, '12345'),
(126, 17, '2016-09-17', 1, 1, '2', '10.00', '1.30', '11.3', 1, '12345'),
(127, 18, '2016-09-17', 1, 1, '2', '10.00', '1.30', '11.3', 1, '12345-2'),
(128, 19, '2016-09-17', 1, 1, '2', '50.00', '6.50', '79.1', 2, '986959'),
(129, 20, '2016-09-17', 7, 1, '4', '200.00', '26.00', '0', 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `id_permisos` int(11) NOT NULL,
  `permisos` varchar(25) NOT NULL,
  PRIMARY KEY (`id_permisos`),
  KEY `permisos` (`permisos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permisos`, `permisos`) VALUES
(1, 'administrador'),
(2, 'vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_producto` char(20) NOT NULL,
  `nombre_producto` char(255) NOT NULL,
  `status_producto` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `precio_producto` double NOT NULL,
  `stock` int(11) NOT NULL,
  `cantidad_total` int(11) NOT NULL,
  PRIMARY KEY (`id_producto`),
  UNIQUE KEY `codigo_producto` (`codigo_producto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id_producto`, `codigo_producto`, `nombre_producto`, `status_producto`, `date_added`, `precio_producto`, `stock`, `cantidad_total`) VALUES
(11, '1', 'frijol', 1, '2016-09-01 20:39:02', 10, 14, 10),
(10, '2', 'ARROZ', 1, '2016-09-01 20:09:58', 10, 20, 10),
(12, '22', 'cafe', 1, '2016-09-02 17:52:20', 25, 90, 0),
(13, '3', 'azucar', 1, '2016-09-06 15:58:25', 12, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE IF NOT EXISTS `tmp` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8,2) DEFAULT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tmp`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=510 ;

--
-- Volcado de datos para la tabla `tmp`
--

INSERT INTO `tmp` (`id_tmp`, `id_producto`, `cantidad_tmp`, `precio_tmp`, `session_id`) VALUES
(498, 11, 1, 10.00, 'v43b16mubv6auvrv90bsl0mk31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index',
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `date_added` datetime NOT NULL,
  `permiso_user` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`),
  KEY `permiso_user` (`permiso_user`),
  KEY `permiso_user_2` (`permiso_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `user_name`, `user_password_hash`, `user_email`, `date_added`, `permiso_user`) VALUES
(1, 'Jorge', 'Castillo', 'admin', '$2y$10$MPVHzZ2ZPOWmtUUGCq3RXu31OTB.jo7M9LZ7PmPQYmgETSNn19ejO', 'admin@admin.com', '2016-05-21 15:06:00', 1),
(2, 'Antonio', 'Moreno', 'Antonio', '$2y$10$EaFBf0SVwIDUFW7s/56DbOf.k6hgivIYkQRVWN40dNo14vxxLOaue', 'antonio@gmail.com', '2016-09-05 15:41:18', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
