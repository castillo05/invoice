<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
		exit;
        }

	
	$active_facturas="active";
	$active_productos="";
	$active_clientes="";
	$active_usuarios="";
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Facturas | Control Total";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<?php include("head.php");?>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	?>  
<div class="content-wrapper">
    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Buscar Factura</h3>
              <div class="btn-group pull-right">
       <!--  <button type='button' class="btn btn-info" data-toggle="modal" data-target="#nuevoProveedor"><span class="glyphicon glyphicon-plus" ></span> Nuevo Proveedor</button>
      </div> -->
            </div>

            <?php include("modal/registro_proveedor.php"); ?>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="proveedor">Nombre del Cliente o # Factura</label>
                  <input type="text" class="form-control" id="q" placeholder="# Factura" onkeyup='load(1);'>
                </div>
                
                
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
               <div class="col-md-3">
                <button type="button" class="btn btn-default" onclick='load(1);'>
                  <span class="glyphicon glyphicon-search" ></span> Buscar</button>
                <span id="loader"></span>
              </div>
              </div>
            </form>

            <div id="resultados"></div><!-- Carga los datos ajax -->
				    <div class='outer_div'></div><!-- Carga los datos ajax -->
          </div>
    		</div>
    	</div>
    </section>
	</div>

	
    
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	<script type="text/javascript" src="js/facturas_credito.js"></script>
  </body>
</html>

