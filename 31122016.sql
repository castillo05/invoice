-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-12-2016 a las 11:45:14
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `simple_invoice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(255) NOT NULL,
  `telefono_cliente` char(30) NOT NULL,
  `email_cliente` varchar(64) NOT NULL,
  `direccion_cliente` varchar(255) NOT NULL,
  `status_cliente` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `telefono_cliente`, `email_cliente`, `direccion_cliente`, `status_cliente`, `date_added`) VALUES
(9, 'Cliente General', '', '', '', 1, '2016-10-03 15:05:33'),
(10, 'Jorge Castillo', '', '', '', 1, '2016-12-22 13:53:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `cod_compra` int(11) NOT NULL,
  `numero_factura` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `estado_compra` int(50) NOT NULL,
  `importe` float NOT NULL,
  `importe_pendiente` float NOT NULL,
  `vencimiento` date NOT NULL,
  `condiciones` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id_cotizacion` int(11) NOT NULL,
  `numero_cotizacion` int(11) NOT NULL,
  `fecha_cotizacion` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` int(20) NOT NULL,
  `validez` int(20) NOT NULL,
  `entrega` varchar(50) NOT NULL,
  `nota` varchar(255) NOT NULL,
  `moneda` int(50) NOT NULL,
  `sub_total` float NOT NULL,
  `descuento` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_cobrar`
--

CREATE TABLE `cuentas_cobrar` (
  `id` int(20) NOT NULL,
  `num_fact` varchar(50) NOT NULL,
  `fecha_fact` varchar(20) NOT NULL,
  `vencimiento` varchar(20) NOT NULL,
  `monto` varchar(50) NOT NULL,
  `id_cliente` varchar(20) NOT NULL,
  `saldo_anterior` varchar(50) NOT NULL,
  `abono` varchar(50) NOT NULL,
  `saldo_actual` varchar(50) NOT NULL,
  `total_abono` varchar(50) NOT NULL,
  `mes` varchar(10) NOT NULL,
  `anio` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compra`
--

CREATE TABLE `detalle_compra` (
  `id_detalle` int(11) NOT NULL,
  `numero_factura` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  `cod_prov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cotizacion`
--

CREATE TABLE `detalle_cotizacion` (
  `id_detalle` int(11) NOT NULL,
  `numero_cotizacion` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descuento_p` float NOT NULL,
  `iva_p` float NOT NULL,
  `precio_venta` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_factura`
--

CREATE TABLE `detalle_factura` (
  `id_detalle` int(11) NOT NULL,
  `numero_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descuento_p` float NOT NULL,
  `iva_p` float NOT NULL,
  `precio_venta` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id_factura` int(11) NOT NULL,
  `numero_factura` int(11) NOT NULL,
  `fecha_factura` date NOT NULL,
  `mes` varchar(20) NOT NULL,
  `anio` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `condiciones` varchar(30) NOT NULL,
  `sub_total` varchar(20) NOT NULL,
  `iva` varchar(20) NOT NULL,
  `total_venta` varchar(20) NOT NULL,
  `estado_factura` tinyint(1) NOT NULL,
  `cheke` varchar(50) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `descuento` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex`
--

CREATE TABLE `kardex` (
  `cod_kardex` int(11) NOT NULL,
  `cod_producto` varchar(100) NOT NULL,
  `fecha_registro` date NOT NULL,
  `movimiento` int(11) NOT NULL,
  `num_documento` varchar(100) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `entradas` int(11) NOT NULL,
  `salidas` int(11) NOT NULL,
  `existencias` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `kardex`
--

INSERT INTO `kardex` (`cod_kardex`, `cod_producto`, `fecha_registro`, `movimiento`, `num_documento`, `descripcion`, `entradas`, `salidas`, `existencias`) VALUES
(31, '45454465', '2016-12-14', 2, '3', 'JBHGHJ', 1, 0, 1),
(32, '7441035322775', '2016-12-14', 2, '3', 'MADETEC TINTE', 1, 0, 1),
(33, '45454465', '2016-12-21', 2, '5', 'JBHGHJ', 1, 0, 1),
(34, '618', '2016-12-22', 1, '3', 'Salidas', 0, 1, -1),
(35, '7441035322775', '2016-12-22', 2, '6', 'MADETEC TINTE', 8, 0, 8),
(36, '618', '2016-12-22', 1, '6', 'Salidas', 0, 8, -8),
(37, '619', '2016-12-22', 1, '5', 'Salidas', 0, 1, -1),
(38, '620', '2016-12-22', 1, '1', 'Salidas', 0, 3, -3),
(39, '620', '2016-12-22', 1, '1', 'Entradas', 3, 0, -3),
(40, '620', '2016-12-22', 1, '1', 'Entradas', 1, 0, -1),
(41, '621', '2016-12-22', 1, '1', 'Entradas', 1, 0, -1),
(42, '620', '2016-12-22', 1, '1', 'salidas', 0, 1, -1),
(43, '621', '2016-12-22', 1, '1', 'salidas', 0, 1, -1),
(44, '45454465', '2016-12-21', 2, '8', 'JBHGHJ', 3, 0, 3),
(45, '619', '2016-12-22', 1, '2', 'Salidas', 0, 1, -1),
(46, '620', '2016-12-22', 1, '2', 'Salidas', 0, 1, -1),
(47, '619', '2016-12-22', 1, '2', 'Entradas', 1, 0, -1),
(48, '619', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(49, '620', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(50, '619', '2016-12-22', 1, '6', 'Salidas', 0, 1, -1),
(51, '621', '2016-12-22', 1, '', 'Entradas', 2, 0, -2),
(52, '620', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(53, '621', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(54, '618', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(55, '620', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(56, '620', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(57, '621', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(58, '619', '2016-12-22', 1, '', 'Entradas', 1, 0, -1),
(59, '620', '2016-12-22', 1, '12', 'Entradas', 2, 0, -2),
(60, '620', '2016-12-23', 1, '1', 'salidas', 0, 1, -1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id_marca` int(11) NOT NULL,
  `nombre_marca` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id_marca`, `nombre_marca`) VALUES
(2, 'SHERWIN WILLIAMS'),
(3, 'PROTECTO'),
(4, 'TRUPER'),
(5, 'CORONA'),
(6, 'LANCO'),
(7, 'OUPONT'),
(8, 'BEST VALUE'),
(9, 'BOXER TOOLS'),
(10, 'PRETUL'),
(11, 'CONICA'),
(12, 'MEGAHOOK'),
(13, 'DIESEL TOOLS'),
(14, 'DOGLEAD'),
(15, 'HUNTER'),
(16, 'HERMEX '),
(17, 'STANLEY'),
(18, 'FIXWORX'),
(19, 'OUPONT'),
(20, 'AMERICAN-ELECTRIC'),
(21, 'EAGLE'),
(22, 'VOLTECH'),
(23, 'MA ELECTRIC'),
(24, 'BTICINO'),
(25, 'BRICKELL'),
(26, 'TRIPP-LITE'),
(27, 'TALTOOOS'),
(28, 'BRINK'),
(29, 'FIERRO'),
(30, 'DEVCON'),
(31, 'LOCTITE'),
(32, 'PHILIPS'),
(33, 'TRESB'),
(34, 'SKY LIGHT'),
(35, 'SUPER-FLEX'),
(36, 'RHODIUS'),
(37, 'UNIVERSAL CUT'),
(38, 'PREMIER'),
(39, 'MAKITA'),
(40, 'DEWALT'),
(41, 'CONINCA'),
(42, 'KORF & HONSBERG'),
(43, 'SYLVANIA'),
(44, 'TARTAN'),
(45, 'TOOLCRAFT'),
(46, 'NORTON'),
(47, 'YALE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_permisos` int(11) NOT NULL,
  `permisos` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permisos`, `permisos`) VALUES
(1, 'administrador'),
(2, 'vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id_producto` int(11) NOT NULL,
  `codigo_producto` varchar(100) NOT NULL,
  `nombre_producto` varchar(255) NOT NULL,
  `unidad_med` varchar(50) NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `marca` varchar(255) NOT NULL,
  `status_producto` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `precio_producto` double NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `cantidad_total` int(11) NOT NULL,
  `ex_min` int(11) NOT NULL,
  `exento` int(11) NOT NULL,
  `salida` int(11) NOT NULL,
  `unidad_m` varchar(255) NOT NULL,
  `marca_p` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id_producto`, `codigo_producto`, `nombre_producto`, `unidad_med`, `proveedor`, `marca`, `status_producto`, `date_added`, `precio_producto`, `precio_compra`, `stock`, `cantidad_total`, `ex_min`, `exento`, `salida`, `unidad_m`, `marca_p`) VALUES
(619, '45454465', 'JBHGHJ', 'GALON', 'Proveedor General', 'PROTECTO', 1, '2016-12-18 11:46:14', 12, 12, 11, 12, 2, 2, 0, '', ''),
(618, '7441035322775', 'MADETEC TINTE nogal', 'litro', 'Proveedor General', 'PROTECTO', 1, '2016-12-18 11:38:18', 320, 320, 6, 7, 3, 1, 0, '', ''),
(762, '8022882055512', 'ESCUADRA FALSA 250MM', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-26 14:17:41', 60, 60, 1, 1, 1, 1, 0, '', ''),
(621, '7419522305891', 'ANTICORROSIVO azul corona', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-18 12:12:52', 340, 340, 11, 11, 1, 1, 0, '', ''),
(622, '7453042889912', 'VENTILADOR DE TECHO', 'UNIDAD', 'Proveedor General', 'CEILING', 1, '2016-12-18 12:15:34', 1500, 500, 3, 4, 1, 1, 0, '', ''),
(623, '7419522281829', 'COLORAMICA LATEX MATE AZUL GEMA', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-19 17:44:49', 250, 250, 4, 4, 1, 1, 0, '', ''),
(624, '7702045342048', 'SILICON TRASPARENTE', 'UNIDAD', 'Proveedor General', 'LOCTITE', 1, '2016-12-19 17:47:47', 60, 60, 12, 12, 1, 1, 0, '', ''),
(625, '845424000973', 'VENTILADOR DE TECHO 56', 'UNIDAD', 'Proveedor General', 'WESTINGHOUSE', 1, '2016-12-19 17:52:38', 1900, 1900, 1, 1, 1, 1, 0, '', ''),
(626, '079340187184', 'silicon gris', 'unidad', 'Proveedor General', 'LOCTITE', 1, '2016-12-23 13:44:36', 105, 90, 17, 17, 1, 1, 0, '', ''),
(627, '078727999396', 'SILICON GRIS', 'UNIDAD', 'Proveedor General', 'mega grey', 1, '2016-12-23 13:48:10', 75, 75, 11, 11, 1, 1, 0, '', ''),
(628, '7453001155607', 'cuchilla para maquina corta azulejo', 'unidad', 'Proveedor General', 'best value', 1, '2016-12-23 13:51:22', 100, 100, 9, 9, 1, 1, 0, '', ''),
(629, '7501206679913', 'juego de autocle o copas', 'unidad', 'Proveedor General', ' pretul', 1, '2016-12-23 14:03:01', 250, 250, 1, 1, 1, 1, 0, '', ''),
(630, '75012619315', 'calibrador para neumaticos', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-23 14:05:31', 260, 260, 0, 0, 1, 1, 0, '', ''),
(631, '7501206647318', 'copa # 14mm', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-23 14:08:23', 40, 40, 6, 6, 1, 1, 0, '', ''),
(632, '7501206644416', 'copa#10mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-23 14:13:46', 65, 65, 5, 5, 1, 1, 0, '', ''),
(633, '7891265403360', 'hoja de sierra safe flex', 'unidad', 'Proveedor General', 'starrett', 1, '2016-12-23 14:19:02', 35, 35, 0, 0, 1, 1, 0, '', ''),
(634, '7419522202725', 'Anticorrosivo coloramica color rojo', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-23 14:20:15', 340, 340, 3, 3, 1, 1, 0, '', ''),
(635, '7419522281911', 'Coloramica latex verde aqua', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-23 14:23:59', 250, 250, 10, 10, 1, 1, 0, '', ''),
(636, '7506240611455', 'linterna para cabeza', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-23 14:25:29', 390, 390, 1, 1, 1, 1, 0, '', ''),
(637, '7441105609072', 'Barniz tinte caoba', 'Cuarto', 'Proveedor General', 'Sur', 1, '2016-12-23 14:28:36', 260, 260, 4, 4, 1, 1, 0, '', ''),
(638, '7501206647349', 'copa 17mm', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-23 14:35:44', 37, 37, 5, 5, 1, 1, 0, '', ''),
(639, '7441035373050', 'tinte cherry para madera madetec ', 'litro', 'Proveedor General', 'PROTECTO', 1, '2016-12-23 14:41:08', 365, 345, 12, 12, 1, 1, 0, '', ''),
(640, '7419522312172', 'aceite limpia madera ', 'unidad ', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-23 14:49:28', 325, 325, 5, 5, 1, 1, 0, '', ''),
(641, '7441018834899', 'Pega pvc', 'Un octavo', 'Proveedor General', 'Durman', 1, '2016-12-23 14:52:56', 150, 150, 0, 0, 1, 1, 0, '', ''),
(642, '7419522281034', 'LATEX COLOR MATE BLANCO HUESO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-23 14:57:43', 250, 250, 2, 2, 1, 1, 0, '', ''),
(643, '1111', 'super acril 100-06', 'unidad', 'Proveedor General', 'industrias zurqui', 1, '2016-12-23 15:04:17', 200, 200, 0, 0, 1, 1, 0, '', ''),
(644, '7419522227483', 'ESMALTE BRILLANTE MORADO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-23 15:04:58', 385, 385, 4, 4, 1, 1, 0, '', ''),
(645, '5180500012408', 'manguera para jardin 15m', 'unidad', 'Proveedor General', 'taltools', 1, '2016-12-23 15:07:58', 350, 350, 2, 2, 1, 1, 0, '', ''),
(646, '709631695752', 'manguera para jardin 22.5m ', 'unidad ', 'Proveedor General', 'boxer tools', 1, '2016-12-23 15:14:46', 700, 700, 1, 1, 1, 1, 0, '', ''),
(647, '718594010151', 'masilla acrilica multiusos interior exterior blanca', ' litros', 'Proveedor General', 'lanco', 1, '2016-12-23 15:38:37', 165, 145, 7, 7, 1, 1, 0, '', ''),
(648, '718594146164', 'tapagotera stopleak rojo', 'unidad 1/2 ltro', 'Proveedor General', 'lanco', 1, '2016-12-23 15:42:58', 110, 90, 11, 11, 1, 1, 0, '', ''),
(649, '718594146157', 'tapagotera estopleak rojo', 'litro', 'Proveedor General', 'lanco', 1, '2016-12-23 15:45:30', 210, 190, 5, 5, 1, 1, 0, '', ''),
(650, '7453042871542', 'soporte para tv 26 a 55pulg', 'unidad', 'Proveedor General', 'hunter', 1, '2016-12-23 15:50:52', 800, 800, 1, 1, 1, 1, 0, '', ''),
(651, '7453013143531', 'antena tv redonda ', 'unidad', 'Proveedor General', 'iml', 1, '2016-12-23 15:56:02', 240, 240, 3, 3, 1, 1, 0, '', ''),
(652, '7453001163817', 'tirolera manual', 'unidad', 'Proveedor General', 'best value', 1, '2016-12-23 15:59:36', 360, 360, 1, 1, 1, 1, 0, '', ''),
(653, '078727442090', 'pega 4minute steel', 'unidad', 'Proveedor General', 'versalchem', 1, '2016-12-23 16:04:11', 68, 68, 8, 8, 1, 1, 0, '', ''),
(654, '7702045342062', 'silicon rojo', 'unidad', 'Proveedor General', 'loctite', 1, '2016-12-23 16:06:35', 45, 45, 4, 4, 1, 1, 0, '', ''),
(655, '050139703006', 'mecha de lampazo con palo ', 'unidad mediano', 'Proveedor General', 'abco', 1, '2016-12-23 16:09:26', 80, 80, 1, 1, 1, 1, 0, '', ''),
(656, '079567520085', 'aceite penetrante ', '8onz', 'Proveedor General', 'wd-40', 1, '2016-12-23 16:12:02', 100, 100, 16, 16, 16, 1, 0, '', ''),
(657, '7501206638927', 'lona de uso rudo o carpa 4x5m', 'unidad ', 'Proveedor General', 'TRUPER', 1, '2016-12-23 16:20:52', 900, 900, 0, 0, 1, 1, 0, '', ''),
(658, '7419522208475', 'anticorrosivo negro', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-23 16:59:40', 340, 340, 6, 6, 1, 1, 0, '', ''),
(659, '7419522208413', 'anticorrosivo blanco', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-23 17:07:28', 340, 340, 7, 7, 1, 1, 0, '', ''),
(660, '7419522208550', 'anticorrosivo gris', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-24 08:55:40', 340, 340, 12, 12, 1, 1, 0, '', ''),
(661, '7419522229036', 'sellador multifuncional o base 4000 ', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-24 08:59:43', 340, 340, 16, 16, 1, 1, 0, '', ''),
(662, '718594022352', 'masilla vinililica', 'litro', 'Proveedor General', 'lanco', 1, '2016-12-24 09:04:23', 220, 200, 5, 5, 1, 1, 0, '', ''),
(663, '7419522308960', 'tinte miel sherwood', '757mm', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-24 09:14:09', 210, 210, 2, 2, 1, 1, 0, '', ''),
(664, '111', 'llanta para carretilla 10x2', 'unidad', 'Proveedor General', 'skk', 1, '2016-12-24 09:19:31', 350, 350, 0, 0, 1, 1, 0, '', ''),
(665, '111111', 'llanta para carretilla grande\r\n\r\n', 'unidad', 'Proveedor General', 'skk', 1, '2016-12-24 09:22:30', 710, 710, 0, 0, 1, 1, 0, '', ''),
(666, 'c11111c', 'cuerda de arranque blanca ', 'yarda', 'Proveedor General', 'n/a', 1, '2016-12-24 09:25:12', 15, 15, 0, 0, 1, 1, 0, '', ''),
(667, 'c22222', ' cuerda arranque delgada blnca', 'yarda', 'Proveedor General', 'n/a', 1, '2016-12-24 09:28:13', 12, 12, 0, 0, 1, 1, 0, '', ''),
(668, 'c111', 'cuerda arranque verde', 'yarda', 'Proveedor General', 'N/A', 1, '2016-12-24 09:29:57', 12, 12, 0, 0, 1, 1, 0, '', ''),
(669, '2222', 'tubo para lampara led grande ', 'unidad', 'Proveedor General', 'N/A', 1, '2016-12-24 09:31:23', 850, 850, 0, 0, 1, 1, 0, '', ''),
(670, '12123', 'tubo de cobre para refrigerador 5/16', 'pie', 'Proveedor General', 'N/A', 1, '2016-12-24 09:34:04', 12, 12, 0, 0, 1, 1, 0, '', ''),
(671, '1213|', ' tubo de cobre para refrigerador 3/8', 'pie', 'Proveedor General', 'N/A', 1, '2016-12-24 09:36:09', 18, 18, 0, 0, 1, 1, 0, '', ''),
(672, '1414', 'lampara grande 39x10.5x39 led', 'unidad', 'Proveedor General', 'energy saving', 1, '2016-12-24 09:49:29', 600, 600, 0, 0, 1, 1, 0, '', ''),
(673, '1312', 'lampara led 14 pulgadas ', 'unidad ', 'Proveedor General', 'american electric', 1, '2016-12-24 09:52:24', 655, 655, 0, 0, 1, 1, 0, '', ''),
(674, '7441105609119', 'barnis tinte nogal claro', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 10:05:29', 260, 260, 6, 6, 1, 1, 0, '', ''),
(675, '7441105608631', 'barnis marino de poliuterano', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 10:12:40', 230, 230, 19, 19, 1, 1, 0, '', ''),
(676, '7441105608778', 'barnis tramparente koral', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 10:22:12', 160, 160, 5, 5, 1, 1, 0, '', ''),
(677, '769409149764', 'barnis tranparente poliuterano', 'ltro', 'Proveedor General', 'modelo', 1, '2016-12-24 10:37:41', 220, 220, 5, 5, 1, 1, 0, '', ''),
(678, '769409090035', 'barnis castaÃ±o alquidico', 'ltro', 'Proveedor General', 'corona', 1, '2016-12-24 10:39:47', 200, 200, 4, 4, 1, 1, 0, '', ''),
(679, '7431003745316', 'termite of', 'ltro', 'Proveedor General', 'futex', 1, '2016-12-24 10:42:31', 130, 130, 4, 4, 1, 1, 0, '', ''),
(680, '12122', 'maderol tramparente ', 'ltro', 'Proveedor General', 'inverciones rodrigues', 1, '2016-12-24 10:45:05', 130, 130, 0, 0, 1, 1, 0, '', ''),
(681, '12142', ' maderol tramparente', '1/8', 'Proveedor General', 'idust rr', 1, '2016-12-24 10:46:23', 90, 90, 0, 0, 1, 1, 0, '', ''),
(682, '1215', 'maderol verde ', 'ltro', 'Proveedor General', 'indt rr', 1, '2016-12-24 10:47:39', 85, 85, 0, 0, 1, 1, 0, '', ''),
(683, '7431003745309', 'termiterol', 'Galon', 'Proveedor General', 'futex', 1, '2016-12-24 10:49:33', 400, 400, 2, 2, 1, 1, 0, '', ''),
(684, '7441105681344', 'antigrava para carro', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 10:50:59', 365, 365, 2, 2, 1, 1, 0, '', ''),
(685, '7441105633077', 'super removedor diablo ', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 10:52:46', 245, 245, 5, 5, 1, 1, 0, '', ''),
(686, '7441105646947', 'laca industrial brillante concentrada', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 10:55:36', 215, 215, 6, 6, 1, 1, 0, '', ''),
(687, '7419522251099', 'arce colonial tinte', 'ltro', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-24 10:56:57', 280, 280, 4, 4, 1, 1, 0, '', ''),
(688, '7419522214018', 'barnis copal brillante', 'ltro', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-24 11:00:18', 280, 280, 6, 6, 1, 1, 0, '', ''),
(689, '7419522250023', 'tinte nogal danes', 'ltro', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-24 11:01:55', 280, 280, 0, 0, 1, 1, 0, '', ''),
(690, '718594007953', 'laca consentrada para madera', 'LTRO', 'Proveedor General', 'lanco', 1, '2016-12-24 11:03:10', 200, 200, 1, 1, 1, 1, 0, '', ''),
(691, '718594340555', 'barnis tinte roble oscuro', 'ltro', 'Proveedor General', 'lanco', 1, '2016-12-24 11:04:22', 200, 200, 4, 4, 1, 1, 0, '', ''),
(692, '7419522249218', 'sellador super consentrado', 'ltro', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-24 11:05:24', 240, 240, 9, 9, 1, 1, 0, '', ''),
(693, '12145', 'barnis para madera miel', 'ltro', 'Proveedor General', 'pectrum', 1, '2016-12-24 11:08:07', 210, 210, 0, 0, 1, 1, 0, '', ''),
(694, '7441105623603', 'tinte miel', '1/16', 'Proveedor General', 'sur', 1, '2016-12-24 11:09:09', 95, 95, 9, 9, 1, 1, 0, '', ''),
(695, '7441105623528', 'tinte rojo manzana', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 11:14:17', 320, 320, 1, 1, 1, 1, 0, '', ''),
(696, '7441105623894', 'tinte nogal claro', 'ltro', 'Proveedor General', 'sur', 1, '2016-12-24 11:17:02', 285, 285, 5, 5, 1, 1, 0, '', ''),
(697, '718594066264', 'tinte walnut', '473ml', 'Proveedor General', 'lanco', 1, '2016-12-24 11:20:33', 138, 138, 8, 8, 1, 1, 0, '', ''),
(698, '7433200040034', ' pega cemento de contacto ', 'ltro', 'Proveedor General', 'klebe', 1, '2016-12-24 11:23:14', 150, 150, 3, 3, 1, 1, 0, '', ''),
(699, '718594071060', 'tinte para madera light brow', '473', 'Proveedor General', 'lanco', 1, '2016-12-24 11:25:09', 138, 138, 3, 3, 1, 1, 0, '', ''),
(700, '718594070667', 'tinte para madera Early american', '473', 'Proveedor General', 'lanco', 1, '2016-12-24 11:26:24', 138, 138, 4, 4, 1, 1, 0, '', ''),
(701, '718594070261', 'tinte para madera golden brown', '473ml', 'Proveedor General', 'lanco', 1, '2016-12-24 11:27:44', 138, 138, 5, 5, 1, 1, 0, '', ''),
(702, '7501206615164', 'copa 19mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:29:21', 70, 70, 4, 4, 1, 1, 0, '', ''),
(703, '7501206614877', 'matraca o ratchet 1/4', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:31:15', 240, 240, 2, 2, 1, 1, 0, '', ''),
(704, '7501206648728', 'copa #11mmx1/2', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:33:06', 60, 60, 2, 2, 1, 1, 0, '', ''),
(705, '7501206647417', 'copa # 5/8x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:34:15', 36, 36, 3, 3, 1, 1, 0, '', ''),
(706, '7501206615645', 'copa #27X1/2MM', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-24 11:40:02', 80, 80, 4, 4, 1, 1, 0, '', ''),
(707, '7501206648667', 'copa#20x1/2mm', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-24 11:41:35', 58, 58, 4, 4, 1, 1, 0, '', ''),
(708, '7501206647363', 'copa#19x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:45:16', 40, 40, 3, 3, 1, 1, 0, '', ''),
(709, '7501206615508', 'copa#13x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:46:58', 38, 38, 6, 6, 1, 1, 0, '', ''),
(710, '7501206647295', 'copa#12x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:48:26', 35, 35, 3, 3, 1, 1, 0, '', ''),
(711, '7501206647288', 'copa#11x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:50:08', 38, 38, 6, 6, 1, 1, 0, '', ''),
(712, '7501206647271', 'copa#10x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:51:25', 34, 34, 4, 4, 1, 1, 0, '', ''),
(713, '7501206647394', 'copa#1/2x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:52:44', 35, 35, 2, 2, 1, 1, 0, '', ''),
(714, '7501206615522', 'copa#15x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:54:45', 35, 35, 4, 4, 1, 1, 0, '', ''),
(715, '7501206647332', 'copa#16x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:55:36', 40, 40, 3, 3, 1, 1, 0, '', ''),
(716, '7501206647356', 'copa#18x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 11:57:07', 68, 68, 4, 4, 1, 1, 0, '', ''),
(717, '7501206648674', 'copa#21x1/2mm', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-24 11:58:20', 52, 52, 5, 5, 1, 1, 0, '', ''),
(718, '7501206648681', 'copa#22x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:00:15', 50, 50, 1, 1, 1, 1, 0, '', ''),
(719, '7501206648698', 'copa#23x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:02:06', 55, 55, 3, 3, 1, 1, 0, '', ''),
(720, '7501206648704', 'copa#24x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:03:25', 55, 55, 3, 3, 1, 1, 0, '', ''),
(721, '7501206615683', 'copa#32x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:05:08', 90, 90, 1, 1, 1, 1, 0, '', ''),
(722, '7501206648711', 'copa#25x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:06:24', 60, 60, 4, 4, 1, 1, 0, '', ''),
(723, '7501206615058', 'copa#8x1/2', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:12:04', 55, 55, 3, 3, 1, 1, 0, '', ''),
(724, '7501206615652', 'copa#28x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:13:23', 85, 85, 3, 3, 1, 1, 0, '', ''),
(725, '7501206615669', 'copa#29x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:14:48', 85, 85, 3, 3, 1, 1, 0, '', ''),
(726, '7501206615676', 'copa#30x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:16:15', 80, 80, 2, 2, 1, 1, 0, '', ''),
(727, '7501206615089', 'copa#14x1/2mm', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:18:06', 60, 60, 1, 1, 1, 1, 0, '', ''),
(728, '7501206615096', 'copa#17x1/2mm extra larga', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:20:21', 80, 80, 5, 5, 1, 1, 0, '', ''),
(729, '7501206648735', 'copa#12x1/2mm extra larga', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-24 12:21:48', 65, 65, 6, 6, 1, 1, 0, '', ''),
(730, '7501206648759', 'copa#14x1/2mm extra larga', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:24:11', 68, 68, 6, 6, 1, 1, 0, '', ''),
(731, '7501206644423', 'copa#15x1/2mm extra larga', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:25:29', 63, 63, 3, 3, 1, 1, 0, '', ''),
(732, '7501206615133', 'copa#16x1/2mm extra larga', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:27:03', 68, 68, 6, 6, 1, 1, 0, '', ''),
(733, '7501206615140', 'copa#17x1/2mm extra larga', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:28:09', 70, 70, 5, 5, 1, 1, 0, '', ''),
(734, '7501206615157', 'copa#18x1/2mm extra larga', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:29:59', 70, 70, 6, 6, 1, 1, 0, '', ''),
(735, '7501206647554', 'adaptador 1/2x3/4mm', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-24 12:32:11', 135, 135, 5, 5, 1, 1, 0, '', ''),
(736, '7506240630104', 'extencion 1/2x8', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-24 12:36:10', 130, 130, 3, 3, 1, 1, 0, '', ''),
(737, '7501206645017', 'matraca o ratchet 1/2 ', 'unidad', 'Proveedor General', 'pretul', 1, '2016-12-24 12:37:52', 280, 280, 2, 2, 1, 1, 0, '', ''),
(738, '7501206616437', 'copa#11/2x3/4', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:39:24', 185, 185, 1, 1, 1, 1, 0, '', ''),
(739, '7501206647486', 'para bujia 5/8x1/2', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-24 12:41:16', 65, 65, 2, 2, 1, 1, 0, '', ''),
(740, '7501206647547', 'copa reducida3/4x1/2mm', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-24 12:45:14', 85, 85, 1, 1, 1, 1, 0, '', ''),
(741, '7501206619315', 'calibrador para neumaticos', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-26 08:07:49', 260, 260, 1, 1, 1, 1, 0, '', ''),
(742, '7453001115236', 'lintera led para cabeza', 'UNIDAD', 'Proveedor General', 'best value', 1, '2016-12-26 08:09:30', 100, 100, 3, 3, 1, 1, 0, '', ''),
(743, '7501206619865', 'cople rapido hembra tipo F', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 08:11:40', 100, 110, 4, 4, 1, 1, 0, '', ''),
(744, '7506240636502', 'balvula antiretorno para compresor de aire', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 08:13:20', 225, 225, 1, 1, 1, 1, 0, '', ''),
(745, '7501206648636', 'juego de 9 llaves allen tipo navaja', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 08:14:47', 80, 80, 4, 4, 1, 1, 0, '', ''),
(746, '7501206623800', 'iman extensible', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 08:17:12', 160, 160, 1, 1, 1, 1, 0, '', ''),
(747, '7501206629192', 'pinza para anillos de retencion', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 08:19:06', 210, 210, 2, 2, 1, 1, 0, '', ''),
(748, '7506240638759', 'linterna delantera para bicicleta', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 08:22:58', 350, 350, 1, 1, 1, 1, 0, '', ''),
(749, '7453001152507', 'juego de llaves exagonles', 'unidad', 'Proveedor General', 'best value', 1, '2016-12-26 08:26:07', 180, 180, 1, 1, 1, 1, 0, '', ''),
(750, '7506240641476', 'linterna tipo llavero ', 'unidad', 'Proveedor General', 'pretul', 1, '2016-12-26 08:36:36', 60, 60, 2, 2, 1, 1, 0, '', ''),
(751, '7501206673720', 'tuerca para grasera', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 08:39:12', 40, 40, 4, 4, 1, 1, 0, '', ''),
(752, '7501206602393', 'cople rapido universal hembra', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-26 08:41:01', 112, 112, 3, 3, 1, 1, 0, '', ''),
(753, '7501206602348', 'niple universal', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-26 08:42:12', 30, 30, 1, 1, 1, 1, 0, '', ''),
(754, '7501206680025', 'calibrador de hojas', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-26 09:22:05', 50, 50, 1, 1, 1, 1, 0, '', ''),
(755, '7501206680827', 'calibrador tipo rampa', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-26 09:23:15', 65, 65, 1, 1, 1, 1, 0, '', ''),
(756, '7501206619803', 'juego de coples y conectores rapido', 'unidad', 'Proveedor General', 'truper', 1, '2016-12-26 09:26:16', 245, 245, 3, 3, 1, 1, 0, '', ''),
(757, '7501206603789', 'juego de 5 piedras montadas', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-26 09:35:21', 70, 70, 2, 2, 1, 1, 0, '', ''),
(758, '7501206620168', 'balbula para inflar cabeza doble', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-26 09:38:52', 120, 120, 6, 6, 1, 1, 0, '', ''),
(759, '7501206653623', 'JUEGO DE PIDRAS MONTADAS ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-26 14:02:08', 150, 150, 3, 3, 1, 1, 0, '', ''),
(760, '7501206671115', 'HILO TIPO CRUCETA PARA DESBROSADORA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-26 14:05:33', 270, 270, 4, 4, 1, 1, 0, '', ''),
(761, '7453001157182', 'COPA DE 18MM BEST VALUE', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-26 14:11:21', 40, 30, 1, 1, 1, 1, 0, '', ''),
(763, '7419522281959', 'COLORAMICA LATEX COLOR MATE VERDE LAGUNA', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:20:41', 250, 250, 5, 5, 1, 1, 0, '', ''),
(764, '7419522281874', 'COLORAMICA LATEX COLOR MATE BEIGE', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:22:15', 250, 250, 9, 9, 1, 1, 0, '', ''),
(765, '7419522230964', 'ESMALTE COLOR brillante verde menta', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:23:41', 250, 250, 2, 2, 1, 1, 0, '', ''),
(766, '7419522282895', 'LATEX COLOR MATE CELESTE OCEANO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:30:50', 250, 250, 3, 3, 1, 1, 0, '', ''),
(767, '7419522230940', 'LATEX COLOR MATE SALMON', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:33:08', 250, 250, 1, 1, 1, 1, 0, '', ''),
(768, '7419522281805', 'LATEX COLOR MATE MANGO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:33:51', 250, 250, 3, 3, 1, 1, 0, '', ''),
(769, '7419522281126', 'LATEX MATE CELESTE', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:34:32', 250, 250, 4, 4, 1, 1, 0, '', ''),
(770, '7419522281997', 'LATEX COLOR MATE AZUL CLARO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:38:03', 250, 250, 4, 4, 1, 1, 0, '', ''),
(771, '7419522281171', 'LATEX COLOR MATE MARFIL', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:41:30', 250, 250, 8, 8, 1, 1, 0, '', ''),
(772, '7419522281935', 'LATEX COLOR MATE VERDE JARDIN', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:44:23', 250, 250, 6, 6, 1, 1, 0, '', ''),
(773, '7419522282772', 'LATEX COLOR MATE BVLANCO ANTIGUO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:46:02', 250, 250, 8, 8, 1, 1, 0, '', ''),
(774, '7419522281898', 'LATEX COLOR MATE VERDE TROPICANA', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:46:49', 250, 250, 3, 3, 1, 1, 0, '', ''),
(775, '7419522281225', 'LATEX COLOR MATE ROJO LADRILLO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:55:01', 250, 250, 1, 1, 1, 1, 0, '', ''),
(776, '7419522226424', 'ESMALTE BRILLANTE CHOCOLATE', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 14:59:43', 440, 440, 10, 10, 1, 1, 0, '', ''),
(777, '7419522226219', 'ESMALTE PENINSULAR COLOR VINO TINTO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:02:03', 440, 440, 1, 1, 1, 1, 0, '', ''),
(778, '7419522214407', 'ESMALTE PENINSULAR COLOR CAFE COBRIZO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:04:39', 440, 440, 1, 1, 1, 1, 0, '', ''),
(779, '7419522226547', 'ESMALTE BRILLANTE VERDE AQUA', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:05:47', 410, 410, 4, 4, 1, 1, 0, '', ''),
(780, '7419522227308', 'ESMALTE BRILLANTE VERDE LIMA', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:06:43', 410, 410, 2, 2, 1, 1, 0, '', ''),
(781, '7419522227605', 'ESMALTE BRILLANTE MARFIL CREMA', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:11:04', 410, 410, 2, 2, 1, 1, 0, '', ''),
(782, '7419522227322', 'ESMALTE BRILLANTE AMARILLO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:16:55', 385, 385, 4, 4, 1, 1, 0, '', ''),
(783, '7419522226578', 'ESMALTE BRILLANTE CELESTE', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:18:08', 385, 385, 3, 3, 1, 1, 0, '', ''),
(784, '7419522224697', 'ESMALTE BRILLANTE NARANJA MANDARINA', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:21:09', 410, 410, 1, 1, 1, 1, 0, '', ''),
(785, '7419522206853', 'ESMALTE COLONIAL STYLE BASE ULTRA DEEP', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:24:59', 420, 420, 1, 1, 1, 1, 0, '', ''),
(786, '7419522227261', 'ESMALTE BRILLANTE ROJO PASION', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:28:41', 410, 410, 1, 1, 1, 1, 0, '', ''),
(787, '7419522226660', 'ESMALTE BRILLANTE GRIS OSCURO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:29:37', 410, 410, 1, 1, 1, 1, 0, '', ''),
(788, '7419522227544', 'ESMALTE BRILLANTE GRIS OSCURO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:34:05', 410, 410, 1, 1, 1, 1, 0, '', ''),
(789, '7419522226509', 'ESMALTE BRILLANTE BLANCO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:36:23', 410, 410, 1, 1, 1, 1, 0, '', ''),
(790, '7419522227629', 'ESMALTE BRILLANTE AMARRILLO TRIGO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:37:28', 410, 410, 1, 1, 1, 1, 0, '', ''),
(791, '7419522225021', 'LATEX PENINSULAR VERDE MENTA', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:38:31', 360, 360, 7, 7, 1, 1, 0, '', ''),
(792, '7419522227346', 'ESMALTE BRILLANTE MARFIL SUAVE', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:39:31', 410, 410, 4, 4, 1, 1, 0, '', ''),
(793, '7419522233361', 'LATEX PENINSULAR NARANJA CARNAVAL', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:40:54', 360, 360, 2, 2, 1, 1, 0, '', ''),
(794, '7419522226462', 'ESMALTE BRILLANTE NEGRO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:46:41', 410, 410, 1, 1, 1, 1, 0, '', ''),
(795, '7419522226486', 'ESMALTE BRILLANTE BLANCO HUESO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:47:40', 410, 410, 2, 2, 1, 1, 0, '', ''),
(796, '7419522225090', 'LATEX PENINSULAR COLOR ROJO TEJA', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:53:28', 360, 360, 3, 3, 1, 1, 0, '', ''),
(797, '7419522214384', 'LATEX PENINSULAR COLOR CAFE COBRIZO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:56:05', 360, 360, 4, 4, 1, 1, 0, '', ''),
(798, '7419522225038', 'LATEX PENINSULAR COLOR AQUA', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:56:53', 360, 360, 4, 4, 1, 1, 0, '', ''),
(799, '7419522224543', 'LATEX PENINSULAR COLOR AZUL INTENSO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:57:48', 360, 360, 2, 2, 1, 1, 0, '', ''),
(800, '7419522229197', 'LATEX PENINSULAR COLOR ROJO TERRACOTA', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 15:58:41', 360, 360, 4, 4, 1, 1, 0, '', ''),
(801, '7419522233385', 'LATEX PENINSULAR COLOR MELON', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 16:02:47', 360, 360, 4, 4, 1, 1, 0, '', ''),
(802, '7419522222860', 'LATEX PENINSULAR COLOR VERDE COCKTAIL', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 16:03:35', 360, 360, 1, 1, 1, 1, 0, '', ''),
(803, '7419522225113', 'LATEX PENINSULAR COLOR NUEVO BLANCO HUESO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 16:04:34', 360, 360, 4, 4, 1, 1, 0, '', ''),
(804, '7419522225045', 'LATEX PENINSULAR COLOR MARFIL CREMA', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 16:05:52', 360, 360, 5, 5, 1, 1, 0, '', ''),
(805, '7419522224512', 'LATEX PENINSULAR COLOR VERDE INTENSO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 16:11:32', 360, 360, 5, 5, 1, 1, 0, '', ''),
(806, '7419522225014', 'LATEX PENINSULAR COLOR NARANJA CLARO', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-26 16:13:49', 360, 360, 5, 5, 1, 1, 0, '', ''),
(807, '7453001162223', 'MANGUERA PARA SOLDAR 15 PIES', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-26 16:21:31', 520, 520, 1, 1, 1, 1, 0, '', ''),
(808, '070963169274', 'MANGUERA REFORZADA PARA JARDIN 75 PIES', 'UNIDAD', 'Proveedor General', 'BOXER TOOLS', 1, '2016-12-26 16:23:39', 580, 580, 1, 1, 1, 1, 0, '', ''),
(809, '7501206653371', 'MANGUERA REFORZADA PARA JARDIN 15 MTS', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-26 16:25:12', 380, 380, 2, 2, 1, 1, 0, '', ''),
(810, '070963169250', 'MANGUERA REFORZADA PARA JARDIN 50 PIES', 'UNIDAD', 'Proveedor General', 'BOXER TOOLS', 1, '2016-12-26 16:27:14', 450, 450, 3, 3, 1, 1, 0, '', ''),
(811, '7501206602294', 'MABNGUERA DE HULE PARA COMPRESOR 15 MTR', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-26 16:28:14', 780, 780, 1, 1, 1, 1, 0, '', ''),
(812, '7441035322546', 'madetec tinte miel', 'ltro', 'Proveedor General', 'protecto', 1, '2016-12-27 07:39:14', 320, 320, 0, 2, 1, 1, 0, '', ''),
(813, '7441035324168', 'madetec tinte cristobal', 'ltro', 'Proveedor General', 'PROTECTO', 1, '2016-12-27 07:52:03', 320, 320, 0, 2, 1, 1, 0, '', ''),
(814, '54940590', 'super acril 100-06', 'unidad', 'Proveedor General', 'industria zurqui', 1, '2016-12-27 09:18:50', 200, 175, 4, 4, 1, 1, 0, '', ''),
(815, '7441105609652', 'imperfast tapagoteras', 'litro', 'Proveedor General', 'sur', 1, '2016-12-27 09:32:07', 220, 185, 9, 9, 1, 1, 0, '', ''),
(816, '7501206653357', 'MANGUERA GARDEN HOUSE 15 MTR', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 10:02:18', 200, 200, 4, 4, 1, 1, 0, '', ''),
(817, '787884', 'jojojojjo', 'unidad', 'Proveedor General', 'TRUPER', 1, '2016-12-27 10:18:27', 20, 20, 10, 10, 1, 1, 0, '', ''),
(818, '818', 'kokoko', 'unidad', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 10:19:31', 50, 50, 22, 22, 1, 1, 0, '', ''),
(819, '7501206653364', 'MANGUERA PARA JARDIN REFORZADA 10 MTR', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 10:31:30', 320, 320, 1, 1, 1, 1, 0, '', ''),
(820, '7460737000552', 'MANGUERA PARA JARDIN ', 'UNIDAD', 'Proveedor General', 'FLEXICOM', 1, '2016-12-27 10:33:17', 185, 185, 1, 1, 1, 1, 0, '', ''),
(821, '7501206653340', 'MANGUERA GARDEN HOUSE 10 MTR', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 10:38:44', 150, 150, 4, 4, 1, 1, 0, '', ''),
(822, '7501206691892', 'MANGUERA GARDEN HOUSE 10 MTR', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 10:43:13', 150, 150, 1, 1, 1, 1, 0, '', ''),
(823, '7501206691939', 'MANGUERA GARDEN HOUSE 15 MTR', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 10:44:40', 200, 200, 2, 2, 1, 1, 0, '', ''),
(824, '5180002512383', 'MANGUERA GARDEN HOUSE 25 PIES', 'UNIDAD', 'Proveedor General', 'TALTOOLS', 1, '2016-12-27 10:46:35', 220, 220, 3, 3, 1, 1, 0, '', ''),
(825, '7419522307871', 'PRIMERA MANO PARA PAREDES ', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-27 11:55:59', 210, 210, 1, 1, 1, 1, 0, '', ''),
(826, '070963169229', 'MANGUERA REFORZADA PARA JARDIN 25 PIES', 'UNIDAD', 'Proveedor General', 'BOXER TOOLS', 1, '2016-12-27 11:57:23', 280, 280, 1, 1, 1, 1, 0, '', ''),
(827, '7501206660874', 'MANGUERA SUPER REFORZADA 20 MTR', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 11:58:53', 420, 420, 1, 1, 1, 1, 0, '', ''),
(828, '7891114051087', 'ESCOFINA REDONDA 10 PULGADAS', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-27 12:01:47', 120, 120, 2, 2, 1, 1, 0, '', ''),
(829, '7453001162865', 'CUCHILLA DE REPUESTO PARA CEPILLO', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 12:03:56', 90, 90, 5, 5, 1, 1, 0, '', ''),
(830, '7506240626497', 'PROBADOR DE TENSION Y POLARIDAD', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 12:06:32', 1, 1, 2, 2, 1, 1, 0, '', ''),
(831, '7501206603369', 'PINZA DE PRESION 4 "', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 12:08:08', 110, 110, 3, 3, 1, 1, 0, '', ''),
(832, '7591594770361', 'JUEGO DE BORNES', 'UNIDAD', 'Proveedor General', 'CONICA', 1, '2016-12-27 12:09:38', 165, 165, 2, 2, 1, 1, 0, '', ''),
(833, '7591594770378', 'PROTECTORES DE BORNES', 'UNIDAD', 'Proveedor General', 'CONICA', 1, '2016-12-27 12:10:28', 45, 45, 1, 1, 1, 1, 0, '', ''),
(834, '7453001161660', 'LIMA TRIANGULAR C/MANGO 6"', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 12:14:11', 65, 65, 2, 2, 1, 1, 0, '', ''),
(835, '835', 'TESTER 6-12 V', 'UNIDAD', 'Proveedor General', 'SCORPIO', 1, '2016-12-27 12:15:31', 60, 60, 1, 1, 1, 1, 0, '', ''),
(836, '7450077002736', 'CINTA DOBLE CONTACTO', 'UNIDAD', 'Proveedor General', 'BRICKKELL', 1, '2016-12-27 12:17:28', 20, 20, 1, 1, 1, 1, 0, '', ''),
(837, '008236800227', 'GANCHO PARA CUADROS', 'UNIDAD', 'Proveedor General', 'MEGAHOOK', 1, '2016-12-27 12:18:21', 90, 90, 2, 2, 1, 1, 0, '', ''),
(838, '838', 'CORREA DE PERRO', 'UNIDAD', 'Proveedor General', 'DOGLEAD', 1, '2016-12-27 12:21:24', 35, 35, 2, 2, 1, 1, 0, '', ''),
(839, '6987021109759', 'CANDADO PARA MOTO', 'UNIDAD', 'Proveedor General', 'DIESEL TOOLS', 1, '2016-12-27 12:21:50', 120, 120, 2, 2, 1, 1, 0, '', ''),
(840, '7453011797705', 'MIRILLA PARA PUERTA', 'UNIDAD', 'Proveedor General', 'HUNTER', 1, '2016-12-27 12:24:35', 80, 80, 3, 3, 1, 1, 0, '', ''),
(841, '7453001131236', 'MIRILLA PARA PUERTA ', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 12:25:46', 55, 55, 3, 3, 1, 1, 0, '', ''),
(842, '7419522203388', 'ANTICORROSIVO COLOR VERDE', 'GALON ', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-27 13:56:18', 340, 340, 3, 3, 1, 1, 0, '', ''),
(843, '7419522246156', 'ESMALTE INDUSTRIAL COLOR AMARILLO', 'GALON ', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-27 13:58:41', 760, 760, 1, 1, 1, 1, 0, '', ''),
(844, '7419522246132', 'ESMALTE INDUSTRIAL COLOR BLANCO', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-27 13:59:54', 760, 760, 1, 1, 1, 1, 0, '', ''),
(845, '7419522303880', 'ESMALTE INDUSTRIAL COLOR ROJO SEGURIDAD', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-27 14:00:53', 760, 760, 2, 2, 1, 1, 0, '', ''),
(846, '7419522303873', 'ESMALTE INDUSTRIAL AZUL SEGURIDAD', 'GALON', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-27 14:01:50', 760, 760, 4, 4, 1, 1, 0, '', ''),
(847, '847', 'SUPER ACRIL 1O-06', 'CUARTO ', 'Proveedor General', 'INDUSTRIAS ZURQUI', 1, '2016-12-27 14:09:13', 200, 200, 4, 4, 1, 1, 0, '', ''),
(848, '7501206649541', 'GATA HIDRAULICA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:12:07', 750, 750, 1, 1, 1, 1, 0, '', ''),
(849, '4897047126765', 'ARMEAS DE SEGURIDAD', 'UNIDAD', 'Proveedor General', 'FIXWORX', 1, '2016-12-27 14:16:40', 45, 45, 5, 5, 1, 1, 0, '', ''),
(850, '4897047126758', 'ARMEAS DE SEGURIDAD', 'UNIDAD', 'Proveedor General', 'FIXWORX', 1, '2016-12-27 14:17:36', 30, 30, 4, 4, 1, 1, 0, '', ''),
(851, '4897047126741', 'ARMEAS DE SEGURIDAD', 'UNIDAD', 'Proveedor General', 'FIXWORX', 1, '2016-12-27 14:18:19', 22, 225, 1, 1, 1, 1, 0, '', ''),
(852, '7501206695159', 'GARRUCHA PARA NORIA', 'UNIDAD', 'Proveedor General', 'FIERO', 1, '2016-12-27 14:19:05', 115, 115, 1, 1, 1, 1, 0, '', ''),
(853, '076174141252', 'CORTA VIDRIO ', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-27 14:20:06', 100, 100, 6, 6, 1, 1, 0, '', ''),
(854, '7453001159605', 'PROBADOR DE CORRIENTE', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 14:20:36', 32, 32, 6, 6, 1, 1, 0, '', ''),
(855, '7501206649213', 'TIJERA DE COSTURA', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 14:21:30', 120, 120, 4, 4, 1, 1, 0, '', ''),
(856, '7453001132967', 'GANCHO DOBLE BRONCEADO ', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 14:22:27', 78, 78, 1, 1, 1, 1, 0, '', ''),
(857, '7501206620625', 'PRENSA DE HIERRO', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:23:50', 240, 240, 1, 1, 1, 1, 0, '', ''),
(858, '7501206643327', 'TIJERA INDUSTRIAL', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:25:17', 370, 370, 1, 1, 1, 1, 0, '', ''),
(859, '7506240609414', 'BISAGRA BIDIMENCIONALNES', 'UNIDAD', 'Proveedor General', 'HERMEX', 1, '2016-12-27 14:26:43', 35, 35, 8, 8, 1, 1, 0, '', ''),
(860, '7506240609421', 'BISAGRA BIDIMENCIONAL ', 'UNIDAD ', 'Proveedor General', 'HERMEX', 1, '2016-12-27 14:27:28', 40, 40, 4, 4, 1, 1, 0, '', ''),
(861, '7453001130147', 'BISAGRAS ESCONDIDAS ', 'UNIDAD ', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 14:29:30', 55, 55, 3, 3, 1, 1, 0, '', ''),
(862, '7453001130154', 'ANDABA CON SERRADURA ', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 14:30:54', 140, 140, 6, 6, 1, 1, 0, '', ''),
(863, '7501206631157', 'ESPATULA TAPIZADORA', 'UNIDAD ', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:31:46', 110, 110, 3, 3, 1, 1, 0, '', ''),
(864, '7501206631164', 'ESPATULA TAPIZADORA ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:32:33', 120, 120, 3, 3, 1, 1, 0, '', ''),
(1433, '7501892821351', 'PASADOR CON CADENA', 'UNIDAD', 'Proveedor General', 'TOOLCRAFT', 1, '2016-12-31 09:46:04', 200, 200, 5, 5, 1, 1, 0, '', ''),
(866, '7501206671214', 'ENGRAPADORA TIPO PISTOLA ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:34:11', 620, 620, 2, 2, 1, 1, 0, '', ''),
(867, '8022884523538', 'LLAVE TIPO L', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-27 14:36:11', 230, 230, 2, 2, 1, 1, 0, '', ''),
(868, 'B-5480', 'MANERAL DE MANO', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:40:00', 190, 190, 1, 1, 1, 1, 0, '', ''),
(869, '869', 'EXTENCION DE MEDIA ', 'UNIDAD ', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:47:11', 65, 65, 1, 1, 1, 1, 0, '', ''),
(870, '870', 'MANERAL DE 3/4', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:47:49', 435, 435, 1, 1, 1, 1, 0, '', ''),
(871, 'E-5262', 'EXTENCION DE 3/8', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:49:00', 75, 75, 6, 6, 1, 1, 0, '', ''),
(872, '872', 'EXTENCION DE 3/4', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:49:42', 260, 260, 1, 1, 1, 1, 0, '', ''),
(873, '7501206647523', 'ADAPTADOR 3/8 A MEDIA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:54:39', 45, 45, 1, 1, 1, 1, 0, '', ''),
(874, '7501206615782', 'ADAPTADOR DE T-60 A 3/8', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 14:56:00', 55, 55, 1, 1, 1, 1, 0, '', ''),
(875, '7501206680902', 'GANCHO PARA CUERDA ', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-27 15:11:05', 130, 130, 2, 2, 1, 1, 0, '', ''),
(876, '7501206676448', 'TOPE MAGNETICO', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-27 15:12:01', 65, 65, 2, 2, 1, 1, 0, '', ''),
(877, '7501206680919', 'SERRADURA PARA VITRINA ', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-27 15:12:46', 110, 110, 1, 1, 1, 1, 0, '', ''),
(878, '7501206653975', 'JUEGO DE 5 PIEDRA MONTADO ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:16:58', 70, 70, 2, 2, 1, 1, 0, '', ''),
(879, '7501206671153', 'HILO TIPO CRUZETA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:18:52', 52, 52, 3, 3, 1, 1, 0, '', ''),
(880, '7501206671146', 'HILO TIPO CRUZETA ', 'UNIDAD ', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:19:45', 55, 55, 4, 4, 1, 1, 0, '', ''),
(881, '7501206619322', 'CALIBRADOR PARA NEUMATICO ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:21:24', 265, 265, 3, 3, 1, 1, 0, '', ''),
(882, '9328675010621', 'LLAVE CRECE DE 150 MM', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-27 15:25:44', 215, 215, 2, 2, 1, 1, 0, '', ''),
(1430, '7501206668788', 'BISAGRA DE DOBLE ACCION', 'UNIDAD', 'Proveedor General', 'GERMEX', 1, '2016-12-31 09:35:27', 310, 310, 6, 6, 1, 1, 0, '', ''),
(884, '7501206620465', 'FORMON DE 1 PULG', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:35:55', 100, 100, 4, 4, 1, 1, 0, '', ''),
(885, '7501206648568', 'MINI PINZA  DE PUNTA REDONDA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:36:44', 140, 140, 4, 4, 1, 1, 0, '', ''),
(886, '7501206621653', 'SARGENTO PARA TUBO ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:37:30', 360, 360, 1, 1, 1, 1, 0, '', ''),
(887, '7453001151760', 'CUCHILLA PARA CORTAR AZULEJO', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 15:38:12', 65, 65, 4, 4, 1, 1, 0, '', ''),
(888, '888', 'PISTOLA PARA SOLDAR ', 'UNIDAD', 'Proveedor General', 'QUICK HEAT-UP', 1, '2016-12-27 15:39:22', 410, 410, 1, 1, 1, 1, 0, '', ''),
(889, '889', 'BALANZA', 'UNIDAD', 'Proveedor General', 'POCKET', 1, '2016-12-27 15:40:04', 65, 65, 1, 1, 1, 1, 0, '', ''),
(890, '7501206620410', 'FORMON DE 3/8', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:40:56', 98, 98, 1, 1, 1, 1, 0, '', ''),
(891, '9328675017217', 'ESCUADRA PARA CARPINTERIA', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-27 15:42:20', 180, 180, 2, 2, 1, 1, 0, '', ''),
(892, '750120662041098', 'PUNZON PARA MARCAR DE MEDIA', 'UNIDAD ', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:44:49', 65, 65, 2, 2, 1, 1, 0, '', ''),
(893, '7501206680681', 'PUNZON PARA MARCAR DE 5/8', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:45:47', 100, 100, 2, 2, 1, 1, 0, '', ''),
(894, '7453001154464', 'PINZA PARA ANILLO DE RETENCION', 'UNIDADS', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 15:47:22', 175, 175, 1, 1, 1, 1, 0, '', ''),
(895, '7453078503738', 'TIJERA PARA CORTAR ZINC', 'UNIDAD', 'Proveedor General', 'SECURITY', 1, '2016-12-27 15:48:12', 140, 140, 1, 1, 1, 1, 0, '', ''),
(896, '7417000500370', 'TIJERA PARA PODAR DE PASO', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 15:49:00', 230, 230, 1, 1, 1, 1, 0, '', ''),
(897, '7501206634912', 'CUCHILLA DE REPUESTO PARA CORTADOR DE AZULEJO', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 15:50:46', 65, 65, 1, 1, 1, 1, 0, '', ''),
(898, '7501206679357', 'PLOMADA DE LATON', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:51:20', 200, 200, 2, 2, 1, 1, 0, '', ''),
(899, '7501206665541', 'CAUTIN TIPO LAPIZ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:52:10', 160, 160, 1, 1, 1, 1, 0, '', ''),
(900, '2066402341750', 'PINZA DE PRECION', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:52:57', 220, 220, 1, 1, 1, 1, 0, '', ''),
(901, '7501206626542', 'PINZA DE PRESION ', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 15:53:57', 130, 130, 1, 1, 1, 1, 0, '', ''),
(902, '7501206626115', 'TIJERA DE AVIACION ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 15:54:59', 200, 200, 5, 5, 1, 1, 0, '', ''),
(903, '6940350887996', 'PIZA PARA CARPINTERO', 'UNIDAD', 'Proveedor General', 'SNAUZER', 1, '2016-12-27 15:56:15', 1, 1, 1, 1, 1, 1, 0, '', ''),
(904, '7453001151463', 'PINZA PARA CORTAR CERAMICA', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 15:56:48', 195, 195, 2, 2, 1, 1, 0, '', ''),
(905, '7417000527230', 'LLAVE AJUSTABLE CRECEN DE 8"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 15:57:36', 175, 175, 1, 1, 1, 1, 0, '', ''),
(906, '7501206645055', 'LLAVE AJUSTABLE CRECEN ', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 15:58:24', 145, 145, 1, 1, 1, 1, 0, '', ''),
(907, '7417000527247', 'LLAVE AJUSTABLE CRECEN 10"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 15:59:06', 245, 245, 2, 2, 1, 1, 0, '', ''),
(908, '7417000540772', 'TENAZA DE ELECTRICISTA 9"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 15:59:47', 255, 255, 2, 2, 1, 1, 0, '', ''),
(909, '7417000500929', 'TENAZA DE ELECTRICISTA DE 7"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 16:00:14', 180, 180, 2, 2, 1, 1, 0, '', ''),
(910, '7501206648575', 'PINZA DE ACENDADO 11"', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:00:56', 380, 380, 3, 3, 1, 1, 0, '', ''),
(911, '7417000526073', 'TENAZA FINQUERA ', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 16:01:33', 290, 290, 1, 1, 1, 1, 0, '', ''),
(912, '6906418451240', 'NIVEL DE 1220MM', 'UNIDAD', 'Proveedor General', 'PANYI', 1, '2016-12-27 16:02:42', 320, 320, 1, 1, 1, 1, 0, '', ''),
(913, '9328675012694', 'TENAZA DE 270MM', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-27 16:04:00', 180, 180, 1, 1, 1, 1, 0, '', ''),
(914, '7501206644829', 'PINZA DE ELECTRICSTA DE 9"', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 16:04:38', 220, 220, 1, 1, 1, 1, 0, '', ''),
(915, '7501206643167', 'PINZA DE CHOFER DE 8"', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 16:05:05', 75, 75, 1, 1, 1, 1, 0, '', ''),
(916, '6920150016376', 'HARNES PARA PERRO', 'UNIDAD', 'Proveedor General', 'MAXCOTAS', 1, '2016-12-27 16:05:40', 330, 330, 2, 2, 1, 1, 0, '', ''),
(917, '7501206643211', 'REMACHADORA DE 254MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-27 16:10:28', 140, 140, 2, 2, 1, 1, 0, '', ''),
(918, '7417000535570', 'TENAZA CORTE DIAGONAL', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 16:11:58', 170, 170, 8, 8, 1, 1, 0, '', ''),
(919, '7453050031198', 'PINZA PARA ZAPATERO ', 'UNIDAD', 'Proveedor General', 'PITBULL', 1, '2016-12-27 16:20:58', 75, 75, 2, 2, 1, 1, 0, '', ''),
(920, '7501206630167', 'ESPATULA FLEXIBLE PRO 4 "', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:21:54', 80, 80, 4, 4, 1, 1, 0, '', ''),
(921, '921', 'CRIMPING TOOLS', 'UNIDAD', 'Proveedor General', 'ELECTRICIANS TOOLS', 1, '2016-12-27 16:23:45', 485, 485, 2, 2, 1, 1, 0, '', ''),
(922, '070983709061', 'TRISCADOR DE SERRUCHO ', 'UNIDAD', 'Proveedor General', 'BOXER TOOLS', 1, '2016-12-27 16:24:32', 250, 250, 1, 1, 1, 1, 0, '', ''),
(923, '7501206623756', 'TRISCADOR DE SERRUCHO', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:25:10', 340, 340, 2, 2, 1, 1, 0, '', ''),
(924, '7501206619889', 'CONECTOR RAPIDO TIPO F', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:25:53', 35, 35, 4, 4, 1, 1, 0, '', ''),
(925, '7501206602232', 'JUEGO DE REFACCION DE PISTOLA PARA PNTAR', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:27:25', 160, 160, 3, 3, 1, 1, 0, '', ''),
(926, '7501206630075', 'SINTA DE 50 MTS', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:29:25', 370, 370, 2, 2, 1, 1, 0, '', ''),
(927, '7501206630068', 'SINTA DE 30 MTS', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:29:57', 300, 300, 2, 2, 1, 1, 0, '', ''),
(928, '8022820134514', 'ALICATE DE PRESION PICO DE PATO', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-27 16:31:02', 225, 225, 2, 2, 1, 1, 0, '', ''),
(929, '7453001127529', 'TOPE PARA PUERTA DE ALARMA', 'UNIDAD', 'Proveedor General', 'ROCKWELL', 1, '2016-12-27 16:32:33', 130, 130, 3, 3, 1, 1, 0, '', ''),
(930, '7501206620861', 'ESCOCHEBRE CARA CURBA ', 'UNIDAD ', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:33:38', 212, 212, 3, 3, 1, 1, 0, '', ''),
(931, '084298031106', 'TIRANTES DE TRABAJO', 'UNIDAD', 'Proveedor General', 'CLC', 1, '2016-12-27 16:36:32', 200, 200, 1, 1, 1, 1, 0, '', ''),
(932, '7501206623664', 'DESARMADOR DE ESTRELLA 6"', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:37:31', 65, 65, 1, 1, 1, 1, 0, '', ''),
(933, '7501206623671', 'DESARMADOR DE ESTRELLA 4"', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:38:13', 75, 75, 1, 1, 1, 1, 0, '', ''),
(934, '7450077111872', 'TIJERA DE JARDINERIA ', 'UNIDAD', 'Proveedor General', 'BRICKELL', 1, '2016-12-27 16:39:16', 150, 150, 1, 1, 1, 1, 0, '', ''),
(935, '7453042847394', 'TIJERA PARA CORTAR ZINC ', 'UNIDAD', 'Proveedor General', 'HUNTER', 1, '2016-12-27 16:40:04', 200, 200, 3, 3, 1, 1, 0, '', ''),
(936, '8022820134446', 'ALICATE DE PRESION PUNTA GIRATORIA', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-27 16:40:43', 230, 230, 1, 1, 1, 1, 0, '', ''),
(937, '075339027578', 'SIERRA DE CALAR', 'UNIDAD', 'Proveedor General', 'RED DEVIL', 1, '2016-12-27 16:41:11', 170, 170, 3, 3, 1, 1, 0, '', ''),
(938, '7417000509076', 'ASPERSOR METALICO CON ESTACA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 16:41:54', 290, 290, 4, 4, 1, 1, 0, '', ''),
(939, '7453011773389', 'RETENEDOR DE SEGURIDAD DE PUERTA', 'UNIDAD', 'Proveedor General', 'HUNTER', 1, '2016-12-27 16:42:54', 95, 95, 1, 1, 1, 1, 0, '', ''),
(940, '7453001132301', 'GANCHO PARA AMACA', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 16:43:22', 50, 50, 1, 1, 1, 1, 0, '', ''),
(941, '084298008399', 'PORTA MARTILLO ', 'UNIDAD', 'Proveedor General', 'CLC', 1, '2016-12-27 16:43:59', 1, 1, 1, 1, 1, 1, 0, '', ''),
(942, '7501206634967', 'SUJETADOR CON MATRACA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:45:38', 750, 750, 1, 1, 1, 1, 0, '', ''),
(943, '7501206650813', 'RECOLECTOR DE FRUTAS', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:46:11', 1, 1, 2, 2, 1, 1, 0, '', ''),
(944, '7417000500400', 'HERRAMIENTA PARA JARDINERIA ', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 16:47:14', 185, 185, 1, 1, 1, 1, 0, '', ''),
(945, '945', 'PISTOLA DE AGUA GARDEN', 'UNIDAD', 'Proveedor General', 'GARDEN COLLECTION', 1, '2016-12-27 16:48:31', 75, 75, 1, 1, 1, 1, 0, '', ''),
(946, '752148600009', 'CHISPERO', 'UNIDAD', 'Proveedor General', 'RED FORCE', 1, '2016-12-27 16:49:13', 90, 90, 2, 2, 1, 1, 0, '', ''),
(947, '7417000523294', 'FORMON DE 3/8', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-27 16:50:25', 115, 115, 1, 1, 1, 1, 0, '', ''),
(948, '7450077007625', 'CANDADO PARA BICICLETA', 'UNIDAD', 'Proveedor General', 'SCAN BRIK', 1, '2016-12-27 16:51:04', 70, 70, 1, 1, 1, 1, 0, '', ''),
(949, '7506240600305', 'FILTRO PARA COMPRESOR DE AIRE', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:52:58', 155, 155, 1, 1, 1, 1, 0, '', '');
INSERT INTO `products` (`id_producto`, `codigo_producto`, `nombre_producto`, `unidad_med`, `proveedor`, `marca`, `status_producto`, `date_added`, `precio_producto`, `precio_compra`, `stock`, `cantidad_total`, `ex_min`, `exento`, `salida`, `unidad_m`, `marca_p`) VALUES
(950, '7453001139386', 'PORTA MARTILLO', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 16:53:34', 95, 95, 3, 3, 1, 1, 0, '', ''),
(951, '0793573968289', 'GUANTES DE SEGURIDAD', 'UNIDAD', 'Proveedor General', 'BESTLY', 1, '2016-12-27 16:55:02', 25, 25, 14, 14, 1, 1, 0, '', ''),
(952, '8022882106139', 'MALETIN CON HERRAMIENTAS 12 PIZAS', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-27 16:55:48', 550, 550, 1, 1, 1, 1, 0, '', ''),
(953, '7453038421256', 'GUANTES DE SEGURIDAD REFLECTIVOS', 'UNIDAD', 'Proveedor General', 'COVO', 1, '2016-12-27 16:56:30', 120, 120, 1, 1, 1, 1, 0, '', ''),
(954, '7453010091026', 'GUANTES PARA TRABAJO ', 'UNIDAD', 'Proveedor General', 'SECURITY', 1, '2016-12-27 16:57:19', 170, 170, 30, 30, 1, 1, 0, '', ''),
(955, '7501206683101', 'GUANTES PARA LIMPIEZA DOMESTICA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 16:58:30', 45, 45, 2, 2, 1, 1, 0, '', ''),
(956, '7501206693155', 'GUANTES TEJIDOS Y RECUBIERTOS DE BITRILO', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 17:01:04', 180, 180, 4, 4, 1, 1, 0, '', ''),
(957, '7453001105732', 'GUANTE ANTI CORTE', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-27 17:01:35', 238, 238, 6, 6, 1, 1, 0, '', ''),
(958, '6931598219533', 'GUANTE CUERO AMAR NEGRO', 'UNIDAD', 'Proveedor General', 'FERRAWYY', 1, '2016-12-27 17:03:36', 8, 8, 8, 8, 1, 1, 0, '', ''),
(959, '752148584552', 'GUANTES PARA SOLDAR', 'UNIDAD', 'Proveedor General', 'RED FORCE', 1, '2016-12-27 17:04:27', 280, 280, 10, 10, 1, 1, 0, '', ''),
(960, '7501206673157', 'GUANTES LARGOS DE CARNAZA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-27 17:10:42', 170, 170, 2, 2, 1, 1, 0, '', ''),
(961, '7506240605676', 'SIERRRA CIRCULAR', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 09:18:07', 4100, 4100, 1, 1, 1, 1, 0, '', ''),
(962, '7501206616482', 'COPA DE 1-7/8', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 09:25:28', 265, 265, 3, 3, 1, 1, 0, '', ''),
(963, '963', 'COPA DE 13MM', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 09:32:49', 1, 1, 1, 1, 1, 1, 0, '', ''),
(964, '964', 'COPA T-50', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 09:33:56', 1, 1, 1, 1, 1, 1, 0, '', ''),
(965, '965', 'COPA DE 1 PULG', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 09:36:42', 35, 435, 2, 2, 1, 1, 0, '', ''),
(966, '966', 'COPA DE 22MM', 'UNIDAD ', 'Proveedor General', 'NICHOLSON', 1, '2016-12-28 09:37:34', 55, 55, 2, 2, 1, 1, 0, '', ''),
(967, '967', 'COPA DE 18MM', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 09:39:03', 35, 35, 3, 3, 1, 1, 0, '', ''),
(968, 'D-5430-EM', 'COPA 30MM', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 09:42:31', 87, 87, 1, 1, 1, 1, 0, '', ''),
(969, '7453001157168', 'COPA DE 16MM', 'UNIDAD ', 'Proveedor General', 'BEST VALUE', 1, '2016-12-28 09:46:01', 35, 35, 1, 1, 1, 1, 0, '', ''),
(970, '7453001157083', 'COPA DE 8MM', 'UNIDAD ', 'Proveedor General', 'BEST VALUE', 1, '2016-12-28 09:47:02', 35, 35, 2, 2, 1, 1, 0, '', ''),
(971, '7453001157106', 'COPA DE 10MM', 'UNIDAD ', 'Proveedor General', 'BEST VALUE', 1, '2016-12-28 09:47:44', 35, 35, 1, 1, 1, 1, 0, '', ''),
(972, '7453001157090', 'COPA DE 9MM', 'UNIDAD ', 'Proveedor General', 'BEST VALUE', 1, '2016-12-28 09:48:16', 35, 35, 2, 2, 1, 1, 0, '', ''),
(973, '7453001157120', 'COPA DE 12MM', 'UNIDAD ', 'Proveedor General', 'BEST VALUE', 1, '2016-12-28 09:48:53', 35, 35, 1, 1, 1, 1, 0, '', ''),
(974, '7433200480038', 'ELIMINA MANCHA ', 'UNIDAD ', 'Proveedor General', 'COSTREC', 1, '2016-12-28 10:15:19', 125, 125, 6, 6, 1, 1, 0, '', ''),
(975, '7425030300709', 'DESMANCHADOR ', 'UNIDAD ', 'Proveedor General', 'COSTREX ', 1, '2016-12-28 10:18:46', 130, 130, 5, 5, 1, 1, 0, '', ''),
(976, '976', 'DESENGRASANTE DE COCINA ', 'UNIDAD ', 'Proveedor General', 'FUTEC INDUSTRIAL ', 1, '2016-12-28 10:20:04', 60, 60, 4, 4, 1, 1, 0, '', ''),
(977, '977', 'ABRILLANTADOR ', 'UNIDAD ', 'Proveedor General', 'FUTEC INDUSTRIAL ', 1, '2016-12-28 10:21:06', 55, 55, 3, 3, 1, 1, 0, '', ''),
(978, 'TDH 897536', 'LLANTIL ', 'UNIDAD ', 'Proveedor General', 'SILICAR ', 1, '2016-12-28 10:22:29', 140, 140, 2, 2, 1, 1, 0, '', ''),
(979, '979', 'DIABLO ROJO ', 'UNIDAD ', 'Proveedor General', 'SOLASA ', 1, '2016-12-28 10:23:22', 50, 50, 12, 12, 1, 1, 0, '', ''),
(980, '7425030300259', 'POTASA DESATORADOR', 'UNIDAD', 'Proveedor General', 'HQ', 1, '2016-12-28 10:24:58', 60, 60, 6, 6, 1, 1, 0, '', ''),
(981, '6953899621372', 'MOLINO PARA MOLER CARNE ', 'UNIDAD', 'Proveedor General', 'MEAT MINCER', 1, '2016-12-28 10:26:52', 370, 370, 1, 1, 1, 1, 0, '', ''),
(982, '982', 'MAQUINA PARA MOLER MAIZ ', 'UNIDAD ', 'Proveedor General', 'VICTORIA ', 1, '2016-12-28 10:28:24', 720, 720, 2, 2, 1, 1, 0, '', ''),
(983, '7453001101048', 'SELLADOR MULTIUSO ', 'UNIDAD ', 'Proveedor General', 'BEST VALUE', 1, '2016-12-28 10:30:31', 100, 100, 11, 11, 1, 1, 0, '', ''),
(984, '093371398944', 'SILICON ', 'UNIDAD ', 'Proveedor General', 'SPI', 1, '2016-12-28 10:31:30', 120, 120, 7, 7, 1, 1, 0, '', ''),
(985, '8711595165488', 'SELLADOR DE POLIURETANO', 'UNIDAD ', 'Proveedor General', 'SUR', 1, '2016-12-28 10:32:38', 120, 120, 2, 2, 1, 1, 0, '', ''),
(986, '7441105658216', 'PEGAFORTE ', 'UNIDAD ', 'Proveedor General', 'SUR ', 1, '2016-12-28 10:41:39', 1, 1, 1, 1, 1, 1, 0, '', ''),
(987, '987', 'SILICON MULTIPROPOSITO ', 'UNIDAD ', 'Proveedor General', 'CLINO ', 1, '2016-12-28 10:42:37', 120, 120, 2, 2, 1, 1, 0, '', ''),
(988, '7441105688329', 'MASILLA PARA MADERA ', 'UNIDAD ', 'Proveedor General', 'SUR ', 1, '2016-12-28 10:44:09', 55, 55, 13, 13, 1, 1, 0, '', ''),
(989, '989', 'MADEROL', 'GALON ', 'Proveedor General', 'INB.RODRIGUEZ', 1, '2016-12-28 10:46:58', 390, 390, 1, 1, 1, 1, 0, '', ''),
(990, '990', 'MADEROL', 'CUARTO', 'Proveedor General', 'INB.RODRIGUEz', 1, '2016-12-28 10:49:02', 130, 130, 12, 12, 1, 1, 0, '', ''),
(991, '7433200040010', 'PRESERVANTE PARA MADERA ', 'CUARTO', 'Proveedor General', ' KLEBE', 1, '2016-12-28 10:50:58', 150, 150, 3, 3, 1, 1, 0, '', ''),
(992, '992', 'MADEROL', '1/8', 'Proveedor General', 'INB.RODRIGUEZ', 1, '2016-12-28 10:52:41', 90, 90, 11, 11, 1, 1, 0, '', ''),
(993, '000413820-L14', 'BARNIZ TRANSPARENTE ', 'CUARTO', 'Proveedor General', 'CORAL ', 1, '2016-12-28 10:54:49', 160, 160, 4, 4, 1, 1, 0, '', ''),
(994, '000416985-L14', 'BARNIZ TINTE CAOBA ', 'CUARTO', 'Proveedor General', 'SUR', 1, '2016-12-28 10:56:39', 260, 260, 3, 3, 1, 1, 0, '', ''),
(995, '7441105623863', 'TINTES CAOBA ', '1/16', 'Proveedor General', 'SUR', 1, '2016-12-28 11:09:51', 85, 85, 8, 8, 1, 1, 0, '', ''),
(996, '000000109994', 'ACEITE BRILLANTE COLOR NEGRO ', 'CUARTO', 'Proveedor General', 'PROFESIONAL', 1, '2016-12-28 11:12:20', 1, 1, 1, 1, 1, 1, 0, '', ''),
(997, '997', 'ACABADO PARA MADERA COLOR GENIZARO ANTIGUO', 'CUARTO', 'Proveedor General', 'PECTRUM', 1, '2016-12-28 11:13:18', 1, 1, 1, 1, 1, 1, 0, '', ''),
(998, '718594007854', 'SELLADOR SUPER CONCENTRADO', 'CUARTO', 'Proveedor General', 'LANCO', 1, '2016-12-28 11:18:43', 200, 200, 11, 11, 1, 1, 0, '', ''),
(999, '718594340456', 'BARNIZ ENTINTADO COLOR ROBLE MEDIO', 'CUARTO', 'Proveedor General', 'LANCO', 1, '2016-12-28 11:22:19', 200, 200, 6, 6, 1, 1, 0, '', ''),
(1000, '718594067063', 'TINTE PARA MADERA DARK BROWN', '1/8', 'Proveedor General', 'LANCO ', 1, '2016-12-28 11:32:34', 110, 110, 1, 1, 1, 1, 0, '', ''),
(1001, '718594070865', 'TINTE PARA MADERA JACOBEN ', '1/8', 'Proveedor General', 'LANCO ', 1, '2016-12-28 11:33:34', 110, 110, 1, 1, 1, 1, 0, '', ''),
(1002, '718594071169', 'TINTE PARA MADERA RED YELLOW', '1/8', 'Proveedor General', 'LANCO ', 1, '2016-12-28 11:35:10', 110, 110, 3, 3, 1, 1, 0, '', ''),
(1003, '1003', 'ACABADO PARA MADERA CRISTOBAL ', 'CUARTO', 'Proveedor General', 'PECTRUM', 1, '2016-12-28 11:48:39', 210, 210, 2, 2, 1, 1, 0, '', ''),
(1004, '1004', 'ACABADO PARA MADERA COLOR MIEL ', 'CUARTO ', 'Proveedor General', 'PECTRUM', 1, '2016-12-28 11:49:20', 210, 210, 5, 5, 1, 1, 0, '', ''),
(1005, '1005', 'ACABADO PARA MADERA COLOR EBANO ', 'CUARTO', 'Proveedor General', 'PECTRUM', 1, '2016-12-28 11:49:59', 210, 210, 2, 2, 1, 1, 0, '', ''),
(1006, '1006', 'ACABADO PARA MADERA COLOR NOGAL OSCURO ', 'CUARTO ', 'Proveedor General', 'PECTRUM', 1, '2016-12-28 11:50:40', 210, 210, 5, 5, 1, 1, 0, '', ''),
(1007, '1007', 'ACABADO PARA MADERA COLOR NOGAL CLARO ', 'CUARTO ', 'Proveedor General', 'PECTRUM ', 1, '2016-12-28 11:52:31', 210, 210, 6, 6, 1, 1, 0, '', ''),
(1008, '7433200040195', 'SEMENTO DE CONTACTO ', '1/8', 'Proveedor General', 'KLEBE', 1, '2016-12-28 12:01:06', 60, 60, 3, 3, 1, 1, 0, '', ''),
(1009, '1009', 'SEMENTO DE CONTACTO ', '1/8', 'Proveedor General', 'PEGAMAX', 1, '2016-12-28 12:02:10', 80, 80, 3, 3, 1, 1, 0, '', ''),
(1010, '7441105670119', 'SEMENTO DE CONTACTO ', 'CUARTO', 'Proveedor General', 'PEGA FORTE', 1, '2016-12-28 12:04:05', 210, 20, 5, 5, 1, 1, 0, '', ''),
(1011, '1011', 'REMOBEDOR DE PINTURA ', 'CUARTO ', 'Proveedor General', 'FURIOZO', 1, '2016-12-28 12:05:04', 210, 210, 4, 4, 1, 1, 0, '', ''),
(1012, '718594084466', 'COLA PARA MADERA ', 'UNIDAD ', 'Proveedor General', 'LANCO ', 1, '2016-12-28 12:08:26', 140, 140, 1, 1, 1, 1, 0, '', ''),
(1013, '7441105608662', 'BARNIZ TRANSPARENTE BRILLANTE ', 'CUARTO ', 'Proveedor General', 'SUR ', 1, '2016-12-28 12:13:42', 175, 175, 14, 14, 1, 1, 0, '', ''),
(1014, '7441105660936', 'MASILLA PLASTICA ', 'GALON ', 'Proveedor General', 'SUR ', 1, '2016-12-28 12:15:09', 560, 560, 1, 1, 1, 1, 0, '', ''),
(1015, '718594061047', 'PEGA CONCRETO ', 'GALON ', 'Proveedor General', 'LANCO', 1, '2016-12-28 13:22:01', 265, 265, 5, 5, 1, 1, 0, '', ''),
(1016, '1016', 'THINER ACRILICO ', 'GALON', 'Proveedor General', 'TM', 1, '2016-12-28 13:22:44', 1, 1, 11, 11, 1, 1, 0, '', ''),
(1017, '769409139376', 'CORANA CLASICA ESMALTE ACRILICO COLOR NARANJA WASHINTONG', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:33:19', 400, 400, 7, 7, 1, 1, 0, '', ''),
(1018, '769409074349', 'CORONA CLASICA LATEX ACRILICA COLOR ALMENDRA', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:35:19', 250, 250, 4, 4, 1, 1, 0, '', ''),
(1019, '769409053078', 'CORONA DURA ESMALTE COLOR CAOBA ', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:39:33', 330, 330, 7, 7, 1, 1, 0, '', ''),
(1020, '769409053016', 'CORONA DURA ESMALTE COLOR MARFIL', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:41:09', 335, 335, 4, 4, 1, 1, 0, '', ''),
(1021, '769409134586', 'CORONA DURA ESMALTE COLOR TURQUESA', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:43:26', 335, 335, 2, 2, 1, 1, 0, '', ''),
(1022, '769409134647', 'CORONA DURA ESMALTE COLOR TERRACOTA', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:45:11', 335, 335, 2, 2, 1, 1, 0, '', ''),
(1023, '769409125447', 'CORONA CLÃSICA LÃTEX ACRÃLICO COLOR MANDARINA', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:48:07', 250, 250, 2, 2, 1, 1, 0, '', ''),
(1024, '769409125164', 'CORONA CLASICA LATEX ACRILICA COLOR AZUL AGATHA', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:49:27', 250, 250, 4, 4, 1, 1, 0, '', ''),
(1025, '769409052408', 'CORONA CLASICA LATEX ACRILICO COLOR AQUA ', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:50:19', 250, 250, 4, 4, 1, 1, 0, '', ''),
(1026, '769409075261', 'CORONA DURA ESMALTE COLOR PARDO', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:52:07', 335, 335, 1, 1, 1, 1, 0, '', ''),
(1027, '769409134685', 'CORONA DURA ESMALTE COLOR AMARILLO SOL', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:52:44', 335, 335, 1, 1, 1, 1, 0, '', ''),
(1028, '769409051234', 'CORONA CLASICA ESMALTE COLOR VERDE SELVA ', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:53:29', 335, 335, 1, 1, 1, 1, 0, '', ''),
(1029, '769409134623', 'CORONA DURA ESMALTE COLOR SAFIRO', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 13:54:37', 335, 335, 4, 4, 1, 1, 0, '', ''),
(1030, '769409065828', 'CORONA DURA ESMALTE COLOR CELESTE', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:05:09', 335, 335, 1, 1, 1, 1, 0, '', ''),
(1031, '769409134661', 'CORONA DURA ESMALTE COLOR ARENA', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:05:57', 335, 335, 1, 1, 1, 1, 0, '', ''),
(1032, '769409065972', 'CORONA DURA ESMALTE COLOR ROJO OXIDO', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:06:44', 335, 335, 1, 1, 1, 1, 0, '', ''),
(1033, '718594144245', 'ESMALTE MULTIUSO DE ACEITE', 'GALON', 'Proveedor General', 'LANCO', 1, '2016-12-28 14:09:36', 380, 380, 4, 4, 1, 1, 0, '', ''),
(1034, '718594143644', 'ESMALTE MULTIUSO DE ACEITE COLOR SUPER RED', 'Galon', 'Proveedor General', 'LANCO', 1, '2016-12-28 14:11:14', 380, 380, 6, 6, 1, 1, 0, '', ''),
(1035, '769409137259', 'CORONA CROMATIC COLOR RUSTIC TILE', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:13:55', 450, 450, 4, 4, 1, 1, 0, '', ''),
(1036, '769409075988', 'CORONA CROMATIC COLOR OLIVE', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:14:52', 450, 450, 2, 2, 1, 1, 0, '', ''),
(1037, '769409061448', 'CORONA CROMATIC COLOR TROPICAL PEACH', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:16:24', 450, 450, 2, 2, 1, 1, 0, '', ''),
(1038, '769409073892', 'CORONA CROMATIC COLOR ANTIQUE RED', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:17:10', 450, 450, 1, 1, 1, 1, 0, '', ''),
(1039, '769409125409', 'CORONA CLASICA ACRILICA COLOR MENTA FUSIAN', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:23:17', 250, 250, 1, 1, 1, 1, 0, '', ''),
(1040, '769409125157', 'CORONA CLASICA ACRILICA COLOR CAFE TOSTADO', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:24:21', 250, 250, 5, 5, 1, 1, 0, '', ''),
(1041, '769409125317', 'CORONA CLASICA ACRILICA COLOR CREMA DURASNO', 'GALON ', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:25:10', 250, 250, 2, 2, 1, 1, 0, '', ''),
(1042, '769409125539', 'CORONA CLASICA ACRILICA COLOR OXIDO COLONIAL', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:26:05', 250, 250, 2, 2, 1, 1, 0, '', ''),
(1043, '769409052668', 'CORONACLASICA ACRILICO COLOR MELON PERSA ', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:26:44', 250, 250, 2, 2, 1, 1, 0, '', ''),
(1044, '769409125188', 'CORONA CLASICA ACRILICA COLOR ARENA EGIPSIA', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:28:40', 250, 250, 1, 1, 1, 1, 0, '', ''),
(1045, '769409052583', 'CORONA CLASICA ACRILICA COLOR AMARILLO SOLAR ', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:29:25', 250, 250, 1, 1, 1, 1, 0, '', ''),
(1046, '7894693024725', 'FREGADERO DE SOBRE PONER ', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-28 14:35:11', 1500, 1500, 3, 3, 1, 1, 0, '', ''),
(1047, '3251112433225', 'PANA DE SOBRE PONER', 'UNIDAD', 'Proveedor General', 'TAL TOOLS', 1, '2016-12-28 14:36:22', 1100, 1100, 1, 1, 1, 1, 0, '', ''),
(1048, '604321093088', 'PANA DE SOBRE PONER DOBLE ', 'UNIDAD', 'Proveedor General', 'EB', 1, '2016-12-28 14:39:07', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1049, '604321145046', 'FREGADERO DOBLE', 'UNIDAD', 'Proveedor General', 'EB', 1, '2016-12-28 14:42:16', 3400, 3400, 2, 2, 1, 1, 0, '', ''),
(1050, '033923090315', 'CARRIL DE ACERO', 'UNIDAD', 'Proveedor General', 'ESTAN', 1, '2016-12-28 14:44:36', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1051, '604321143547', 'FREGADERO DE UNA PANA ', 'UNIDAD', 'Proveedor General', 'EB', 1, '2016-12-28 14:46:00', 2450, 2450, 1, 1, 1, 1, 0, '', ''),
(1052, '769409058349', 'SELLADOR PARA BLOQUE Y CONCRETO', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:48:35', 180, 180, 8, 8, 1, 1, 0, '', ''),
(1053, '769409128721', 'CORONA DURA LATEX ACRILICA COLOR NARANJA MEDIO', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:55:53', 250, 250, 2, 2, 1, 1, 0, '', ''),
(1054, '769409053191', 'CORONA DURA LATEX ACRILICA COLOR BLANCO HUESO', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:57:17', 250, 250, 2, 2, 1, 1, 0, '', ''),
(1055, '769409128745', 'CORONA DURA LETEX ACRILICA COLOR VERDE GRAMA', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 14:58:29', 210, 210, 4, 4, 1, 1, 0, '', ''),
(1056, '769409053306', 'CORONA DURA LETEX ACRILICA COLOR TURQUEZA', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:00:20', 210, 210, 3, 3, 1, 1, 0, '', ''),
(1057, '769409053160', 'CORONA DURA LETEX ACRILICA COLOR BLANCO', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:01:52', 210, 210, 4, 4, 1, 1, 0, '', ''),
(1058, '769409053412', 'CORONA DURA LETEX ACRILICA COLOR GERANIO', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:02:52', 210, 210, 2, 2, 1, 1, 0, '', ''),
(1059, '769409066450', 'CORONA DURA LETEX ACRILICA COLOR SALMON OSCURO', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:04:23', 210, 210, 2, 2, 1, 1, 0, '', ''),
(1060, '769409053276', 'CORONA DURA LETEX ACRILICA COLOR AMARILLO PALIDO', 'GALON', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:05:47', 210, 210, 6, 6, 1, 1, 0, '', ''),
(1061, '769409074516', 'CORONA DURA LETEX ACRILICA COLOR MANDARINA', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:06:49', 210, 210, 7, 7, 1, 1, 0, '', ''),
(1062, '769409061561', 'CORONA DURA LETEX ACRILICA COLOR MELOCOTON', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:09:16', 210, 210, 2, 2, 1, 1, 0, '', ''),
(1063, '769409074493', 'CORONA DURA LETEX ACRILICA COLOR ORO TROPICAL', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:10:29', 210, 210, 9, 9, 1, 1, 0, '', ''),
(1064, '769409128646', 'CORONA DURA LETEX ACRILICA COLOR MARFIL', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:12:10', 210, 210, 5, 5, 1, 1, 0, '', ''),
(1065, '769409074530', 'CORONA DURA LETEX ACRILICA COLOR MELON', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:12:54', 210, 210, 2, 2, 1, 1, 0, '', ''),
(1066, '7419522321983', 'IMPERMIABILIZANTE AQUALOOK COLOR ROJO', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:16:03', 180, 180, 6, 6, 1, 1, 0, '', ''),
(1067, '7419522321990', 'IMPERMIABILIZANTE AQUALOOK COLOR VERDE', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:16:51', 180, 180, 6, 6, 1, 1, 0, '', ''),
(1068, '7419522322003', 'IMPERMIABILIZANTE AQUALOOK COLOR BLANCO ', 'Galon', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:17:25', 180, 180, 5, 5, 1, 1, 0, '', ''),
(1069, '121996104', 'MURAL ESMALTE ACEITE COLOR MARFIL', 'CUARTO', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:20:24', 105, 105, 3, 3, 1, 1, 0, '', ''),
(1070, '121998504', 'MURAL ESMALTE ACEITE COLOR AQUA', 'CUARTO', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:21:45', 105, 105, 1, 1, 1, 1, 0, '', ''),
(1071, '121995201', 'MURAL ESMALTE ACEITE NARANJA', 'CUARTO', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:23:52', 105, 105, 1, 1, 1, 1, 0, '', ''),
(1072, '121995504', 'MURAL ESMALTE ACEITE COLOR MORADO PALIDO', 'CUARTO', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:25:09', 105, 105, 2, 2, 1, 1, 0, '', ''),
(1073, '718594022659', 'SELLADOR ESTIRENO ACRILICO SILICONIZER', 'CUARTO', 'Proveedor General', 'LANCO', 1, '2016-12-28 15:26:01', 160, 160, 2, 2, 1, 1, 0, '', ''),
(1074, '7419522226073', 'ESMALTE PENINSULAR COLOR BINO TINTO', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:27:28', 160, 160, 2, 2, 1, 1, 0, '', ''),
(1075, '7419522308762', 'BLOQUEADOR DE AGUA ', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:28:00', 180, 180, 1, 1, 1, 1, 0, '', ''),
(1076, '7419522308465', 'MASILLA PARA GRIETA COLOR MATE BLANCO', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:29:00', 165, 165, 1, 1, 1, 1, 0, '', ''),
(1077, '7419522208024', 'MASILLA PARA GRIETA INTERIOR MATE BLANCO', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:30:18', 220, 220, 1, 1, 1, 1, 0, '', ''),
(1078, '7419522228367', 'ESMALTE BRILLANTE COLOR NARANJA INTENSO', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:32:35', 120, 120, 2, 2, 1, 1, 0, '', ''),
(1079, '7419522228640', 'ESMALTE BRILLANTE COLOR BRILLANTE NEGRO', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:33:25', 120, 120, 1, 1, 1, 1, 0, '', ''),
(1080, '7419522224703', 'ESMALTE BRILLANTE COLOR NARANJA MANDARINA', 'CUARTO', 'Proveedor General', 'SHER', 1, '2016-12-28 15:34:07', 120, 120, 5, 5, 1, 1, 0, '', ''),
(1081, '7419522228428', 'ESMALTE BRILLANTE COLOR CHOCOLATE', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:34:43', 120, 120, 3, 3, 1, 1, 0, '', ''),
(1082, '7419522228510', 'ESMALTE BRILLANTE COLOR AZUL', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:35:16', 120, 120, 1, 1, 1, 1, 0, '', ''),
(1083, '7419522228619', 'ESMALTE BRILLANTE COLOR VERDE OSCURO', 'CUARTO', 'Proveedor General', 'SHERWIN WILLIAMS', 1, '2016-12-28 15:35:58', 120, 120, 1, 1, 1, 1, 0, '', ''),
(1084, '769409076657', 'CORONA DURA ANTICORROSIVA', 'Galon', 'Proveedor General', 'CORONA', 1, '2016-12-28 15:37:00', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1085, '121986101', 'MURAL LATEX COLOR MANDARINA', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:39:23', 220, 220, 3, 3, 1, 1, 0, '', ''),
(1086, '121987501', 'MURAL LATEX COLOR ROJO TEJA', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:41:00', 220, 220, 2, 2, 1, 1, 0, '', ''),
(1087, '121992301', 'MURAL LATEX COLOR PALO ROSA', 'Galon', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:42:05', 220, 220, 3, 3, 1, 1, 0, '', ''),
(1088, '121986301', 'MURAL LATEX COLOR TERRACOTA', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:43:28', 220, 220, 3, 3, 1, 1, 0, '', ''),
(1089, '121987701', 'MURAL LATEX COLOR MANGO', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:44:44', 220, 220, 4, 4, 1, 1, 0, '', ''),
(1090, '121986701', 'MURAL LATEX COLOR AMARILLO OCRE', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:45:43', 220, 220, 2, 2, 1, 1, 0, '', ''),
(1091, '121985701', 'MURAL LATEX COLOR VERDE MANZANA', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:46:35', 220, 220, 1, 1, 1, 1, 0, '', ''),
(1092, '121992101', 'MURAL LATEX COLOR ROSA PALIDO', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:47:34', 220, 220, 2, 2, 1, 1, 0, '', ''),
(1093, '121935501', 'MURAL LATEX COLOR BEIGE', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:48:11', 220, 220, 3, 3, 1, 1, 0, '', ''),
(1094, '121986501', 'MURAL LATEX COLOR BARRO', 'GALON', 'Proveedor General', 'OUPONT', 1, '2016-12-28 15:49:12', 220, 220, 2, 2, 1, 1, 0, '', ''),
(1095, '122175001', 'PRIMA PLUS VERDE LIMON', 'Galon', 'Proveedor General', 'AXALTA', 1, '2016-12-28 15:51:12', 210, 210, 4, 4, 1, 1, 0, '', ''),
(1096, '7501206613689', 'BOMBA CENTRIFUGA PARA AGUA ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 15:52:18', 2500, 2500, 1, 1, 1, 1, 0, '', ''),
(1097, '7417000535105', 'BOMBA CENTRIFUGA PARA AGUA 1/2 HP', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 15:53:11', 1400, 1400, 1, 1, 1, 1, 0, '', ''),
(1098, '662383734301', 'ACEITE PARA MOTORES DIESEL', 'CUBETA', 'Proveedor General', 'TOTAL', 1, '2016-12-28 15:54:43', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1099, '7417000535136', 'BOMBA CENTRIFUGA DE 1/3 HP', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 15:55:19', 1800, 1800, 1, 1, 1, 1, 0, '', ''),
(1100, '7431003710048', 'DILUYENTE DE PINTURA', 'GALON', 'Proveedor General', 'FUTEC', 1, '2016-12-28 15:57:44', 220, 220, 49, 49, 1, 1, 0, '', ''),
(1101, '7425030300037', 'ACELERANTE PARA CONCRETO', 'GALON', 'Proveedor General', 'PLASTICON', 1, '2016-12-28 16:00:37', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1102, '7431003705020', 'CRIOLINA', 'GALON', 'Proveedor General', 'FUTEC', 1, '2016-12-28 16:01:06', 1, 1, 2, 2, 1, 1, 0, '', ''),
(1103, '7453055396735', 'GUANTES PARA TRABAJO', 'UNIDAD', 'Proveedor General', 'BRINK', 1, '2016-12-28 16:07:33', 85, 85, 1, 1, 1, 1, 0, '', ''),
(1104, '7223121555164', 'GU8ANTES DE SEGURIDAD PUÃ‘O AVIERTO', 'UNIDAD', 'Proveedor General', 'WORKMAN', 1, '2016-12-28 16:08:42', 110, 110, 4, 4, 1, 1, 0, '', ''),
(1105, '7453001105701', 'GUANTES PARA MECANICA', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-28 16:09:39', 220, 220, 3, 3, 1, 1, 0, '', ''),
(1106, '1106', 'GUANTES PARA MOTO', 'UNIDAD', 'Proveedor General', 'PROBICKER', 1, '2016-12-28 16:10:31', 280, 280, 1, 1, 1, 1, 0, '', ''),
(1107, '6987021103825', 'GUANTES PARA SOLDAR ROJO', 'UNIDAD', 'Proveedor General', 'WELDING GLOVES', 1, '2016-12-28 16:12:07', 170, 170, 24, 24, 1, 1, 0, '', ''),
(1108, '7501206683026', 'GUANTES RESISTENTES A QUIMICOS', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 16:13:46', 280, 280, 6, 6, 1, 1, 0, '', ''),
(1109, '7501206652084', 'ASPERSOR METALICO DE PULSACIONES', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 16:14:35', 240, 240, 2, 2, 1, 1, 0, '', ''),
(1110, '7891117064961', 'HIDRO PISTOLA', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-28 16:15:42', 185, 185, 2, 2, 1, 1, 0, '', ''),
(1111, '7891117043560', 'HIDROPISTOLA MULTIFUNSIONAL', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-28 16:18:04', 230, 230, 4, 4, 1, 1, 0, '', ''),
(1112, '7891117010258', 'REGADOR', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-28 16:19:15', 100, 100, 5, 5, 1, 1, 0, '', ''),
(1113, '7417000528756', 'MULTIHERRAMIENTA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:20:18', 380, 380, 2, 2, 1, 1, 0, '', ''),
(1114, '7417000555066', 'PISTOLA PLASTICA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:21:10', 200, 200, 3, 3, 1, 1, 0, '', ''),
(1115, '7417000535587', 'MINI PINSA PUNTA DE AGUA  6"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:21:47', 65, 65, 2, 2, 1, 1, 0, '', ''),
(1116, '7417000525366', 'ASPERSOR ESTASIONARIO METALICO', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:22:20', 180, 180, 3, 3, 1, 1, 0, '', ''),
(1117, '7417000535594', 'MINI PINSA CORTE DIAGONAL', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:23:32', 65, 65, 6, 6, 1, 1, 0, '', ''),
(1118, '7417000535600', 'MINI PINSA PUNTA Y CORTE', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:24:03', 70, 70, 4, 4, 1, 1, 0, '', ''),
(1119, '7417000535648', 'MINI PINSA PUNTA CURVA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:24:42', 65, 65, 6, 6, 1, 1, 0, '', ''),
(1120, '7417000535617', 'MINI PINSACOMBINADA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:27:36', 70, 70, 1, 1, 1, 1, 0, '', ''),
(1121, '7417000535624', 'MINI PINSA PUNTA REDONDA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:28:30', 70, 70, 6, 6, 1, 1, 0, '', ''),
(1122, '7417000527216', 'ESPATULA PLASTICA DE 4"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:29:07', 22, 22, 17, 17, 1, 1, 0, '', ''),
(1123, '7417000527193', 'ESPATULA PLASTICA DE 2"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:29:37', 15, 15, 8, 8, 1, 1, 0, '', ''),
(1124, '7417000527209', 'ESPATULA PLASTICA DE 3"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:30:09', 18, 18, 7, 7, 1, 1, 0, '', ''),
(1125, '7417000538380', 'RASTRILLO DE JARDINERIA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 16:32:45', 90, 90, 3, 3, 1, 1, 0, '', ''),
(1126, '9328675017330', 'ESPATULA DE 1-1/2"', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-28 16:34:27', 42, 42, 2, 2, 1, 1, 0, '', ''),
(1127, '7501206634363', 'ESPATULA DE 2"', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:35:11', 45, 45, 4, 4, 1, 1, 0, '', ''),
(1128, '7501206634349', 'ESPATULA FLEXIBLE DE 3"', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:35:51', 45, 45, 1, 1, 1, 1, 0, '', ''),
(1129, '747752280808', 'ESPATUL FLEXIBLE DE 1-1/2"', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-28 16:37:41', 45, 45, 2, 2, 1, 1, 0, '', ''),
(1130, '7501206648957', 'LLAVE COMBINADA DE 11MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:38:35', 40, 40, 9, 9, 1, 1, 0, '', ''),
(1131, '7501206648940', 'LLAVE COMBINADA DE 10 MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:39:34', 35, 35, 5, 5, 1, 1, 0, '', ''),
(1132, '8022813523042', 'LLAVE COMBINADA DE 9MM', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-28 16:44:23', 48, 48, 2, 2, 1, 1, 0, '', ''),
(1133, '8022813523035', 'LLAVE COMBINADA DE 8MM', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-28 16:44:46', 48, 48, 2, 2, 1, 1, 0, '', ''),
(1134, '660731408096', 'LLAVE COMBINADA DE 9MM', 'UNIDAD', 'Proveedor General', 'FOY TOOLS', 1, '2016-12-28 16:45:36', 34, 34, 1, 1, 1, 1, 0, '', ''),
(1135, '8022813523028', 'LLAVE COMBINADA DE 7MM', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-28 16:46:20', 45, 45, 5, 5, 1, 1, 0, '', ''),
(1136, '8022813523011', 'LLAVE COMBINADA DE 6MM', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-28 16:47:03', 48, 48, 6, 6, 1, 1, 0, '', ''),
(1137, '7891114009262', 'LLAVE COMBINADA DE 12 MM', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-28 16:47:42', 65, 65, 2, 2, 1, 1, 0, '', ''),
(1138, '660731408126', 'LLAVE COMBINADA DE 12 MM', 'UNIDAD', 'Proveedor General', 'FOY TOOLS', 1, '2016-12-28 16:48:25', 48, 48, 1, 1, 1, 1, 0, '', ''),
(1139, '7501206648971', 'LLAVE COMBINADA 13MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:49:16', 45, 45, 3, 3, 1, 1, 0, '', ''),
(1140, '1140', 'LLAVE COMBINADA 7/16 MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:50:17', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1141, '1141', 'LLAVE COMBINADA 3/8MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:50:51', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1142, '7501206648988', 'LLAVE COMBINADA 14 MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:51:15', 50, 50, 3, 3, 1, 1, 0, '', ''),
(1143, '7501206648995', 'LLAVE COMBINADA 15 MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:51:53', 55, 55, 3, 3, 1, 1, 0, '', ''),
(1144, '7501206649008', 'LLAVE COMBINADA 16MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:53:18', 60, 60, 5, 5, 1, 1, 0, '', ''),
(1145, '1145', 'LLAVE COMBINADA 17MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:54:04', 65, 65, 1, 1, 1, 1, 0, '', ''),
(1146, '7501206649022', 'LLAVE COMBINADA 18 MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:54:52', 70, 70, 4, 4, 1, 1, 0, '', ''),
(1147, '7501206642757', 'LLAVE COMBINADA 19MM', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 16:55:23', 140, 140, 2, 2, 1, 1, 0, '', ''),
(1148, '7501206649039', 'LLAVE COMBINADA 19 MM ', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:56:12', 80, 80, 2, 2, 1, 1, 0, '', ''),
(1149, '1149', 'LLAVE FIJA DE 22MM', 'UNIDAD', 'Proveedor General', 'KAGON', 1, '2016-12-28 16:57:58', 70, 70, 3, 3, 1, 1, 0, '', ''),
(1150, '1150', 'LLAVE COMBINADA DE 20 MM', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-28 16:58:28', 90, 90, 2, 2, 1, 1, 0, '', ''),
(1151, '1151', 'LLAVE COMBINADA 13/16 MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 16:58:55', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1152, '1152', 'LLAVE COMBINADA 1/2 MM', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 17:00:18', 1, 1, 2, 2, 1, 1, 0, '', ''),
(1153, '1153', 'LLAVE COMBINADA 1/2 MM', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-28 17:00:41', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1154, '1154', 'LLAVE COMBINADA 1/2 MM ', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-28 17:01:05', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1155, '7501206643273', 'LLAVE STILSON DE 18 ', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 17:03:12', 320, 320, 1, 1, 1, 1, 0, '', ''),
(1156, '7450077003443', 'JUEGO DE DESARMADORES DE 6 PIEZAS', 'UNIDAD', 'Proveedor General', 'TREK', 1, '2016-12-28 17:04:02', 175, 175, 2, 2, 1, 1, 0, '', ''),
(1157, '7417000523355', 'FORMON 1-1/4', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 17:04:33', 145, 145, 1, 1, 1, 1, 0, '', ''),
(1158, '1158', 'LLAVE COMBINADA 21 MM', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-28 17:05:52', 60, 60, 2, 2, 1, 1, 0, '', ''),
(1159, '1159', 'LLAVE COMBINADA 23 MM', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-28 17:06:31', 70, 70, 1, 1, 1, 1, 0, '', ''),
(1160, '1160', 'LLAVE COMBINADA 25 MM', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-28 17:07:24', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1161, '1161', 'LLAVE COMBINADA 27 MM', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-28 17:07:48', 1, 1, 2, 2, 1, 1, 0, '', ''),
(1162, '1162', 'LLAVE COMBINADA 32 MM', 'UNIDAD', 'Proveedor General', 'FORGE', 1, '2016-12-28 17:08:15', 165, 165, 2, 2, 1, 1, 0, '', ''),
(1163, '7501206641453', 'LLAVE STILSON DE 24 "', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-28 17:12:52', 880, 880, 1, 1, 1, 1, 0, '', ''),
(1164, '7501206648889', 'LLAVE STILSON DE 24"', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-28 17:13:31', 460, 460, 1, 1, 1, 1, 0, '', ''),
(1165, '1165', 'LLAVE STILSON 14"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 17:14:16', 265, 265, 1, 1, 1, 1, 0, '', ''),
(1166, '7417000500844', 'LLAVE STILSON DE 12 "', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 17:15:11', 220, 220, 1, 1, 1, 1, 0, '', ''),
(1167, '7417000500820', 'LLAVE STILSON DE 8"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-28 17:15:53', 140, 140, 1, 1, 1, 1, 0, '', ''),
(1168, '1168', 'LLAVE TIPO TEE 14MM', 'UNIDAD', 'Proveedor General', 'DLTOOLS', 1, '2016-12-28 17:18:14', 50, 50, 3, 3, 1, 1, 0, '', ''),
(1169, '1169', 'LLAVE TIPO TEE 12MM', 'UNIDAD', 'Proveedor General', 'DLDTOOLS', 1, '2016-12-28 17:18:48', 50, 50, 3, 3, 1, 1, 0, '', ''),
(1170, '1170', 'LLAVE CRECEN DE 12MM ', 'UNIDAD', 'Proveedor General', 'WOLFGANG', 1, '2016-12-28 17:19:42', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1171, '7417000512236', 'NIVEL PLASTICO TORPEDO 9"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-29 08:37:34', 70, 70, 4, 4, 1, 1, 0, '', ''),
(1172, '12364238929', 'PROTECTOR DE VOLTAGE 900', 'UNIDAD', 'Proveedor General', 'FORZA', 1, '2016-12-29 08:39:05', 400, 400, 2, 2, 1, 1, 0, '', ''),
(1173, '722312244520', 'LAMPARA PARA ESPOTRAR', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 08:41:12', 285, 285, 2, 2, 1, 1, 0, '', ''),
(1174, '7209212011276', 'INTERRUPTOR DE RIENDA 6A', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 08:43:14', 45, 45, 28, 28, 1, 1, 0, '', ''),
(1175, '7441109006372', 'ANGULO EXTERIOR PARA 25*17', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 08:44:58', 32, 32, 11, 11, 1, 1, 0, '', ''),
(1176, '722312244568', 'LAMPARA PARA ENPOTRAR ', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 08:46:20', 300, 300, 2, 2, 1, 1, 0, '', ''),
(1177, '722312244582', 'LAMPARA PARA EMPOTRAR ', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 08:47:55', 285, 285, 4, 4, 1, 1, 0, '', ''),
(1178, '7441109000226', 'BASE PORTAFLOURECENTE', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 08:49:20', 32, 32, 4, 4, 1, 1, 0, '', ''),
(1179, '7506240626091', 'CONECTOR 2 POLOS - TIERRA CON DOS PUERTOS USB', 'UNIDAD', 'Proveedor General', 'VOLTECH', 1, '2016-12-29 08:51:27', 260, 260, 2, 2, 1, 1, 0, '', ''),
(1180, '7592978392377', 'ENCHUFE MULTITOMA ', 'UNIDAD', 'Proveedor General', 'MA ELECTRIC', 1, '2016-12-29 08:53:08', 56, 56, 6, 6, 1, 1, 0, '', ''),
(1181, '7794008013188', 'ESPIRAL INTERNA', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 08:54:46', 170, 170, 11, 11, 1, 1, 0, '', ''),
(1182, '7794008013171', 'LAMPARA LUZ VERDE', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 08:56:22', 230, 230, 4, 4, 1, 1, 0, '', ''),
(1183, '7794008013164', 'LAMPARA LUZ ROJA', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 08:57:48', 180, 180, 3, 3, 1, 1, 0, '', ''),
(1184, '9041299674382', 'TOMA CORRIENTE DOBLE', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 08:59:50', 68, 68, 1, 1, 1, 1, 0, '', ''),
(1185, '8012199674025', 'INTERRUPTOR MONOBLOQUE', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 09:00:59', 60, 60, 65, 65, 1, 1, 0, '', ''),
(1186, '8012199674360', 'INTERRUPTOR MAS TOMA 2P', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 09:02:43', 100, 100, 11, 9, 11, 1, 0, '', ''),
(1187, '8012199674346', 'INTERRUPTOR + TOMA 2P MONOBLOQUE', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 09:06:19', 100, 100, 1, 1, 1, 1, 0, '', ''),
(1188, '8012199677293', 'INTERRUTOR TRES VIAS +TOMA 2P', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 09:09:17', 100, 100, 2, 2, 1, 1, 0, '', ''),
(1189, '7501206657270', 'PORTA LAMPARA DE VAQUELITA SEPO', 'UNIDAD', 'Proveedor General', 'VOLTECH', 1, '2016-12-29 09:11:55', 25, 25, 4, 4, 1, 1, 0, '', ''),
(1190, '7441031003531', 'INTEREUPTOR DE 1 VIA', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 09:14:37', 80, 80, 9, 9, 1, 1, 0, '', ''),
(1191, '7453911738075', 'INTERRUPTOR DE PASO', 'UNIDAD', 'Proveedor General', 'HUNTER', 1, '2016-12-29 09:15:40', 20, 20, 5, 5, 1, 1, 0, '', ''),
(1192, '7501206656587', 'CONTACTO SENCILLO CARA DE CHINO', 'UNIDAF', 'Proveedor General', 'VOLTECH', 1, '2016-12-29 09:16:55', 30, 30, 1, 1, 1, 1, 0, '', ''),
(1193, '7441109000554', 'TOMA CORRIENTE DOBLE POLARIZADO', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 09:18:21', 45, 45, 3, 3, 1, 1, 0, '', ''),
(1194, '7441109009410', 'INTERRUPTOR SENCILLO', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 09:19:25', 38, 38, 21, 21, 1, 1, 0, '', ''),
(1195, '7441109000479', 'INTERRUPTOR TRIPLE', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 09:20:16', 70, 70, 3, 3, 1, 1, 0, '', ''),
(1196, '7441109009465', 'INTERRUPTOR DOBLE', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 09:21:09', 55, 55, 3, 3, 1, 1, 0, '', ''),
(1197, '7441109001193', 'INTERRUPTOR DOBLE L', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 09:22:18', 38, 38, 6, 6, 1, 1, 0, '', ''),
(1198, '7441109010560', 'TOMA Y APAGADOR', 'UNIDAF', 'Proveedor General', 'EAGLE', 1, '2016-12-29 09:23:15', 55, 55, 4, 4, 1, 1, 0, '', ''),
(1199, '5180009132751', 'SEPO DE HULE', 'UNIDAD', 'Proveedor General', 'TALTOOLS', 1, '2016-12-29 09:51:02', 15, 15, 2, 2, 1, 1, 0, '', ''),
(1200, '7450077110431', 'SEPO DE HULE', 'UNIDAD', 'Proveedor General', 'BRICKELL', 1, '2016-12-29 09:52:26', 20, 20, 3, 3, 1, 1, 0, '', ''),
(1201, '690306101725', 'TOMA MULTIPLE', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 09:53:52', 22, 22, 1, 1, 1, 1, 0, '', ''),
(1202, '7441109000165', 'TOMACORRIENTE SUPERFICIAL', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 09:54:57', 20, 20, 9, 9, 1, 1, 0, '', ''),
(1203, '037332010643', 'PROTECTOR DE CORRIENTE', 'UNIDAD', 'Proveedor General', 'TRIPP-LITE', 1, '2016-12-29 09:56:51', 140, 140, 2, 2, 1, 1, 0, '', ''),
(1204, '8012199678467', 'PLACA DE 3 MODULOS', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 09:57:59', 65, 65, 8, 8, 1, 1, 0, '', ''),
(1205, '5180009112302', 'INTERRUPTOR DE PASO', 'UNIDAD', 'Proveedor General', 'TALTOOOS', 1, '2016-12-29 10:02:09', 50, 50, 5, 5, 1, 1, 0, '', ''),
(1206, '7506240625711', 'TEMPORIZADOR DIGITAL', 'UNIDAD', 'Proveedor General', 'VOLTECH', 1, '2016-12-29 10:03:16', 330, 330, 2, 2, 1, 1, 0, '', ''),
(1207, '7441109002459', 'TOMA CORRIENTE DOBLE', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 10:04:07', 28, 28, 4, 4, 1, 1, 0, '', ''),
(1208, '7453017910160', 'INTERRUPTOR TRIPLE', 'UNIDAD', 'Proveedor General', 'BRINK', 1, '2016-12-29 10:06:27', 58, 58, 3, 3, 1, 1, 0, '', ''),
(1209, '7506240626428', 'CARGADOR DE PILAS AA AAA', 'UNIDAD', 'Proveedor General', 'VOLTECH', 1, '2016-12-29 10:08:46', 410, 410, 2, 2, 1, 1, 0, '', ''),
(1210, '7441109001018', 'PLACA TOMA DOBLE', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 10:11:53', 11, 11, 3, 3, 1, 1, 0, '', ''),
(1211, '7441109004132', 'ENCHUFE DE VINIL ARMADO', 'UNIDAD', 'Proveedor General', 'AGUILA', 1, '2016-12-29 10:13:40', 50, 50, 4, 4, 1, 1, 0, '', ''),
(1212, '7450077008974', 'CARGADOR UNIVERSAL ', 'UNIDAD', 'Proveedor General', 'TREK', 1, '2016-12-29 10:15:00', 130, 130, 3, 3, 1, 1, 0, '', ''),
(1213, '7441109002190', 'TOMA DOBLE SUPERFICIAL', 'UNIDAF', 'Proveedor General', 'EAGLE', 1, '2016-12-29 10:16:29', 50, 50, 53, 53, 1, 1, 0, '', ''),
(1214, '7450077003962', 'ADAPTADOR MULTIPLE', 'UNIDAD', 'Proveedor General', 'BRICKELL', 1, '2016-12-29 10:17:28', 78, 78, 1, 1, 1, 1, 0, '', ''),
(1215, '7501206694701', 'TENSOR FORJADO GANCHO ARGOLLA', 'UNIDAD', 'Proveedor General', 'FIERRO', 1, '2016-12-29 10:18:58', 150, 150, 2, 2, 1, 1, 0, '', ''),
(1216, '7501206671948', 'PROBADOR DE CIRCUITOS', 'UNIDAF', 'Proveedor General', 'TRUPER', 1, '2016-12-29 10:21:56', 25, 25, 1, 1, 1, 1, 0, '', ''),
(1217, '6987021101500', 'MINI NIVEL', 'UNIDAD', 'Proveedor General', 'DIESEL TOOLS', 1, '2016-12-29 10:23:59', 15, 15, 1, 1, 1, 1, 0, '', ''),
(1218, '7453017910146', 'INTERRUPTOR SIMPLE', 'UNIDAD', 'Proveedor General', 'BRINK', 1, '2016-12-29 10:27:11', 32, 32, 1, 1, 1, 1, 0, '', ''),
(1219, '078477199985', 'SEPO', 'UNIDAD', 'Proveedor General', 'NOC', 1, '2016-12-29 10:28:20', 95, 95, 1, 1, 1, 1, 0, '', ''),
(1220, '7501206690529', 'CARGADOR PARA PILAS AA AAA', 'UNIDAD', 'Proveedor General', 'VOLTECH', 1, '2016-12-29 10:29:16', 580, 580, 1, 1, 1, 1, 0, '', ''),
(1221, '7453017910153', 'INTERRUPTOR DOBLE', 'UNIDAD', 'Proveedor General', 'BRINK', 1, '2016-12-29 10:30:20', 42, 42, 1, 1, 1, 1, 0, '', ''),
(1222, '8022884724027', 'CUERDA DE GOMA CON GANCHO', 'UNIDAD', 'Proveedor General', 'BRUFER', 1, '2016-12-29 10:33:46', 60, 60, 2, 2, 1, 1, 0, '', ''),
(1223, '7506240616320', 'JUAGO DE TRES MINI CEPILLOS DE ALAMBRA', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-29 10:35:29', 85, 85, 3, 3, 1, 1, 0, '', ''),
(1224, '7501206652039', 'CONEXION DE LATON PARA MANGUERA DE JARDIN', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 10:37:03', 85, 85, 1, 1, 1, 1, 0, '', ''),
(1225, '7501206652022', 'CONEXION DE LATON PARA MANGUERA DE JARDIN', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 10:38:09', 45, 45, 1, 1, 1, 1, 0, '', ''),
(1226, '7501206681367', 'CINTA DE 10 MTR CONTRA IMPACTO', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 10:39:20', 330, 330, 2, 2, 1, 1, 0, '', ''),
(1227, '1227', 'CRAYON PARA MARCAR', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-29 10:40:51', 25, 25, 8, 8, 1, 1, 0, '', ''),
(1228, '1228', 'TIZA PARA METAL', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-29 10:42:14', 14, 14, 12, 12, 1, 1, 0, '', ''),
(1229, '076174473506', 'LAPIZ PARA CARPINTERIA ', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-29 10:43:46', 14, 14, 29, 29, 1, 1, 0, '', ''),
(1230, '1230', 'LAPIZ PARA CARPINTERIA COLOR ROJO ', 'UNIDAF', 'Proveedor General', 'PROFI', 1, '2016-12-29 10:45:11', 14, 14, 17, 17, 1, 1, 0, '', ''),
(1231, '078143290459', 'SUPER GLUE', 'UNIDAD', 'Proveedor General', 'DEVCON', 1, '2016-12-29 10:46:12', 25, 25, 2, 2, 1, 1, 0, '', ''),
(1232, '7891200313846', 'SUPER VONDER', 'UNIDAD', 'Proveedor General', 'LOCTITE', 1, '2016-12-29 10:47:09', 38, 38, 1, 1, 1, 1, 0, '', ''),
(1233, '790920049875', 'SUPER GLUE', 'UNIDAD', 'Proveedor General', 'ABRO', 1, '2016-12-29 10:47:54', 25, 25, 1, 1, 1, 1, 0, '', ''),
(1234, '7891200013197', 'PEGAMENTO UNIVERSAL', 'UNIDAD', 'Proveedor General', 'LOCTITE', 1, '2016-12-29 10:49:19', 186, 186, 1, 1, 1, 1, 0, '', ''),
(1235, '078143313455', 'PEGAMENTO', 'UNIDAD', 'Proveedor General', 'DEVCON', 1, '2016-12-29 10:51:24', 110, 110, 6, 6, 1, 1, 0, '', ''),
(1236, '7501206658512', 'CLAVIJA REFORZADA CARA DE CHINO', 'UNIDAD', 'Proveedor General', 'VOLTECH', 1, '2016-12-29 10:53:40', 30, 30, 1, 1, 1, 1, 0, '', ''),
(1237, '078727132090', 'ESMERILADOR', 'UNIDAD', 'Proveedor General', 'VERSASHEM', 1, '2016-12-29 10:55:03', 75, 75, 3, 3, 1, 1, 0, '', ''),
(1238, '078727141092', 'LIMPIADOR PARA RADIADOR', 'UNIDAD', 'Proveedor General', 'VERSACHEM', 1, '2016-12-29 10:58:39', 40, 40, 2, 2, 1, 1, 0, '', ''),
(1239, '8718291738794', 'LAMPARA LUZ CLARA 27W', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-29 11:01:08', 170, 170, 4, 4, 1, 1, 0, '', ''),
(1240, '8718696436448', 'LAMPARA LUZ BLANCA 20W', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-29 11:02:06', 110, 110, 4, 4, 1, 1, 0, '', ''),
(1241, '8718696436400', 'LAMPARA LUZ BLANCA 15W', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-29 11:03:07', 95, 95, 1, 1, 1, 1, 0, '', ''),
(1242, 'C6-11', 'LAMPARA LUZ BLANCA 11W', 'UNIDAD', 'Proveedor General', 'TRESB', 1, '2016-12-29 11:07:28', 40, 40, 2, 2, 1, 1, 0, '', ''),
(1243, 'C6-45', 'LAMPARA LUZ BLANCA 45W', 'UNIDAD', 'Proveedor General', 'TRESB', 1, '2016-12-29 11:08:25', 110, 110, 1, 1, 1, 1, 0, '', ''),
(1244, 'C6-15', 'LAMPARA LUZ BLANCA 15W', 'UNIDAD', 'Proveedor General', 'TRESB', 1, '2016-12-29 11:10:19', 47, 47, 1, 1, 1, 1, 0, '', ''),
(1245, 'C6-20', 'LAMPARA LUZ BLANCA 20W', 'UNIDAD', 'Proveedor General', 'TRESB', 1, '2016-12-29 11:11:39', 55, 55, 4, 4, 1, 1, 0, '', ''),
(1246, 'C6-25', 'LAMPARA LUZ BLANCA 25W', 'UNIDAD', 'Proveedor General', 'TRESB', 1, '2016-12-29 11:12:55', 57, 57, 3, 3, 1, 1, 0, '', ''),
(1247, '7453001100652', 'LAMPARA LED 5W', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-29 11:14:12', 165, 165, 1, 1, 1, 1, 0, '', ''),
(1248, '7450077014227', 'LAMPARA LED RGB', 'UNIDAD', 'Proveedor General', 'SKY LIGHT', 1, '2016-12-29 11:16:19', 200, 200, 5, 5, 1, 1, 0, '', ''),
(1249, '8718696436486', 'LAMPARA LUZ BLANCA 23W', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-29 11:18:15', 120, 120, 4, 4, 1, 1, 0, '', ''),
(1250, '7453001115724', 'LAMPARA TORPEDO 7W', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-29 11:21:51', 100, 100, 5, 5, 1, 1, 0, '', ''),
(1251, '7417000554366', 'FOCO AHORRADOR', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-29 11:23:02', 130, 130, 5, 5, 1, 1, 0, '', ''),
(1252, '722312244506', 'BULBO REPUESTO PARA LAMPARA DE EMPOTRAR', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 11:24:30', 130, 130, 5, 5, 1, 1, 0, '', ''),
(1253, '7453071578863', 'LAMPARA LED 7W', 'UNIDAD', 'Proveedor General', 'GEL', 1, '2016-12-29 11:25:51', 160, 160, 2, 2, 1, 1, 0, '', ''),
(1254, '7453071806942', 'LAMPARA LED 7W', 'UNIDAD', 'Proveedor General', 'VELLMAX', 1, '2016-12-29 11:27:05', 185, 185, 3, 3, 1, 1, 0, '', ''),
(1255, 'LED-AF5', 'LAMPARA LED DE ALTO BRILLO 5W', 'UNIDAD', 'Proveedor General', 'IML', 1, '2016-12-29 11:28:47', 220, 220, 1, 1, 1, 1, 0, '', ''),
(1256, 'LED-AF4', 'LAMPARA LED ALTO BRILLI', 'UNIDAD', 'Proveedor General', 'IML', 1, '2016-12-29 11:29:41', 180, 180, 2, 2, 1, 1, 0, '', ''),
(1257, '6921454540048', 'LAMPARA BOMBILLO REDONDO 18W', 'UNIDAD', 'Proveedor General', 'IML', 1, '2016-12-29 11:31:15', 180, 180, 1, 1, 1, 1, 0, '', ''),
(1258, '7453071533572', 'LAMPARA LED 7W', 'UNIDAF', 'Proveedor General', 'HUNTER', 1, '2016-12-29 11:32:25', 130, 130, 4, 4, 1, 1, 0, '', ''),
(1259, '7743219876532', 'LAMPARA DE REPUESTO 36W', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 11:33:48', 280, 280, 5, 5, 1, 1, 0, '', ''),
(1260, '7441007032596', 'LAMPARA LUZ AMARRILLA 15W', 'UNIDAD', 'Proveedor General', 'SYLVANIA', 1, '2016-12-29 11:35:35', 120, 120, 7, 7, 1, 1, 0, '', ''),
(1261, '7794008013133', 'LAMPARA LUZ AMARILLA 13W', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 11:38:00', 175, 175, 5, 5, 1, 1, 0, '', ''),
(1262, '7794008013140', 'LAMPARA LUZ NEGRA 13W', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 11:39:07', 175, 175, 2, 2, 1, 1, 0, '', ''),
(1263, 'LED/W5', 'LAMPARA LUZ LED 5W', 'UNIDAD', 'Proveedor General', 'HIGH POWER LAMP', 1, '2016-12-29 11:41:06', 240, 240, 4, 4, 1, 1, 0, '', ''),
(1264, '794008734349', 'LAMAPARA LUZ BLANCA 13W=40W', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 11:42:35', 115, 115, 5, 5, 1, 1, 0, '', ''),
(1265, '7453001112433', 'LAMPARA LED 5W', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-29 11:43:42', 210, 210, 5, 5, 1, 1, 0, '', ''),
(1266, '8718696463208', 'LAMPARA LED 60W', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-29 11:44:50', 220, 220, 4, 4, 1, 1, 0, '', ''),
(1267, '8718696463246', 'LAMPARA LED 50W', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-29 11:45:05', 185, 185, 3, 3, 1, 1, 0, '', ''),
(1268, '7794008013270', 'LAMPARA LED 23W ', 'UNIDAD', 'Proveedor General', 'AMERICAN-ELECTRIC', 1, '2016-12-29 11:50:56', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1269, '1269', 'DISCO PARA METAL 4*5/64*5/8', 'UNIDAD', 'Proveedor General', 'SUPER-FLEX', 1, '2016-12-29 14:26:12', 30, 30, 7, 7, 1, 1, 0, '', ''),
(1270, '7914288100162', 'DISCO PARA CORTAR METAL 4*1/16*5/8', 'UNIDAD', 'Proveedor General', 'SUPER-FLEX', 1, '2016-12-29 14:30:32', 30, 30, 4, 4, 1, 1, 0, '', ''),
(1271, '4011890004159', 'DISCI PARA CORTAR METAL 9*1/8*7/8', 'UNIDAD', 'Proveedor General', 'RHODIUS', 1, '2016-12-29 14:32:37', 60, 60, 1, 1, 1, 1, 0, '', ''),
(1272, '8924890180326', 'DISCO PARA CORTAR METAL 7*1/8*7/8', 'UNIDAD', 'Proveedor General', 'UNIVERSAL CUT', 1, '2016-12-29 14:34:12', 30, 30, 1, 1, 1, 1, 0, '', ''),
(1273, '7894768020126', 'DISCO PARA CORTAR METAL 9*1/8*7/8', 'UNIDAD', 'Proveedor General', 'PREMIER', 1, '2016-12-29 14:35:45', 60, 60, 1, 1, 1, 1, 0, '', ''),
(1274, '7891114095524', 'DISCO PARA CORTAR METAL 9*1/8*7/8', 'UNIDAD', 'Proveedor General', 'TRAMONTINA', 1, '2016-12-29 14:37:02', 170, 170, 1, 1, 1, 1, 0, '', ''),
(1275, '088381185813', 'DISCO PARA CORTAR METAL 14*1/8*1', 'UNIDAD', 'Proveedor General', 'MAKITA', 1, '2016-12-29 14:38:38', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1395, '028877321806', 'DISCO PARA METAL 4-1/2*.045*7/8', 'UNIDAD', 'Proveedor General', 'DEWALT', 1, '2016-12-30 12:13:57', 48, 33, 54, 54, 1, 1, 0, '', ''),
(1277, '4007430135346', 'DISCO PARA CORTAR METAL 4-1/2*.04*7/8', 'UNIDAD', 'Proveedor General', 'METABO', 1, '2016-12-29 14:43:53', 60, 60, 19, 19, 6, 1, 0, '', ''),
(1278, '885911137508', 'DISCO PARA CORTAR METAL 9*5/64*7/8', 'UNIDAD', 'Proveedor General', 'DEWALT', 1, '2016-12-29 14:45:32', 95, 69, 35, 35, 1, 1, 0, '', ''),
(1279, '4027497513116', 'DISCO DIAMANTE', 'UNIDAD', 'Proveedor General', 'ROTTLUFF', 1, '2016-12-29 14:50:25', 100, 100, 3, 3, 1, 1, 0, '', ''),
(1280, '4007430135063', 'DISCO PARA CORTAR METAL 9*5/64*7/8', 'UNIDAD', 'Proveedor General', 'METABO', 1, '2016-12-29 14:53:13', 100, 100, 1, 1, 1, 1, 0, '', '');
INSERT INTO `products` (`id_producto`, `codigo_producto`, `nombre_producto`, `unidad_med`, `proveedor`, `marca`, `status_producto`, `date_added`, `precio_producto`, `precio_compra`, `stock`, `cantidad_total`, `ex_min`, `exento`, `salida`, `unidad_m`, `marca_p`) VALUES
(1281, '4007430135087', 'DISCO PARA CORTAR METAL 7*1/16*7/8', 'UNIDAD', 'Proveedor General', 'METABO', 1, '2016-12-29 14:54:33', 85, 85, 9, 9, 1, 1, 0, '', ''),
(1282, '4402274101159', 'DISCO DIAMANTE', 'UNIDAD', 'Proveedor General', 'KORFF & HONSBERG', 1, '2016-12-29 14:55:59', 100, 100, 1, 1, 1, 1, 0, '', ''),
(1283, '7591594222631', 'ESCOBILLA DE CARBON 3-B', 'UNIDAD', 'Proveedor General', 'CONINCA', 1, '2016-12-29 14:57:27', 60, 60, 4, 4, 1, 1, 0, '', ''),
(1284, '7591594223218', 'ESCOBILLA DE CARBON 160-B', 'UNIDAF', 'Proveedor General', 'CONINCA', 1, '2016-12-29 14:58:40', 60, 60, 4, 4, 1, 1, 0, '', ''),
(1285, '7591594790727', 'ESCOBILLA DE CARBON 99-B', 'UNIDAD', 'Proveedor General', 'CONINCA', 1, '2016-12-29 14:59:34', 60, 60, 1, 1, 1, 1, 0, '', ''),
(1286, '7591594265034', 'ESCOBILLA DE CARBON 222-B', 'UNIDAD', 'Proveedor General', 'CONINCA', 1, '2016-12-29 15:00:39', 70, 70, 11, 11, 1, 1, 0, '', ''),
(1287, '7591594223638', 'ESCOBILLA DE CARBON 104-B', 'UNIDAD', 'Proveedor General', 'CONINCA', 1, '2016-12-29 15:01:30', 60, 60, 10, 10, 1, 1, 0, '', ''),
(1288, '7591594223621', 'ESCOBILLA DE CARBON 103-B', 'UNIDAD', 'Proveedor General', 'CONINCA', 1, '2016-12-29 15:02:40', 67, 67, 9, 9, 1, 1, 0, '', ''),
(1289, '7591594223652', 'ESCOBILLA DE CARBON 106-B', 'UNIDAD', 'Proveedor General', 'CONINCA', 1, '2016-12-29 15:03:48', 56, 56, 11, 11, 1, 1, 0, '', ''),
(1290, '7501206640814', 'CERDA MEDIANA 6*3/4 "', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 15:05:18', 160, 160, 1, 1, 1, 1, 0, '', ''),
(1291, '7450077007083', 'DISCO DIAMANTE', 'UNIDAD', 'Proveedor General', 'BRICKELL', 1, '2016-12-29 15:06:28', 70, 70, 8, 8, 1, 1, 0, '', ''),
(1292, '7501206620915', 'SIERRA CIRCULAR', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 15:08:04', 230, 230, 2, 2, 1, 1, 0, '', ''),
(1293, '11127116', 'DISCO DIAMANTE 230MM', 'UNIDAD', 'Proveedor General', 'KORF & HONSBERG', 1, '2016-12-29 15:10:01', 300, 300, 2, 2, 1, 1, 0, '', ''),
(1294, '7453001160502', 'DISCO DIAMANTE CORTE CONTINUO', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-29 15:11:05', 365, 365, 2, 2, 1, 1, 0, '', ''),
(1295, '9416143960513', 'DISCO DIAMANTE 7"', 'UNIDAD', 'Proveedor General', 'BPA', 1, '2016-12-29 15:12:55', 150, 150, 1, 1, 1, 1, 0, '', ''),
(1296, '7453001151296', 'DISCO DIAMANTE SEGMENTADO 7-1/4"', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-29 15:14:08', 300, 300, 1, 1, 1, 1, 0, '', ''),
(1297, '6902015890178', 'DISCO DIAMANTE 7"', 'UNIDAD', 'Proveedor General', 'HERCULES', 1, '2016-12-29 15:15:48', 148, 148, 3, 3, 1, 1, 0, '', ''),
(1298, '4402274101807', 'DISCO DIAMANTE', 'UNIDAD', 'Proveedor General', 'KORF & HONSBERG', 1, '2016-12-29 15:16:50', 200, 200, 4, 4, 1, 1, 0, '', ''),
(1299, '7891792937635', 'DISCO DIAMANTE ', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 15:18:45', 310, 310, 2, 2, 1, 1, 0, '', ''),
(1300, '7501206603413', 'SIERRA CIRCULAR 7-1/4" ', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 15:21:19', 265, 265, 1, 1, 1, 1, 0, '', ''),
(1301, '7501206603802', 'SIERRA CIRCULAR', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 15:22:26', 195, 195, 1, 1, 1, 1, 0, '', ''),
(1302, '7501206624982', 'SIERRA CIRCULAR 12"', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 15:25:36', 550, 550, 1, 1, 1, 1, 0, '', ''),
(1303, '7501206696439', 'SIERRA CIRCULAR 14"', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-29 15:26:33', 910, 910, 1, 1, 1, 1, 0, '', ''),
(1304, '7450077007373', 'BROCHA 4"', 'UNIDAD', 'Proveedor General', 'BRICKELL', 1, '2016-12-29 15:29:45', 50, 50, 1, 1, 1, 1, 0, '', ''),
(1305, '7417000532340', 'BROCHA 4"', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-29 15:30:59', 100, 100, 6, 6, 1, 1, 0, '', ''),
(1306, '7450077007359', 'BROCHA 2"', 'UNIDAD', 'Proveedor General', 'BRICKELL', 1, '2016-12-29 15:32:08', 20, 20, 7, 7, 1, 1, 0, '', ''),
(1307, '7501206674963', 'BROCHA 3', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-29 15:33:35', 28, 28, 18, 18, 1, 1, 0, '', ''),
(1308, '7501206674956', 'BROCHA 2-1/2', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-29 15:34:51', 25, 25, 24, 24, 1, 1, 0, '', ''),
(1309, '2548206674956', 'BROCHA 2-1/2"', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-29 15:36:04', 25, 25, 14, 14, 1, 1, 0, '', ''),
(1310, '7503005496227', 'BROCHA 1/2"', 'UNIDAD', 'Proveedor General', 'BYP', 1, '2016-12-29 15:37:30', 10, 10, 7, 7, 1, 1, 0, '', ''),
(1311, '7501206674932', 'BROCHA 1/2', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-29 15:38:32', 15, 15, 23, 23, 1, 1, 0, '', ''),
(1312, '7501206674949', 'BROCHA 4"', 'UNIDAD', 'Proveedor General', 'PRETUL', 1, '2016-12-29 15:43:21', 20, 20, 4, 4, 1, 1, 0, '', ''),
(1313, '6920130015351', 'BRIDAS COLOR NEGRA PEQUENA ', 'UNIDAD', 'Proveedor General', 'TOP RACING', 1, '2016-12-29 15:46:09', 3, 3, 7, 7, 1, 1, 0, '', ''),
(1314, '1314', 'BRIDAS COLOR NEGRO GRANDE', 'UNIDAD', 'Proveedor General', 'TOP RACING', 1, '2016-12-29 15:48:46', 5, 5, 67, 67, 1, 1, 0, '', ''),
(1315, '6940350802142', 'BRIDAS COLOR BLANCA PEQUENA', 'UNIDAD', 'Proveedor General', 'SNAUZER', 1, '2016-12-29 15:50:30', 1.5, 2, 31, 31, 1, 1, 0, '', ''),
(1316, '051131921047', 'CINTA ADHESIVA ', 'INIDAD', 'Proveedor General', 'TARTAN', 1, '2016-12-29 16:03:14', 48, 48, 30, 30, 1, 1, 0, '', ''),
(1317, '7501892826035', 'CINTA ADHESICA 3/4', 'UNIDAD', 'Proveedor General', 'TOOLCRAFT', 1, '2016-12-29 16:04:04', 25, 25, 10, 10, 1, 1, 0, '', ''),
(1318, '051131921078', 'CINTA ADHESIVA 3/4', 'UNIDAD', 'Proveedor General', 'TARTAN', 1, '2016-12-29 16:05:53', 35, 35, 30, 30, 1, 1, 0, '', ''),
(1319, '7441109001377', 'INTERRUPTOR SUPERFICIAL', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 16:08:34', 20, 20, 50, 50, 1, 1, 0, '', ''),
(1320, '8012199674322', 'INTERRUPTOR DUPLEX 2P + T MONOBLOQUE', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-29 16:11:33', 68, 68, 20, 20, 1, 1, 0, '', ''),
(1321, '7501030842217', 'ROLLO DE REPUESTO PARA PINTAR', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-29 16:17:25', 20, 20, 50, 50, 1, 1, 0, '', ''),
(1322, '7441109000738', 'ENCHUFE PLANO', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 16:20:53', 10, 10, 25, 25, 1, 1, 0, '', ''),
(1323, '7441109000035', 'ENCHUFE REDONDO YUMBO NEGRO', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-29 16:21:42', 18, 18, 25, 25, 1, 1, 0, '', ''),
(1324, '1324', 'CERRADURA DE PELOTA', 'UNIDAD', 'Proveedor General', 'PROFER', 1, '2016-12-29 16:29:35', 160, 160, 12, 12, 1, 1, 0, '', ''),
(1325, '7441118320674', 'ASIENTO REDONDO STANDARD', 'UNIDAD', 'Proveedor General', 'ECOLINE', 1, '2016-12-29 16:31:57', 310, 310, 12, 12, 1, 1, 0, '', ''),
(1326, '7441118320513', 'ASIENTO REDONDO STANDARD BONE', 'UNIDAD', 'Proveedor General', 'ECOLINE', 1, '2016-12-29 16:33:05', 310, 310, 2, 2, 1, 1, 0, '', ''),
(1327, '7702048573036', 'TUBO FLURECENTE 40W', 'UNIDAD', 'Proveedor General', 'SYLVANIA', 1, '2016-12-29 16:35:11', 30, 30, 34, 34, 1, 1, 0, '', ''),
(1328, '7895316667800', 'LIJA DE ESMERIL DE LONA 180', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 16:50:57', 13, 13, 50, 50, 1, 1, 0, '', ''),
(1329, '7895316667770', 'LIJA DE ESMERIL DE LONA 100', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 16:52:19', 13, 13, 50, 50, 1, 1, 0, '', ''),
(1330, '7991792133822', 'LIJA DE ESMERIL DE LONA 60', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 16:53:16', 19, 19, 50, 50, 1, 1, 0, '', ''),
(1331, '7895316667794', 'LIJA DE ESMERIL DE LONA 150', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 16:53:57', 14, 14, 50, 50, 1, 1, 0, '', ''),
(1332, '7895316667732', 'LIJA DE ESMERIL DE LONA 40', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 16:54:41', 24, 24, 50, 50, 1, 1, 0, '', ''),
(1333, '1333', 'LIJA DE AGUA 120', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 16:58:21', 13, 13, 25, 25, 1, 1, 0, '', ''),
(1334, '1334', 'LIJA DE AGUA 220', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 16:59:39', 11, 11, 25, 25, 1, 1, 0, '', ''),
(1335, '1335', 'LIJA DE AGUA 80', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 17:00:15', 14, 14, 25, 25, 1, 1, 0, '', ''),
(1336, '1336', 'LIJA DE AGUA 100', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 17:00:57', 13, 13, 25, 25, 1, 1, 0, '', ''),
(1337, '1337', 'LIJA DE AGUA 60', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 17:01:29', 18, 18, 25, 225, 1, 1, 0, '', ''),
(1338, '1338', 'LIJA DE AGUA 180', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 17:02:15', 11, 11, 25, 25, 1, 1, 0, '', ''),
(1339, '7895316756245', 'LIJA DE ESMERIL DE DISCO 7"', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 17:05:05', 35, 35, 10, 10, 1, 1, 0, '', ''),
(1340, '7895316756238', 'LIJA DE ESMERIL DE DISCO 7"', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-29 17:06:16', 35, 35, 10, 10, 1, 1, 0, '', ''),
(1341, '747752280372', 'ESPATULA TAPIZADORA', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-29 17:08:09', 240, 240, 10, 10, 1, 1, 0, '', ''),
(1342, '091712480891', 'MANGUERA PARA HINODORO ', 'UNIDAD', 'Proveedor General', 'EASTMAN', 1, '2016-12-29 17:11:32', 80, 80, 20, 20, 1, 1, 0, '', ''),
(1343, '091712480044', 'MANGUERA PARA LAVA MANO', 'UNIDAD', 'Proveedor General', 'EASTMAN', 1, '2016-12-29 17:12:19', 75, 75, 20, 20, 1, 1, 0, '', ''),
(1344, '7501206676783', 'CERRADURA DE SOBREPONER', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-30 08:48:01', 700, 700, 1, 1, 1, 1, 0, '', ''),
(1345, '798660016157', 'CERRADURA DE SOBREPONER', 'UNIDAD', 'Proveedor General', 'FANAL', 1, '2016-12-30 08:49:36', 380, 380, 2, 2, 1, 1, 0, '', ''),
(1346, '751299000843', 'CERRADURA DE SOBREPONER DERECHA', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-30 08:53:17', 700, 700, 3, 3, 1, 1, 0, '', ''),
(1347, '751299000829', 'CERRADURA DE SOBREPONER MARI GIL DERECHA', 'UNIDAD', 'Proveedor General', 'PHILIPS', 1, '2016-12-30 08:55:09', 680, 680, 3, 3, 1, 1, 0, '', ''),
(1348, '8012232101457', 'CERRADURA DEBSOBREPONER ', 'UNIDAD', 'Proveedor General', 'YALE', 1, '2016-12-30 08:59:37', 930, 930, 1, 1, 1, 1, 0, '', ''),
(1349, '7417000555516', 'CERRADURA DE SOBREPONER CLASICA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-30 09:03:18', 380, 380, 1, 1, 1, 1, 0, '', ''),
(1350, '7704359089534', 'CERRADURA RESIDENCIAL', 'UNIDAD', 'Proveedor General', 'YALE', 1, '2016-12-30 09:04:34', 400, 400, 1, 1, 1, 1, 0, '', ''),
(1351, 'PD25P10', 'CLAVOS DE IMPACTO 25MM', 'UNIDAD', 'Proveedor General', 'PATMAN', 1, '2016-12-30 09:10:50', 1, 1, 1000, 1000, 1, 1, 0, '', ''),
(1352, '6221116011672', 'CERRADURA DE SOBREPONER', 'UNIDAD', 'Proveedor General', 'IZO', 1, '2016-12-30 09:12:20', 160, 160, 1, 1, 1, 1, 0, '', ''),
(1353, '7704359089800', 'CERRADURA CON LLAVE', 'UNIDAD', 'Proveedor General', 'YALE', 1, '2016-12-30 09:13:58', 300, 300, 12, 12, 1, 1, 0, '', ''),
(1354, '7453050046390', 'CERRADURA REDONDA ', 'UNIDAD', 'Proveedor General', 'RABBIT', 1, '2016-12-30 09:16:20', 158, 158, 3, 3, 1, 1, 0, '', ''),
(1355, '7704359102981', 'CERTADURA DOBLE ', 'UNIDAD', 'Proveedor General', 'YALE', 1, '2016-12-30 09:17:47', 285, 285, 5, 5, 1, 1, 0, '', ''),
(1356, '7453050046444', 'CERRADURA DE PELOTA ', 'UNIDAD', 'Proveedor General', 'RABBIT', 1, '2016-12-30 09:25:55', 180, 180, 3, 3, 1, 1, 0, '', ''),
(1357, '7417000555486', 'CERRADURA DE SOBREPONER DERECHA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-30 09:28:32', 300, 300, 2, 2, 1, 1, 0, '', ''),
(1358, '7417000555509', 'CERTADURA DE SOBREPONER CLASICA DERECHA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-30 09:29:49', 320, 320, 5, 5, 1, 1, 0, '', ''),
(1359, '767705002202', 'cable duple 2x12  blanco', 'metro', 'Proveedor General', 'phelps dodga', 1, '2016-12-30 09:51:19', 25, 25, 300, 300, 1, 1, 0, '', ''),
(1360, '1360', 'CABLE TSJ 3*12 FLEXIBLE', 'METRO', 'Proveedor General', 'AWG', 1, '2016-12-30 10:04:57', 33, 21, 100, 100, 1, 1, 0, '', ''),
(1361, '767705048064', 'ALAMBRE CABLEADO #12 VERDE', 'CAJA', 'Proveedor General', 'AGW', 1, '2016-12-30 10:08:37', 1130, 1130, 4, 4, 1, 1, 0, '', ''),
(1362, '767705047999', 'ALAMBRE CABLEADO #12 AZUL', 'CAJA', 'Proveedor General', 'AGW', 1, '2016-12-30 10:09:28', 1130, 1130, 4, 4, 1, 1, 0, '', ''),
(1364, '4701163048057', 'ALAMBRE CABLEADO #12 ROJO', 'CAJA', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 10:15:42', 1130, 1130, 4, 4, 1, 1, 0, '', ''),
(1365, '2318738047906', 'ALAMBRE CABLEADO #10 NEGRO', 'CAJA', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 10:16:37', 1800, 1241, 2, 2, 1, 1, 0, '', ''),
(1366, '767705048040', 'ALAMBRE CABLEADO #12 NEGRO', 'CAJA', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 10:18:28', 1130, 1130, 3, 3, 1, 1, 0, '', ''),
(1367, '767705003001', 'ALAMBRE DUPLEX #2*18 ', 'CAJA', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 10:20:04', 9, 6, 30, 300, 1, 1, 0, '', ''),
(1368, '767705002721', 'ALAMBRE DUPLEX #2*16', 'METRO', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 10:21:17', 12, 8, 300, 300, 1, 1, 0, '', ''),
(1369, '3765265048026', 'ALAMBRE CABLEADO #12 BLANCO', 'CAJA', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 10:22:50', 1130, 1130, 4, 4, 1, 1, 0, '', ''),
(1370, '1370', 'ALAMBRE CABLEADO #8 NEGRO ', 'CAJA', 'Proveedor General', 'AWG', 1, '2016-12-30 10:42:13', 30, 20, 100, 100, 1, 1, 0, '', ''),
(1371, '6925059790587', 'LLAVE DE PASE PVC 1"', 'UNIDAD', 'Proveedor General', 'PROFER', 1, '2016-12-30 10:49:57', 45, 27, 25, 25, 1, 1, 0, '', ''),
(1372, '6925059790600', 'LLAVE DE PASE PVC 1-1/2"', 'UNIDAD', 'Proveedor General', 'PROFER', 1, '2016-12-30 10:52:36', 70, 50, 12, 12, 1, 1, 0, '', ''),
(1373, '767705047937', 'ALAMBRE CABLEADO #10 ROJO', 'CAJA', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 10:55:51', 1800, 1241, 2, 2, 1, 1, 0, '', ''),
(1374, '1374', 'ALAMBRE DUPLEX 2*12', 'UNIDAD', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 11:05:15', 2417, 1752, 3, 3, 1, 1, 0, '', ''),
(1375, '6925059790570', 'LLAVE DE PASE PVC 3/4"', 'UNIDAD', 'Proveedor General', 'PRUFER', 1, '2016-12-30 11:14:39', 28, 28, 25, 25, 1, 1, 0, '', ''),
(1376, '6925059790617', 'LLAVE DE PASE PVC 2"', 'UNIDAD', 'Proveedor General', 'PRUFER', 1, '2016-12-30 11:16:01', 130, 130, 12, 12, 1, 1, 0, '', ''),
(1377, '7501973707024', 'LLAVE DE CHORRO 1/2"', 'UNIDAD', 'Proveedor General', 'DICA', 1, '2016-12-30 11:17:54', 100, 100, 24, 24, 1, 1, 0, '', ''),
(1378, '3253562306265', 'CINTA METRICA 8MTR', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-30 11:20:53', 280, 280, 6, 6, 1, 1, 0, '', ''),
(1379, '3253562306159', 'CINTA METRICA 5MTR', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-30 11:21:30', 165, 165, 6, 6, 1, 1, 0, '', ''),
(1380, '3253562306081', 'CINTA METRICA 3M', 'UNIDAD', 'Proveedor General', 'STANLEY', 1, '2016-12-30 11:22:00', 100, 100, 6, 6, 1, 1, 0, '', ''),
(1381, '7501030802020', 'BROCHA 2" SERIE 500', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-30 11:27:56', 25, 18, 24, 24, 1, 1, 0, '', ''),
(1382, '7501030802051', 'BROCHA 4" SERIE 500', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-30 11:28:56', 60, 44, 24, 24, 1, 1, 0, '', ''),
(1383, '7501030802044', 'BROCHA 3" SERIE 500', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-30 11:30:14', 36, 26, 24, 24, 1, 1, 0, '', ''),
(1384, '7501030802013', 'BROCHA 1-1/2" SERIE 500', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-30 11:31:16', 18, 13, 24, 24, 1, 1, 0, '', ''),
(1385, '7501973706591', 'LLAVE DE EMPOTRAR ROSCABLE', 'UNIDAD', 'Proveedor General', 'DICA', 1, '2016-12-30 11:35:12', 255, 177, 12, 12, 1, 1, 0, '', ''),
(1386, '7460104505512', 'LLAVE DE PANTRI CUELLO DE GANSO', 'UNIDAD', 'Proveedor General', 'SENDOF', 1, '2016-12-30 11:37:24', 395, 283, 6, 6, 1, 1, 0, '', ''),
(1387, '7895316756207', 'LIJA DE ESMERIL DE DISCO 7"', 'UNIDAD', 'Proveedor General', 'NORTON', 1, '2016-12-30 11:39:34', 35, 35, 10, 10, 1, 1, 0, '', ''),
(1388, '1388', 'ALAMBRE CABLEADO #6', 'METRO', 'Proveedor General', 'ECOPLUS', 1, '2016-12-30 11:46:18', 45, 31, 100, 100, 1, 1, 0, '', ''),
(1389, '786689003432', 'CENTRO DE CARGA 12 ESPACIOS', 'CAJA', 'Proveedor General', 'EATON', 1, '2016-12-30 11:51:08', 2660, 2211, 1, 1, 1, 1, 0, '', ''),
(1390, '786689003975', 'CENTRO DE CARGA DE 8 ESPACIOS', 'CAJA', 'Proveedor General', 'EATON', 1, '2016-12-30 11:55:07', 1630, 1171, 2, 2, 1, 1, 0, '', ''),
(1391, '786689003470', 'CENTRO DE MESA 16 ESACIOS', 'CAJA', 'Proveedor General', 'EATON', 1, '2016-12-30 11:59:16', 3050, 2270, 1, 1, 1, 1, 0, '', ''),
(1392, '7501030838043', 'MANERAL CON FELPA 4"', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-30 12:06:48', 40, 29, 25, 25, 1, 1, 0, '', ''),
(1393, '7501030838098', 'MANERAL CON FELPA 9"', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-30 12:07:51', 50, 34, 20, 20, 1, 1, 0, '', ''),
(1394, '1394', 'MANERAL PARA FELPA 9"', 'UNIDAD', 'Proveedor General', 'PERFECT', 1, '2016-12-30 12:09:01', 55, 36, 25, 25, 1, 1, 0, '', ''),
(1396, '028877321837', 'DISCO PARA METAL 7"*.045*7/8', 'UNIDAD', 'Proveedor General', 'DEWALT', 1, '2016-12-30 12:16:26', 75, 56, 25, 25, 1, 1, 0, '', ''),
(1397, '1397', 'PLIWOOD 3/16*4*8', 'UNIDAD', 'Proveedor General', 'PLYWOOD', 1, '2016-12-30 12:21:26', 330, 260, 20, 20, 1, 1, 0, '', ''),
(1398, '1398', 'PUERTA FIBRAN DE 6 TABLEROS 0.70*2.10', 'UNIDAD', 'Proveedor General', 'N/T', 1, '2016-12-30 12:24:16', 930, 661, 4, 4, 1, 1, 0, '', ''),
(1399, '1399', 'PUERTA FIBRAN 6 TABLERO 0.80*2.10', 'UNIDAD', 'Proveedor General', 'N/T', 1, '2016-12-30 12:25:36', 930, 661, 4, 4, 1, 1, 0, '', ''),
(1400, '1400', 'PUERTA FIBRAN DE 6 TABLEROS 0,90*210', 'UNIDAD', 'Proveedor General', 'N/T', 1, '2016-12-30 12:26:31', 930, 661, 4, 4, 1, 1, 0, '', ''),
(1401, '8012199674148', 'DOBLE INTERRUPTOR MONOBLOQUE', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-30 12:30:40', 95, 70, 20, 20, 1, 1, 0, '', ''),
(1402, '8012199674186', 'TRIPLE INTERRUPTOR ', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-30 12:34:09', 130, 96, 12, 12, 1, 1, 0, '', ''),
(1403, '6946852311168', 'espray rojo susuki', 'unidad  ', 'Proveedor General', 'AC', 1, '2016-12-31 07:32:16', 52, 43, 12, 12, 1, 1, 0, '', ''),
(1404, '1404', 'espray negro brillante', 'unidad', 'Proveedor General', 'AC', 1, '2016-12-31 07:44:47', 52, 43, 12, 12, 1, 1, 0, '', ''),
(1405, '1405', 'espray blanco brillante', 'unidad', 'Proveedor General', 'AC', 1, '2016-12-31 07:47:20', 52, 43, 6, 6, 1, 1, 0, '', ''),
(1406, '1406', 'espray celeste ', 'UNIDAD', 'Proveedor General', 'AC ', 1, '2016-12-31 07:49:56', 52, 43, 6, 6, 1, 1, 0, '', ''),
(1407, '1407', 'ESPRAY AZUL TORNER', 'UNIDAD', 'Proveedor General', 'AC', 1, '2016-12-31 07:51:59', 52, 43, 8, 8, 1, 1, 0, '', ''),
(1408, '1408', 'ESPRAY AMARILLO LIMON ', 'UNIDAD', 'Proveedor General', 'AC', 1, '2016-12-31 07:54:13', 52, 43, 4, 4, 1, 1, 0, '', ''),
(1409, '1409', 'ESPRAY BASE GRIS', 'UNIDAD', 'Proveedor General', 'AC', 1, '2016-12-31 08:00:13', 52, 43, 9, 9, 1, 1, 0, '', ''),
(1410, '7441109010966', 'CEPO PLATO AGUILA', 'UNIDAD', 'Proveedor General', 'EAGLE', 1, '2016-12-31 08:02:41', 20, 13, 50, 50, 1, 1, 0, '', ''),
(1411, '8012199645889', 'TOMA CORRIENTE SUPERFICIAL ', 'UNIDAD', 'Proveedor General', 'BTICINO', 1, '2016-12-31 08:06:58', 55, 39, 20, 20, 1, 1, 0, '', ''),
(1412, '1412', 'ESPRAY VIOLETA OSCURO', 'UNIDAD', 'Proveedor General', 'AC', 1, '2016-12-31 08:08:18', 52, 43, 6, 6, 1, 1, 0, '', ''),
(1413, '1413', 'ESPRAY VIOLETA FLORECENTE', 'UNIDAD', 'Proveedor General', 'AC', 1, '2016-12-31 08:09:00', 80, 63, 6, 6, 1, 1, 0, '', ''),
(1425, '1425', 'SPRAY VERDE FLOURECENTE', 'UNIDAD', 'Proveedor General', 'AC', 1, '2016-12-31 09:13:41', 63, 63, 6, 6, 1, 1, 0, '', ''),
(1424, '1424', 'ESPRAY CROMO', 'UNIDAD', 'Proveedor General', 'AC', 1, '2016-12-31 09:13:16', 63, 63, 12, 12, 1, 1, 0, '', ''),
(1417, '6925059790693', 'GRAPITA AISLANTE PARA CABLE # 10MM', 'BOLSA', 'Proveedor General', 'PROFER', 1, '2016-12-31 08:35:04', 35, 23, 6, 6, 1, 1, 0, '', ''),
(1418, '6925059791683', 'GRAPA AISLANTE PARA CABLE# 9MM', 'BOLSA', 'Proveedor General', 'PROFER', 1, '2016-12-31 08:37:54', 28, 21, 6, 6, 1, 1, 0, '', ''),
(1426, '7501892821078', 'FARON DE PARED TIPO COLONIAL', 'UNIDAD', 'Proveedor General', 'FULGORE', 1, '2016-12-31 09:19:48', 520, 520, 3, 3, 1, 1, 0, '', ''),
(1420, '7501892815336', 'FAROL DE PARED CLASICO BLANCO FUO0670', 'UNIDAD', 'Proveedor General', 'FULGORE', 1, '2016-12-31 08:48:22', 300, 216, 6, 6, 1, 1, 0, '', ''),
(1421, '1421', 'CLAVO DE IMPACTO RANSET 1', 'CAJA DE 100', 'Proveedor General', 'RANSET', 1, '2016-12-31 08:55:15', 60, 39, 10, 10, 1, 1, 0, '', ''),
(1422, '1422', 'CLAVO DE IMPACTO RANSET 1', 'CAJA DE 100', 'Proveedor General', 'RANSET', 1, '2016-12-31 08:55:15', 60, 39, 10, 10, 1, 1, 0, '', ''),
(1423, '1423', 'BRIDA EMT UN HOYO 1/2', 'UNIDAD', 'Proveedor General', 'ONE JOLE', 1, '2016-12-31 08:58:20', 2.5, 1, 300, 300, 1, 1, 0, '', ''),
(1427, '9456732116419', 'SERRADURA DE PARCHE', 'UNIDAD', 'Proveedor General', 'TALTOOOS', 1, '2016-12-31 09:31:09', 200, 200, 5, 5, 1, 1, 0, '', ''),
(1428, '7460104505642', 'LLAVE DE LAVA MANOS', 'UNIDAD', 'Proveedor General', 'SANDOF', 1, '2016-12-31 09:33:04', 215, 215, 6, 6, 1, 1, 0, '', ''),
(1429, '7460104505635', 'LLAVE DE LAVA MANO', 'UNIDAD', 'Proveedor General', 'SANDOF', 1, '2016-12-31 09:33:39', 155, 155, 6, 6, 1, 1, 0, '', ''),
(1431, '1431', 'SUPER BONDER  DE 20 KGS', 'BOLSA', 'Proveedor General', 'SUPER BOND', 1, '2016-12-31 09:42:05', 130, 98, 150, 150, 1, 1, 0, '', ''),
(1432, '7506240609452', 'BISAGRA BIDIMENCIONAL', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-31 09:43:31', 37, 37, 15, 15, 1, 1, 0, '', ''),
(1434, '070963137952', 'PICAPORTE DE PIE 5 PLG', 'UNIDAD', 'Proveedor General', 'BOXER', 1, '2016-12-31 09:47:52', 75, 75, 3, 3, 1, 1, 0, '', ''),
(1435, '7453001132035', 'BISAGRA DE PRECION', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:00:00', 30, 30, 2, 2, 1, 1, 0, '', ''),
(1436, '7453001130192', 'BISAGRA ESCONDIDA', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:01:47', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1437, '7453001110088', 'SERRADURA PARA MUEBLE', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:02:28', 60, 60, 1, 1, 1, 1, 0, '', ''),
(1438, '1438', 'SERRUDA  PARA PUERTA', 'UNIDAD', 'Proveedor General', 'CHAVEZ TOOLS', 1, '2016-12-31 10:03:26', 160, 160, 1, 1, 1, 1, 0, '', ''),
(1439, '5180003152069', 'PASADOR DE PUERTA 6 PULG', 'UNIDAD', 'Proveedor General', 'TALTOOOS', 1, '2016-12-31 10:04:40', 95, 95, 2, 2, 1, 1, 0, '', ''),
(1440, '7501206681435', 'SERRADURA PARA MUEBLE', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-31 10:06:35', 95, 95, 6, 6, 1, 1, 0, '', ''),
(1441, '7501206681305', 'SERRADURA PARA MUEBLE', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-31 10:08:13', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1442, '7501206680896', 'GANCHO PARA PUERTA', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-31 10:08:53', 130, 130, 1, 1, 1, 1, 0, '', ''),
(1443, '7501206681299', 'SERRADUDA PARA VITRINA', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-31 10:09:34', 110, 110, 4, 4, 1, 1, 0, '', ''),
(1444, '7453010006358', 'SERRADURA DE PELOTA MANGO DE MADERA', 'UNIDAD', 'Proveedor General', 'SEGURITY', 1, '2016-12-31 10:10:40', 140, 140, 1, 1, 1, 1, 0, '', ''),
(1445, '7450077018607', 'PASADOR DE BARRA\r\n', 'UNIDAD', 'Proveedor General', 'SCAN BRIK', 1, '2016-12-31 10:11:46', 22, 22, 1, 1, 1, 1, 0, '', ''),
(1446, '7453001130604', 'BISAGRA DECORATIVA 3 1/2', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:12:48', 45, 45, 2, 2, 1, 1, 0, '', ''),
(1447, '7453050012197', 'SERRADURA', 'UNIDAD', 'Proveedor General', 'RABBIT', 1, '2016-12-31 10:17:21', 420, 420, 1, 1, 1, 1, 0, '', ''),
(1448, '7501206626177', 'REBAJADORA', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-31 10:18:27', 3700, 3700, 1, 1, 1, 1, 0, '', ''),
(1449, '7501206626856', 'ROTO MARTILLO PEGADO', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-31 10:19:17', 2380, 2380, 2, 2, 1, 1, 0, '', ''),
(1450, '7506240604433', 'ROTOMARTILLO', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-31 10:21:14', 1000, 1000, 2, 2, 1, 1, 0, '', ''),
(1451, '7453001116769', 'HALOJENO 100 W ROJO', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:22:48', 162, 162, 5, 5, 1, 1, 0, '', ''),
(1452, '7453001116783', 'HALOJENO DE 100 W AMARILLO', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:23:46', 162, 162, 3, 3, 1, 1, 0, '', ''),
(1453, '886316031439', 'LAMPARA DE PARED', 'UNIDAD', 'Proveedor General', 'LITE', 1, '2016-12-31 10:24:42', 450, 450, 1, 1, 1, 1, 0, '', ''),
(1454, '7453042898907', 'LAMPARA EMPOTRABLE', 'UNIDAD', 'Proveedor General', 'TRAIN', 1, '2016-12-31 10:26:00', 75, 75, 4, 4, 1, 1, 0, '', ''),
(1455, '886316002774', 'LAMPARA EMPOTRABLE', 'UNIDAD', 'Proveedor General', 'GENERAL', 1, '2016-12-31 10:29:41', 200, 200, 5, 5, 1, 1, 0, '', ''),
(1456, '886316002767', 'LAMPARA EMPOTRABLE', 'UNIDAD', 'Proveedor General', 'GENERAL', 1, '2016-12-31 10:30:26', 200, 200, 5, 5, 1, 1, 0, '', ''),
(1457, '7453001116752', 'HALOJENA DE 100 W CLARO', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:33:34', 150, 150, 3, 3, 1, 1, 0, '', ''),
(1458, '7453001116820', 'HALOGENO DE 100 W VERDE', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:34:23', 160, 160, 1, 1, 1, 1, 0, '', ''),
(1459, '7453042898556', 'LAMPARA EMPOTRABLE', 'UNIDAD', 'Proveedor General', 'TRAIN', 1, '2016-12-31 10:35:01', 85, 85, 5, 5, 1, 1, 0, '', ''),
(1460, '7453001133186', 'TIRADOR PARA PUERTA', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:37:19', 185, 185, 2, 2, 1, 1, 0, '', ''),
(1461, '7453001111207', 'SERRADURA DECORATIVA', 'UNIDAD', 'Proveedor General', 'BEST VALUE', 1, '2016-12-31 10:38:30', 1300, 1300, 1, 1, 1, 1, 0, '', ''),
(1462, '7501206676004', 'MANIJA DECORATIVA', 'UNIDAD', 'Proveedor General', 'HERMEX ', 1, '2016-12-31 10:39:28', 200, 200, 2, 2, 1, 1, 0, '', ''),
(1463, '7506240637011', 'BRAZO Y CHAPETON DE REPUESTO', 'UNIDAD', 'Proveedor General', 'FOSET', 1, '2016-12-31 10:40:40', 165, 165, 3, 3, 1, 1, 0, '', ''),
(1464, '1464', 'BRAZO DE REGADERA', 'UNIDAD', 'Proveedor General', 'N/T', 1, '2016-12-31 10:41:54', 30, 30, 6, 6, 1, 1, 0, '', ''),
(1465, '711774007811', 'BRAZO Y CHAPETON DE REGADERA', 'UNIDAD', 'Proveedor General', 'RUGO', 1, '2016-12-31 10:42:29', 50, 50, 1, 1, 1, 1, 0, '', ''),
(1466, '6925059790914', 'HILO DE NYLON 60 LBRS', 'UNIDAD', 'Proveedor General', 'PROFER', 1, '2016-12-31 10:44:52', 45, 45, 8, 8, 1, 1, 0, '', ''),
(1467, '6925059790907', 'HILO  DE NILON 50 LBRS', 'UNIDAD', 'Proveedor General', 'PROFER', 1, '2016-12-31 10:45:41', 40, 40, 3, 3, 1, 1, 0, '', ''),
(1468, '6925059790891', 'HILO DE NYLON', 'UNIDAD', 'Proveedor General', 'PROFER', 1, '2016-12-31 10:46:25', 38, 38, 10, 10, 1, 1, 0, '', ''),
(1469, '6925059792086', 'CINTA MALLA PARA GYTSON', 'UNIDAD', 'Proveedor General', 'PROFER', 1, '2016-12-31 10:48:10', 80, 80, 3, 3, 1, 1, 0, '', ''),
(1470, '7417000500424', 'PISTOLA PLASTICA PARA MANGUERA', 'UNIDAD', 'Proveedor General', 'IMACASA', 1, '2016-12-31 10:54:56', 55, 55, 5, 5, 1, 1, 0, '', ''),
(1471, '1471', 'PISTOLA DE METAL PARA MANGUERA', 'UNIDAD', 'Proveedor General', 'N/T', 1, '2016-12-31 10:56:04', 60, 60, 10, 10, 1, 1, 0, '', ''),
(1472, '1472', 'PISTOLA DE METAL GRANDE PARA MANGUERA', 'UNIDAD', 'Proveedor General', 'N/T', 1, '2016-12-31 10:56:45', 98, 98, 1, 1, 1, 1, 0, '', ''),
(1473, '1473', 'LLAVE PARA LAVA MANO NIQUELADAS', 'UNIDAD', 'Proveedor General', 'W', 1, '2016-12-31 10:58:55', 110, 110, 2, 2, 1, 1, 0, '', ''),
(1474, '1474', 'LLAVE DE LAVA MANO SENCILLA ANIQUELADA', 'UNIDAD', 'Proveedor General', 'W', 1, '2016-12-31 10:59:45', 146, 146, 3, 3, 1, 1, 0, '', ''),
(1475, '1475', 'LLAVE PARA LAVA MANO SENCILLA ANIQUELADA PEQUEÃ‘A', 'UNIDAD', 'Proveedor General', 'W', 1, '2016-12-31 11:00:32', 140, 140, 3, 3, 1, 1, 0, '', ''),
(1476, '1476', 'LLAVE PARA LAVAMANO DE COLOR ', 'UNIDAD', 'Proveedor General', 'B&G', 1, '2016-12-31 11:01:17', 100, 100, 5, 5, 1, 1, 0, '', ''),
(1477, '1477', 'LLAVE DE PLASTICO COLOR BLANCO', 'UNIDAD', 'Proveedor General', 'KOLON', 1, '2016-12-31 11:03:40', 100, 100, 2, 2, 1, 1, 0, '', ''),
(1478, '1478', 'LLAVE DE CHORO DE  LAVAMANO', 'UNIDAD', 'Proveedor General', 'CHROMA', 1, '2016-12-31 11:09:11', 1, 1, 3, 3, 1, 1, 0, '', ''),
(1479, '1479', 'LLAVE DE CHORO DE LAVAMANO', 'UNIDAD', 'Proveedor General', 'POLO', 1, '2016-12-31 11:09:51', 1, 1, 1, 1, 1, 1, 0, '', ''),
(1480, '7501206695289', 'DISCO DE LIJA DE 7 PULGS', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-31 11:12:31', 35, 35, 5, 5, 1, 1, 0, '', ''),
(1481, '7501206695296', 'DISCO DE LIJA DE 7 PULG GRANO 80', 'UNIDAD', 'Proveedor General', 'TRUPER', 1, '2016-12-31 11:14:49', 35, 35, 5, 5, 1, 1, 0, '', ''),
(1482, '1482', 'ALADERA PARA PUERTA ', 'UNIDAD', 'Proveedor General', 'HARDY', 1, '2016-12-31 11:22:29', 24, 24, 1, 1, 1, 1, 0, '', ''),
(1483, '1483', 'ALADERA PARA PUERTA', 'UNIDAD', 'Proveedor General', 'ZBW', 1, '2016-12-31 11:23:15', 22, 22, 3, 3, 1, 1, 0, '', ''),
(1484, '1484', 'ALADERA PARA  PUERTA ROJA', 'UNIDAD', 'Proveedor General', 'ZBW', 1, '2016-12-31 11:25:17', 25, 25, 7, 7, 1, 1, 0, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `cod_prov` int(11) NOT NULL,
  `cedula` varchar(25) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`cod_prov`, `cedula`, `nombre`, `direccion`, `telefono`, `celular`, `email`, `estado`) VALUES
(1, '2810302910010x', 'Proveedor General', 'El Sauce', '', '12345678', '', 1),
(2, 'gyybjnjknjniii99o', 'ferreteria jenny', 'Leon', '', '12345678', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE `tmp` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8,2) DEFAULT NULL,
  `descuento_tmp` float NOT NULL,
  `iva_tmp` float NOT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_compra`
--

CREATE TABLE `tmp_compra` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double NOT NULL,
  `session_id` varchar(100) NOT NULL,
  `cod_prov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_cotizacion`
--

CREATE TABLE `tmp_cotizacion` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad_tmp` int(11) NOT NULL,
  `precio_tmp` double(8,2) DEFAULT NULL,
  `descuento_tmp` float NOT NULL,
  `iva_tmp` float NOT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_medida`
--

CREATE TABLE `unidad_medida` (
  `id_medida` int(11) NOT NULL,
  `nombre_medida` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidad_medida`
--

INSERT INTO `unidad_medida` (`id_medida`, `nombre_medida`) VALUES
(2, 'Galon'),
(3, 'CUARTO'),
(4, '1/8'),
(5, '1/16'),
(6, 'UNIDAD'),
(7, 'CUBETA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL COMMENT 'auto incrementing user_id of each user, unique index',
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `date_added` datetime NOT NULL,
  `permiso_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `user_name`, `user_password_hash`, `user_email`, `date_added`, `permiso_user`) VALUES
(1, 'Jorge', 'Castillo', 'admin', '$2y$10$SS6MCqM7XMzya86JcXjKSOZaxYYGVJW6dYlaGka/xup2M8IRaIcUK', 'admin@admin.com', '2016-05-21 15:06:00', 1),
(2, 'Antonio', 'Moreno', 'Antonio', '$2y$10$uObwQIkIsD0jsI2VRyXYoOf8hp8RmE8uw5XQmQLpUDnEHMCL2dtn2', 'antonio@gmail.com', '2016-09-05 15:41:18', 2),
(3, 'FERRETERIA', 'EL ESFUERZO', 'VENDEDOR', '$2y$10$MeA091YuJ6Zs7wdBF/XrSOgvL2GV3HyAY3efLBSNxIsS5/TAq31WC', 'vendedor@controltotal.com', '2016-12-18 10:01:16', 2),
(4, 'Administrador', 'EL ESFUERZO', 'admon', '$2y$10$jqguraiY3PHmZ1rUx4u0n.QPRuFXRL62iL5j/aLLB2iSi/Cbkz/9m', 'ferreteriaelesfuerzo@outlook.es', '2016-12-18 11:35:52', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `codigo_producto` (`nombre_cliente`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`cod_compra`);

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- Indices de la tabla `cuentas_cobrar`
--
ALTER TABLE `cuentas_cobrar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indices de la tabla `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indices de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `numero_cotizacion` (`numero_factura`,`id_producto`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id_factura`),
  ADD UNIQUE KEY `numero_cotizacion` (`numero_factura`);

--
-- Indices de la tabla `kardex`
--
ALTER TABLE `kardex`
  ADD PRIMARY KEY (`cod_kardex`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permisos`),
  ADD KEY `permisos` (`permisos`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_producto`),
  ADD UNIQUE KEY `codigo_producto` (`codigo_producto`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`cod_prov`);

--
-- Indices de la tabla `tmp`
--
ALTER TABLE `tmp`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `tmp_compra`
--
ALTER TABLE `tmp_compra`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `tmp_cotizacion`
--
ALTER TABLE `tmp_cotizacion`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  ADD PRIMARY KEY (`id_medida`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD KEY `permiso_user` (`permiso_user`),
  ADD KEY `permiso_user_2` (`permiso_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `cod_compra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cuentas_cobrar`
--
ALTER TABLE `cuentas_cobrar`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;
--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT de la tabla `kardex`
--
ALTER TABLE `kardex`
  MODIFY `cod_kardex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1485;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `cod_prov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tmp`
--
ALTER TABLE `tmp`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1049;
--
-- AUTO_INCREMENT de la tabla `tmp_compra`
--
ALTER TABLE `tmp_compra`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tmp_cotizacion`
--
ALTER TABLE `tmp_cotizacion`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=858;
--
-- AUTO_INCREMENT de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  MODIFY `id_medida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index', AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
