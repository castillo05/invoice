<?php
  
  session_start();
  if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
    exit;
        }
  $active_facturas="active";
  $active_productos="";
  $active_clientes="";
  $active_usuarios="";  
  $active_reportes="";
  $active_reportes_fecha="";
  $title="Nueva Factura | Simple Invoice";
  
  /* Connect To Database*/
  require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
  require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php");?>
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
  <?php
  include("navbar2.php");
  include("modal/buscar_productos.php");
  ?> 
  
  <section class="content-wrapper">
    
   
    <div class="content">
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4><i class='glyphicon glyphicon-edit'><a href="javascript:location.reload()"></i> Nueva Compra</a></h4>
      
    </div>
    <div class="panel-body">
   
      <form class="form-horizontal" role="form" id="datos_compra">
      <div id="resultados_ajax_productos"></div>
        <div class="form-group row">
          <label for="nombre_cliente" class="col-md-1 control-label">Proveedor</label>
          <div class="col-md-3">
            <input type="text" class="form-control input-sm" name="nombre_proveedor" id="nombre_proveedor" placeholder="Selecciona un Proveedor" required>
            <input id="cod_prov" name="cod_prov" type='hidden'> 
          </div>
          
         </div>


         <div class="form-group">
           
           <label for="nombre_cliente" class="col-md-1 control-label">Fecha Compra</label>
         
            <div class='input-group date col-md-3' id='divMiCalendario'>
                          <input type='text' name="fecha" id="fecha" placeholder="Fecha de Compra" class="form-control input-sm" onkeyup='load(1);'/>
                          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
          
         </div>

         <div class="form-group">
           <label for="nombre_cliente" class="col-md-1 control-label">Factura #</label>
          <div class="col-md-3">
            <input type="text" class="form-control input-sm" name="num_factura" id="num_factura" placeholder="Numero de Factura" required>
           
          </div>
         </div>

         <div class="form-group">
           <label for="nombre_cliente" class="col-md-1 control-label">Condiciones</label>
           <div class="col-md-3">
             <select name="estado" id="estado" class="form-control input-sm">
            <option value="1">Contado</option>
            <option value="2">Credito 15 dias</option>
            <option value="3">Credito 30 dias</option>         
          </select>
           </div>
          
         </div>
          
        <!-- <div class="form-group">
           
           <label for="nombre_cliente" class="col-md-1 control-label">Vencimiento</label>
         
            <div class='input-group date col-md-3' id='divMiCalendario2'>
                          <input type='text' id="fecha_vencimiento" name="fecha_vencimiento" placeholder="Fecha de Compra" class="form-control input-sm" onkeyup='load(1);'/>
                          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
          
         </div>   -->

           <div class="form-group">
           <label for="nombre_cliente" class="col-md-1 control-label">Estado</label>
           <div class="col-md-3">
             <select name="condiciones" id="condiciones" class="form-control input-sm">
            <option value="4">Cancelada</option>
            <option value="5">Pendiente</option>
            <option value="6">Vencida</option>          
          </select>
           </div>
          
         </div>
               
        
        
        <div class="col-md-12">
          <div class="pull-right">
            
            
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
             <span class="glyphicon glyphicon-search"></span> Agregar productos
            </button>
            <button type="submit" class="btn btn-default" id="guardar_datos">
             <span class="glyphicon glyphicon-search"></span>Guardar Datos
            </button>
            
          </div>  
        </div>
      </form> 
      

    <div id="resultados" class='col-md-12' style="margin-top:10px"></div><!-- Carga los datos ajax -->      
    </div>
  </div>    
      
  </div>
  </section>
  <?php
  include("footer.php");
  ?>
  <script type="text/javascript" src="js/VentanaCentrada.js"></script>
  <script type="text/javascript" src="js/nueva_compra.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="jquery-ui-themes-1.12.0/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="bootstrap/js/moment.min.js"></script>
   <script src="bootstrap/js/bootstrap-datetimepicker.min.js"></script>
   <script src="bootstrap/js/bootstrap-datetimepicker.es.js"></script>
    <script type="text/javascript">
     $('#divMiCalendario').datetimepicker({
          format: 'YYYY-MM-DD'      
      });
      

      $('#divMiCalendario2').datetimepicker({
          format: 'YYYY-MM-DD'       
      });
      
   </script>
 <script>

 $(document).ready(function() {
    $("form").keypress(function(e) {
        if (e.which == 13) {
            return false;
        }
    });
});
    $(function() {
            $("#nombre_proveedor").autocomplete({
              source: "./ajax/autocomplete/proveedor.php",
              minLength: 2,
              select: function(event, ui) {
                event.preventDefault();
                $('#cod_prov').val(ui.item.cod_prov);
                $('#nombre_proveedor').val(ui.item.nombre);
                // $('#tel1').val(ui.item.telefono_cliente);
                // $('#mail').val(ui.item.email_cliente);
                                
                
               }
            });
             
            
          });
          
  $("#nombre_proveedor" ).on( "keydown", function( event ) {
            if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
            {
              $("#id_cliente" ).val("");
              $("#tel1" ).val("");
              $("#mail" ).val("");
                      
            }
            if (event.keyCode==$.ui.keyCode.DELETE){
              $("#nombre_cliente" ).val("");
              $("#id_cliente" ).val("");
              $("#tel1" ).val("");
              $("#mail" ).val("");
            }
      }); 
  </script>



   <script>
$( "#datos_compra" ).submit(function( event ) {
  $('#guardar_datos').attr("disabled", true);
  
 var parametros = $(this).serialize();
   $.ajax({
      type: "POST",
      url: "ajax/guardar_compra.php",
      data: parametros,
       beforeSend: function(objeto){
        $("#resultados_ajax_productos").html("Mensaje: Cargando...");
        },
      success: function(datos){
      $("#resultados_ajax_productos").html(datos);
      $('#guardar_datos').attr("disabled", false);
      load(1);
      }
  });
  event.preventDefault();
})

</script>

  </body>
</html>