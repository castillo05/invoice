<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] != 1 && !isset($_SESSION['permiso_user']) && $_SESSION['permiso_user'] != 1) {
        header("location: login.php");
		exit;
        }
        // elseif ($_SESSION['permiso_user'] == 2) {
        // 	header("location: accesorestringido.php");
        // }elseif($_SESSION['permiso_user'] == 1){


        		/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_facturas="";
	$active_productos="active";
	$active_clientes="";
	$active_usuarios="";	
	$active_reportes="";
	$active_reportes_fecha="";
	$title="Productos | Control Total";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php");?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
	<?php
	include("navbar2.php");
	?>
	<div class="content-wrapper">
   
	<div class="panel panel-info">
		<div class="panel-heading">
		    <div class="btn-group pull-right">
				<!-- <button type='button' class="btn btn-info" data-toggle="modal" data-target="#nuevoProducto"><span class="glyphicon glyphicon-plus" ></span> Nuevo Producto</button> -->
			</div>

			<h4><i class='glyphicon glyphicon-search'></i> Buscar Productos</h4>
		</div>
		<div class="panel-body">
		
			
			
			<?php
			include("modal/registro_productos.php");
			include("modal/editar_productos.php");
			?>
			<form class="form-horizontal" role="form" id="datos_cotizacion">
				
						<div class="form-group row">
							<label for="q" class="col-md-2 control-label">Código o nombre</label>
							<div class="col-md-5">
								<input type="text" class="form-control" id="q" placeholder="Código o nombre del producto" onkeyup='load(1);'>
							</div>
							<div class="col-md-3">
								<button type="button" class="btn btn-default" onclick='load(1);'>
									<span class="glyphicon glyphicon-search" ></span> Buscar</button>

								
								<a class="btn btn-default" href="ajax/exportar_excel.php"><span class="glyphicon glyphicon-search" ></span> Exporta a Excel</a>
								<span id="loader"></span>
							</div>
							
						</div>
				
				
				
			</form>
				<div id="resultados"></div><!-- Carga los datos ajax -->
				<div class='outer_div'></div><!-- Carga los datos ajax -->
			
		
	
			
			
			
  </div>
</div>
		 
	
	

	</div>
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/productos.js"></script>
  </body>
</html>
<script>
/*$( "#guardar_producto" ).submit(function( event ) {
  $('#guardar_datos').attr("disabled", true);
  
 var parametros = $(this).serialize();
	 $.ajax({
			type: "POST",
			url: "ajax/nuevo_producto.php",
			data: parametros,
			 beforeSend: function(objeto){
				$("#resultados_ajax_productos").html("Mensaje: Cargando...");
			  },
			success: function(datos){
			$("#resultados_ajax_productos").html(datos);
			$('#guardar_datos').attr("disabled", false);
			load(1);
		  }
	});
  event.preventDefault();
})*/

$( "#editar_producto" ).submit(function( event ) {
  $('#actualizar_datos').attr("disabled", true);
  
 var parametros = $(this).serialize();
	 $.ajax({
			type: "POST",
			url: "ajax/editar_producto.php",
			data: parametros,
			 beforeSend: function(objeto){
				$("#resultados_ajax2").html("Mensaje: Cargando...");
			  },
			success: function(datos){
			$("#resultados_ajax2").html(datos);
			$('#actualizar_datos').attr("disabled", false);
			load(1);
		  }
	});
  event.preventDefault();
})

	function obtener_datos(id){
			var codigo_producto = $("#codigo_producto"+id).val();
			var nombre_producto = $("#nombre_producto"+id).val();
			var estado = $("#estado"+id).val();
			var precio_producto = $("#precio_producto"+id).val();
			var precio_compra=$("#precio_compra"+id).val();
			var cant_total= $("#cantidad_total"+id).val();
			var ex_min = $("#ex_min"+id).val();
			var stock = $("#stock"+id).val();
			var exento = $("#exento"+id).val();
			var cod_prov= $("#cod_prov"+id).val();
			var prov_prod= $("#prov_prod"+id).val();
			var marca= $("#marca"+id).val();
			var id_marca= $("#id_marca"+id).val();
			var unidad_m= $("#unidad_m"+id).val();
			var id_medida=$("#id_medida"+id).val();
			$("#mod_id").val(id);
			$("#mod_codigo").val(codigo_producto);
			$("#mod_nombre").val(nombre_producto);
			$("#mod_precio").val(precio_producto);
			$("#mod_precio_compra").val(precio_compra);
			$("#mod_cantidad").val(cant_total);
			$("#mod_ex_min").val(ex_min);
			$("#mod_stock").val(stock);
			$("#mod_exento").val(exento);
			$("#id_prov").val(cod_prov);
			$("#mod_prov").val(prov_prod);
			$("#mod_marca").val(marca);
			$("#id_marca").val(id_marca);
			$("#mod_unidad_m").val(unidad_m);
			$("#id_medida").val(id_medida);
		}
</script>
<!-- <script>
    $(function() {
            $("#mod_marca").autocomplete({
              source: "./ajax/autocomplete/marca.php",
              minLength: 2,
              select: function(event, ui) {
                event.preventDefault();
                $('#id_marca').val(ui.item.id_marca);
                $('#mod_marca').val(ui.item.nombre_marca);
                // $('#tel1').val(ui.item.telefono_cliente);
                // $('#mail').val(ui.item.email_cliente);
                                
                
               }
            });
             
            
          });
          
  $("#mod_marca" ).on( "keydown", function( event ) {
            if (event.keyCode== $.ui.keyCode.LEFT || event.keyCode== $.ui.keyCode.RIGHT || event.keyCode== $.ui.keyCode.UP || event.keyCode== $.ui.keyCode.DOWN || event.keyCode== $.ui.keyCode.DELETE || event.keyCode== $.ui.keyCode.BACKSPACE )
            {
              $("#id_marca" ).val("");
             
                      
            }
            if (event.keyCode==$.ui.keyCode.DELETE){
              $("#mod_marca" ).val("");
              $("#id_marca" ).val("");
             
            }
      }); 
  </script> -->


<?php
        //}
?>
